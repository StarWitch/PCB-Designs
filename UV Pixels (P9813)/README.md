# UV Pixels (P9813)

Pixels intended to be used with the P9813 chipset, controlling 12 LEDs with 3 channels, using NPN transistors.

The drivers run off of 5V, but the pixels ought to be powered by 12-14V. They're chainable, but only in one direction, which helps with creating odd patterns. They're meant to be sewable, but with copper wires providing power. 

Intended for a Sumireko Usami cosplay (from Touhou Project).

Silkscreen art was borrowed from <a href="https://www.reddit.com/r/touhou/comments/9jkphu/windows_characters_as_flatstyle_sprites/" target="_blank">here.</a> Not planning on selling these currently.

<img src="https://github.com/StarWitch/PCB-Designs/blob/assets/uvpixelp9813-2019-07-24-2.png" width="350"/>
<br />
<img src="https://github.com/StarWitch/PCB-Designs/blob/assets/uvpixelp9813-2019-07-24-1.png" width="350"/>

