# PCB-Designs

All of these boards are 2-layer designs!

You can open all of these projects with AutoCAD EAGLE Free Edition on any major operating system.

You'll likely need the Adafruit, SparkFun, and SeeedStudio EAGLE libraries to be able to do much.

Current designs I'm working on as of June 2019:

1. LED Pixels/UVPixels (P9813)
2. Level Shifting Daughter Board 
3. Star Earrings

Complete designs that were fabricated:

1. SparkleShinyProduction (and Alpha)
2. RobotEarsModule
3. LiPoNommer
4. VULamp (and 2.0)
5. Latern Battery Power Supply (although complete routed Board was lost in backups)
6. ControlBoardAlpha (again, lost the routing, and it never worked to begin with)

Designs that were never completed and/or fabricated:

2. RobotEarsShield
3. AlarmClock
