# Sparkle Shiny Matrix Controller

I made this for a Burning Man project in 2012!

It uses the Texas Instruments TLC5940 PWM controllers for cathodes, and dual-channel NPN transistors for the anodes - therefore this is an anode-driven RGB LED matrix driver. There are trimpots available to directly limit the current that can flow through the cathodes, and allows for both external 5V input as well as taking 5V from the Arduino (though I wouldn't recommend it!).

Since the advent of individually-addressable RGB LED pixels like the WS2801 and P9813, it's less useful since building a full multiplexed Matrix isn't as necessary as it once was. However, I'm still very happy with how this project turned out.

<img src="https://github.com/StarWitch/PCB-Designs/blob/assets/sparkleshiny-2019-07-24-1.png" width="350"/>
<br />
<img src="https://github.com/StarWitch/PCB-Designs/blob/assets/sparkleshiny-2019-07-24-2.png" width="350"/>

