# Level Shifting Daughter Board

I couldn't find a board like this that actually did everything I wanted, so I created one!

It has:

* SPI level shifters (74ACT125D)
* I2C level shifters 
* ADC for thumbpot input (ADS1015)
* RTC (for fun)
* High-side current sensor (INA219)

Silkscreen art was borrowed from <a href="https://www.reddit.com/r/touhou/comments/9jkphu/windows_characters_as_flatstyle_sprites/" target="_blank">here.</a> Not planning on selling these currently.

<img src="https://github.com/StarWitch/PCB-Designs/blob/assets/daughterboard-2019-07-24-2.png" width="350"/>
<br />
<img src="https://github.com/StarWitch/PCB-Designs/blob/assets/daughterboard-2019-07-24-1.png" width="350"/>

