# Star Earrings

I went a bit nuts with these! The features are as follows:

* SAMD21G18 (as in the Circuit Python Express)
* 5V Boost Converter for Lithium Polymer batteries
* Lithium Polymer charger
* LIS3DH accelerometer
* I2S microphone
* 25 DotStar 2020 RGB LED pixels (individually addressable)
* 2x touchpads 

Silkscreen art was borrowed from <a href="https://www.reddit.com/r/touhou/comments/9jkphu/windows_characters_as_flatstyle_sprites/" target="_blank">here.</a> Not planning on selling these currently.

<img src="https://github.com/StarWitch/PCB-Designs/blob/assets/starearrings-2019-07-24-2.png" width="350"/>
<br />
<img src="https://github.com/StarWitch/PCB-Designs/blob/assets/starearrings-2019-07-24-1.png" width="350"/>

