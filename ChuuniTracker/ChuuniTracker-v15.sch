<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="2.54" unitdist="mm" unit="mm" style="lines" multiple="1" display="no" altdistance="0.254" altunitdist="mm" altunit="mm"/>
<layers>
<layer number="1" name="Top" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" blackcolor="0xb462d987" whitecolor="0xb462d987" coloredcolor="0xcc0dab76" blackhighlight="0xb400ff00" whitehighlight="0xb400ff00" coloredhighlight="0xb400ff00" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" blackcolor="0xb462d987" whitecolor="0xb462d987" coloredcolor="0xcc0dab76" blackhighlight="0xb400ff00" whitehighlight="0xb400ff00" coloredhighlight="0xb400ff00" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" blackcolor="0xccd9d9d9" whitecolor="0xbbc8c832" coloredcolor="0xbbc8c832" blackhighlight="0xb462d987" whitehighlight="0xb462d987" coloredhighlight="0xccffcd07" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" blackcolor="0xc8ffffff" whitecolor="0xccafd134" coloredcolor="0xccafd134" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" blackcolor="0xc8ffffff" whitecolor="0xccafd134" coloredcolor="0xccafd134" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" blackcolor="0xc8ffffff" whitecolor="0xccafd134" coloredcolor="0xccafd134" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" blackcolor="0xccd9d9d9" whitecolor="0xbbc8c832" coloredcolor="0xbbc8c832" blackhighlight="0xb462d987" whitehighlight="0xb462d987" coloredhighlight="0xccffcd07" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" blackcolor="0xccd9d9d9" whitecolor="0xbbc8c832" coloredcolor="0xbbc8c832" blackhighlight="0xb462d987" whitehighlight="0xb462d987" coloredhighlight="0xccffcd07" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" blackcolor="0xb462d987" whitecolor="0xb462d987" coloredcolor="0xcc0dab76" blackhighlight="0xb400ff00" whitehighlight="0xb400ff00" coloredhighlight="0xb400ff00" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" blackcolor="0xcc32c8c8" whitecolor="0xb432c8c8" coloredcolor="0xb432c8c8" blackhighlight="0xb400ffff" whitehighlight="0xb400ffff" coloredhighlight="0xb400ffff" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="58" name="b3D" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="61" name="stand" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" blackcolor="0xb4815cc2" whitecolor="0xb4815cc2" coloredcolor="0xc8fdc689" blackhighlight="0xb4815cc2" whitehighlight="0xb4815cc2" coloredhighlight="0xc8fdc689" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" blackcolor="0xb4815cc2" whitecolor="0xb4815cc2" coloredcolor="0xc8fdc689" blackhighlight="0xb4815cc2" whitehighlight="0xb4815cc2" coloredhighlight="0xc8fdc689" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" blackcolor="0xdcfac36e" whitecolor="0xdcf57c10" coloredcolor="0xc8d9ff3d" blackhighlight="0xdcfac36e" whitehighlight="0xdcf57c10" coloredhighlight="0xc8d9ff3d" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" blackcolor="0xdc009292" whitecolor="0xdc009292" coloredcolor="0xc8e3e3e3" blackhighlight="0xdc009292" whitehighlight="0xdc009292" coloredhighlight="0xc8e3e3e3" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" blackcolor="0xdc009292" whitecolor="0xdc009292" coloredcolor="0xc8d62525" blackhighlight="0xdc009292" whitehighlight="0xdc009292" coloredhighlight="0xc8d62525" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" blackcolor="0xdcbbbbbb" whitecolor="0xdcbbbbbb" coloredcolor="0xc839d437" blackhighlight="0xdcbbbbbb" whitehighlight="0xdcbbbbbb" coloredhighlight="0xc839d437" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" blackcolor="0xdcfac36e" whitecolor="0xdcf57c10" coloredcolor="0xc8e6e600" blackhighlight="0xdcfac36e" whitehighlight="0xdcf57c10" coloredhighlight="0xc8e6e600" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" blackcolor="0xf0b2b2b2" whitecolor="0xc8757575" coloredcolor="0xc82c7bce" blackhighlight="0xf0b2b2b2" whitehighlight="0xc8757575" coloredhighlight="0xc82c7bce" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" blackcolor="0xf0b2b2b2" whitecolor="0xc8757575" coloredcolor="0xc82c7bce" blackhighlight="0xf0b2b2b2" whitehighlight="0xc8757575" coloredhighlight="0xc82c7bce" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" blackcolor="0xf0b2b2b2" whitecolor="0xc8757575" coloredcolor="0xc89526de" blackhighlight="0xf0b2b2b2" whitehighlight="0xc8757575" coloredhighlight="0xc89526de" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" blackcolor="0xf0b2b2b2" whitecolor="0xc8757575" coloredcolor="0xc89526de" blackhighlight="0xf0b2b2b2" whitehighlight="0xc8757575" coloredhighlight="0xc89526de" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" blackcolor="0xccff0000" whitecolor="0xb4ff0000" coloredcolor="0xb4ff0000" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" blackcolor="0xc800bfbf" whitecolor="0xc800bfbf" coloredcolor="0xccffe74c" blackhighlight="0xb462d986" whitehighlight="0xb462d986" coloredhighlight="0xcceec643" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" blackcolor="0xb400ff00" whitecolor="0xb400ff00" coloredcolor="0xb400ff00" blackhighlight="0xb400ff00" whitehighlight="0xb400ff00" coloredhighlight="0xb400ff00" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" blackcolor="0xb400ffff" whitecolor="0xb400ffff" coloredcolor="0xb400ffff" blackhighlight="0xb400ffff" whitehighlight="0xb400ffff" coloredhighlight="0xb400ffff" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" blackcolor="0xb40000ff" whitecolor="0xcc8252c2" coloredcolor="0xcc8252c2" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="134" name="silk_top" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="135" name="silk_bottom" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="136" name="silktop" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="138" name="EEE" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="139" name="_tKeepout" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="141" name="ASSEMBLY_TOP" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="143" name="PLACE_BOUND_TOP" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="145" name="DrillLegend_01-16" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="147" name="PIN_NUMBER" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="149" name="DrillLegend_02-15" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="166" name="AntennaArea" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="188" name="188bmp" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="10" visible="yes" active="yes"/>
<layer number="191" name="mNets" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" blackcolor="0xc80696d7" whitecolor="0xcc0696d7" coloredcolor="0xcc0696d7" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" blackcolor="0xb462d987" whitecolor="0xb462d987" coloredcolor="0xcc0dab76" blackhighlight="0xb400ff00" whitehighlight="0xb400ff00" coloredhighlight="0xb400ff00" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" blackcolor="0xcc32c8c8" whitecolor="0xb432c8c8" coloredcolor="0xb432c8c8" blackhighlight="0xb400ffff" whitehighlight="0xb400ffff" coloredhighlight="0xb400ffff" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" blackcolor="0xc8ff4164" whitecolor="0xccc90d0c" coloredcolor="0xccc90d0c" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" blackcolor="0xccffba08" whitecolor="0xccffba08" coloredcolor="0xccffba08" blackhighlight="0xccff00ff" whitehighlight="0xb4ff00ff" coloredhighlight="0xb4ff00ff" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" blackcolor="0xccd9d9d9" whitecolor="0xbbc8c832" coloredcolor="0xbbc8c832" blackhighlight="0xb462d987" whitehighlight="0xb462d987" coloredhighlight="0xccffcd07" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" blackcolor="0xb4282828" whitecolor="0xb4282828" coloredcolor="0xb4282828" blackhighlight="0xb4282828" whitehighlight="0xb4282828" coloredhighlight="0xb4282828" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" blackcolor="0xb40000ff" whitecolor="0xcc8252c2" coloredcolor="0xcc8252c2" blackhighlight="0xb40000ff" whitehighlight="0xcc8252c2" coloredhighlight="0xcc8252c2" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" blackcolor="0xc89fc966" whitecolor="0xc89fc966" coloredcolor="0xccffdda1" blackhighlight="0xc8ff8cc6" whitehighlight="0xc8ff8cc6" coloredhighlight="0xcc6f45b4" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" blackcolor="0xc8a7bacf" whitecolor="0xc8a7bacf" coloredcolor="0xcc5fad56" blackhighlight="0xc8f26a52" whitehighlight="0xc8f26a52" coloredhighlight="0xcc516363" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" blackcolor="0xc8bb7a14" whitecolor="0xc8bb7a14" coloredcolor="0xcc00bfb2" blackhighlight="0xc89bd5ef" whitehighlight="0xc89bd5ef" coloredhighlight="0xccd6a2ad" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" blackcolor="0xc8658d30" whitecolor="0xc8658d30" coloredcolor="0xcc65def1" blackhighlight="0xc8ffbeab" whitehighlight="0xc8ffbeab" coloredhighlight="0xccef476f" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" blackcolor="0xc823a597" whitecolor="0xc823a597" coloredcolor="0xccc55ea3" blackhighlight="0xc8e8430f" whitehighlight="0xc8e8430f" coloredhighlight="0xccd4dcff" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" blackcolor="0xc8a76ef5" whitecolor="0xc8a76ef5" coloredcolor="0xccf56416" blackhighlight="0xccafd134" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" blackcolor="0xb462d986" whitecolor="0xb462d986" coloredcolor="0xcceec643" blackhighlight="0xb462d986" whitehighlight="0xb462d986" coloredhighlight="0xcceec643" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" blackcolor="0xc8c5a1f8" whitecolor="0xc8c5a1f8" coloredcolor="0xcc5e897c" blackhighlight="0xc8c5a1f8" whitehighlight="0xc8c5a1f8" coloredhighlight="0xcc5e897c" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" blackcolor="0xcc32c8c8" whitecolor="0xb432c8c8" coloredcolor="0xb432c8c8" blackhighlight="0xb400ffff" whitehighlight="0xb400ffff" coloredhighlight="0xb400ffff" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" blackcolor="0xccff0000" whitecolor="0xb4ff0000" coloredcolor="0xb4ff0000" blackhighlight="0xccff0000" whitehighlight="0xb4ff0000" coloredhighlight="0xb4ff0000" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" blackcolor="0xccc8c8c8" whitecolor="0xbb808080" coloredcolor="0xbb808080" blackhighlight="0xc8ffffff" whitehighlight="0xccafd134" coloredhighlight="0xccafd134" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="alicedee" urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA">
<packages>
<package name="BNO080" library_version="26">
<wire x1="-2.6" y1="-1.9" x2="-2.6" y2="1.9" width="0.127" layer="51"/>
<wire x1="-2.6" y1="1.9" x2="2.6" y2="1.9" width="0.127" layer="51"/>
<wire x1="2.6" y1="1.9" x2="2.6" y2="-1.9" width="0.127" layer="51"/>
<wire x1="2.6" y1="-1.9" x2="-2.6" y2="-1.9" width="0.127" layer="51"/>
<smd name="1" x="-2.25" y="1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="2" x="-2.3125" y="0.75" dx="0.25" dy="0.575" layer="1" rot="R90"/>
<smd name="3" x="-2.3125" y="0.25" dx="0.25" dy="0.575" layer="1" rot="R90"/>
<smd name="4" x="-2.3125" y="-0.25" dx="0.25" dy="0.575" layer="1" rot="R90"/>
<smd name="5" x="-2.3125" y="-0.75" dx="0.25" dy="0.575" layer="1" rot="R90"/>
<smd name="6" x="-2.25" y="-1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="7" x="-1.75" y="-1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="8" x="-1.25" y="-1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="9" x="-0.75" y="-1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="10" x="-0.25" y="-1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="11" x="0.25" y="-1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="12" x="0.75" y="-1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="13" x="1.25" y="-1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="14" x="1.75" y="-1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="15" x="2.25" y="-1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="16" x="2.3125" y="-0.75" dx="0.25" dy="0.575" layer="1" rot="R90"/>
<smd name="17" x="2.3125" y="-0.25" dx="0.25" dy="0.575" layer="1" rot="R90"/>
<smd name="18" x="2.3125" y="0.25" dx="0.25" dy="0.575" layer="1" rot="R90"/>
<smd name="19" x="2.3125" y="0.75" dx="0.25" dy="0.575" layer="1" rot="R90"/>
<smd name="20" x="2.25" y="1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="21" x="1.75" y="1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="22" x="1.25" y="1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="23" x="0.75" y="1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="24" x="0.25" y="1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="25" x="-0.25" y="1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="26" x="-0.75" y="1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="27" x="-1.25" y="1.5625" dx="0.25" dy="0.675" layer="1"/>
<smd name="28" x="-1.75" y="1.5625" dx="0.25" dy="0.675" layer="1"/>
<wire x1="-2.7" y1="2" x2="2.7" y2="2" width="0.127" layer="21"/>
<wire x1="2.7" y1="2" x2="2.7" y2="-2" width="0.127" layer="21"/>
<wire x1="2.7" y1="-2" x2="-2.7" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.7" y1="-2" x2="-2.7" y2="2" width="0.127" layer="21"/>
<circle x="-3.3" y="1.5" radius="0.22360625" width="0.4064" layer="21"/>
<text x="-2.64" y="2.248" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.64" y="-2.61" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="GCT_USB4105-GF-A">
<wire x1="-4.79" y1="4.93" x2="4.79" y2="4.93" width="0.1" layer="51"/>
<wire x1="4.79" y1="4.93" x2="4.79" y2="-2.6" width="0.1" layer="51"/>
<wire x1="4.79" y1="-2.6" x2="-4.79" y2="-2.6" width="0.1" layer="51"/>
<wire x1="-4.79" y1="-2.6" x2="-4.79" y2="4.93" width="0.1" layer="51"/>
<wire x1="-4.79" y1="-1.32" x2="-4.79" y2="-2.6" width="0.2" layer="21"/>
<wire x1="-4.79" y1="-2.6" x2="4.79" y2="-2.6" width="0.2" layer="21"/>
<wire x1="4.79" y1="-2.6" x2="4.79" y2="-1.32" width="0.2" layer="21"/>
<wire x1="-5.1" y1="5.58" x2="-5.1" y2="-2.85" width="0.05" layer="39"/>
<wire x1="-5.1" y1="-2.85" x2="5.1" y2="-2.85" width="0.05" layer="39"/>
<wire x1="5.1" y1="-2.85" x2="5.1" y2="5.58" width="0.05" layer="39"/>
<wire x1="5.1" y1="5.58" x2="-5.1" y2="5.58" width="0.05" layer="39"/>
<text x="-5" y="7.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="6" size="1.27" layer="27">&gt;VALUE</text>
<text x="5.4" y="-2.5" size="0.4064" layer="51">PCB EDGE</text>
<wire x1="4.8" y1="-2.6" x2="8.4" y2="-2.6" width="0.1" layer="51"/>
<wire x1="-4.79" y1="2.65" x2="-4.79" y2="1.4" width="0.2" layer="21"/>
<wire x1="4.79" y1="2.65" x2="4.79" y2="1.4" width="0.2" layer="21"/>
<hole x="2.89" y="3.68" drill="0.65"/>
<hole x="-2.89" y="3.68" drill="0.65"/>
<smd name="A1-B12" x="-3.2" y="4.755" dx="0.6" dy="1.15" layer="1"/>
<smd name="A12-B1" x="3.2" y="4.755" dx="0.6" dy="1.15" layer="1"/>
<smd name="A4-B9" x="-2.4" y="4.755" dx="0.6" dy="1.15" layer="1"/>
<smd name="B4-A9" x="2.4" y="4.755" dx="0.6" dy="1.15" layer="1"/>
<smd name="B6" x="0.25" y="4.755" dx="0.3" dy="1.15" layer="1"/>
<smd name="A6" x="-0.25" y="4.755" dx="0.3" dy="1.15" layer="1"/>
<smd name="A7" x="0.75" y="4.755" dx="0.3" dy="1.15" layer="1"/>
<smd name="B5" x="1.25" y="4.755" dx="0.3" dy="1.15" layer="1"/>
<smd name="A8" x="1.75" y="4.755" dx="0.3" dy="1.15" layer="1"/>
<smd name="B7" x="-0.75" y="4.755" dx="0.3" dy="1.15" layer="1"/>
<smd name="B8" x="-1.25" y="4.755" dx="0.3" dy="1.15" layer="1"/>
<smd name="A5" x="-1.75" y="4.755" dx="0.3" dy="1.15" layer="1"/>
<wire x1="4.318" y1="4.572" x2="4.318" y2="3.556" width="0.5842" layer="46"/>
<wire x1="4.318" y1="0.635" x2="4.318" y2="-0.635" width="0.5842" layer="46"/>
<wire x1="-4.318" y1="4.572" x2="-4.318" y2="3.556" width="0.5842" layer="46"/>
<wire x1="-4.318" y1="0.635" x2="-4.318" y2="-0.635" width="0.5842" layer="46"/>
<pad name="P$1" x="4.318" y="4.572" drill="0.5842" diameter="1.016"/>
<pad name="P$2" x="4.318" y="3.556" drill="0.5842" diameter="1.016"/>
<wire x1="4.318" y1="4.572" x2="4.318" y2="3.556" width="1.016" layer="1"/>
<wire x1="4.318" y1="4.572" x2="4.318" y2="3.556" width="1.016" layer="16"/>
<wire x1="4.318" y1="4.572" x2="4.318" y2="3.556" width="1.016" layer="30"/>
<wire x1="4.318" y1="4.572" x2="4.318" y2="3.556" width="1.016" layer="29"/>
<pad name="P$5" x="4.318" y="0.635" drill="0.57" diameter="1.016"/>
<pad name="P$6" x="4.318" y="-0.635" drill="0.57" diameter="1.016"/>
<wire x1="4.318" y1="0.635" x2="4.318" y2="-0.635" width="1.016" layer="1"/>
<wire x1="4.318" y1="0.635" x2="4.318" y2="-0.635" width="1.016" layer="16"/>
<wire x1="4.318" y1="0.635" x2="4.318" y2="-0.635" width="1.016" layer="30"/>
<wire x1="4.318" y1="0.635" x2="4.318" y2="-0.635" width="1.016" layer="29"/>
<pad name="P$7" x="-4.318" y="4.572" drill="0.5" diameter="1.016"/>
<pad name="P$8" x="-4.318" y="3.556" drill="0.5" diameter="1.016"/>
<wire x1="-4.318" y1="4.572" x2="-4.318" y2="3.556" width="1.016" layer="1"/>
<wire x1="-4.318" y1="4.572" x2="-4.318" y2="3.556" width="1.016" layer="16"/>
<wire x1="-4.318" y1="4.572" x2="-4.318" y2="3.556" width="1.016" layer="30"/>
<wire x1="-4.318" y1="4.572" x2="-4.318" y2="3.556" width="1.016" layer="29"/>
<pad name="P$3" x="-4.318" y="0.635" drill="0.5" diameter="1.016"/>
<pad name="P$9" x="-4.318" y="-0.635" drill="0.5" diameter="1.016"/>
<wire x1="-4.318" y1="0.635" x2="-4.318" y2="-0.635" width="1.016" layer="1"/>
<wire x1="-4.318" y1="0.635" x2="-4.318" y2="-0.635" width="1.016" layer="16"/>
<wire x1="-4.318" y1="0.635" x2="-4.318" y2="-0.635" width="1.016" layer="30"/>
<wire x1="-4.318" y1="0.635" x2="-4.318" y2="-0.635" width="1.016" layer="29"/>
</package>
<package name="SOT23-5L" urn="urn:adsk.eagle:footprint:6240078/1" library_version="2">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;&lt;p&gt;
package type OT</description>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.81" x2="-0.522" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.81" x2="0.428" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="-1.328" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="1.328" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.328" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="-1.328" y2="0.81" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="JST-PH-2-SMT" urn="urn:adsk.eagle:footprint:6240117/1" library_version="2">
<wire x1="-4" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="51"/>
<wire x1="4" y1="2.5" x2="4" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-4" y1="-2.5" x2="-4" y2="2.5" width="0.2032" layer="51"/>
<wire x1="4" y1="-2.5" x2="-4" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="2.5" x2="2.25" y2="2.5" width="0.127" layer="21"/>
<wire x1="4" y1="-0.5" x2="4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="4" y1="-2.5" x2="1.75" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-1.75" y1="-2.5" x2="-4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-2.5" x2="-4" y2="-0.5" width="0.127" layer="21"/>
<smd name="1" x="-1" y="-1.8" dx="1" dy="5.5" layer="1"/>
<smd name="2" x="1" y="-1.8" dx="1" dy="5.5" layer="1"/>
<smd name="NC1" x="-3.4" y="0" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="0" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" font="vector">&gt;Name</text>
<text x="-2.54" y="-7.62" size="1.27" layer="27" font="vector">&gt;Value</text>
<text x="-2.914" y="-6.096" size="1.4224" layer="21" ratio="12">+</text>
<text x="2.271" y="-6.096" size="1.4224" layer="21" ratio="12">-</text>
</package>
<package name="1X04_1MM_RA" urn="urn:adsk.eagle:footprint:37714/1" library_version="1">
<description>&lt;h3&gt;SMD- 4 Pin Right Angle &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MODULE_ESP32-S2-WROVER" library_version="130">
<wire x1="-9" y1="15.5" x2="9" y2="15.5" width="0.127" layer="51"/>
<wire x1="9" y1="15.5" x2="9" y2="9.2" width="0.127" layer="51"/>
<wire x1="9" y1="9.2" x2="9" y2="-15.5" width="0.127" layer="51"/>
<wire x1="9" y1="-15.5" x2="-9" y2="-15.5" width="0.127" layer="51"/>
<wire x1="-9" y1="-15.5" x2="-9" y2="9.2" width="0.127" layer="51"/>
<wire x1="-9" y1="15.5" x2="-9" y2="9.2" width="0.127" layer="51"/>
<wire x1="-9" y1="9.2" x2="9" y2="9.2" width="0.127" layer="51"/>
<text x="-6" y="11.75" size="1.778" layer="51">ANTENNA</text>
<wire x1="-9.25" y1="15.75" x2="9.25" y2="15.75" width="0.05" layer="39"/>
<wire x1="9.25" y1="15.75" x2="9.25" y2="8.7" width="0.05" layer="39"/>
<wire x1="9.25" y1="8.7" x2="9.75" y2="8.7" width="0.05" layer="39"/>
<wire x1="9.75" y1="8.7" x2="9.75" y2="-16.25" width="0.05" layer="39"/>
<wire x1="9.75" y1="-16.25" x2="-9.75" y2="-16.25" width="0.05" layer="39"/>
<wire x1="-9.75" y1="-16.25" x2="-9.75" y2="8.7" width="0.05" layer="39"/>
<wire x1="-9.75" y1="8.7" x2="-9.25" y2="8.7" width="0.05" layer="39"/>
<wire x1="-9.25" y1="8.7" x2="-9.25" y2="15.75" width="0.05" layer="39"/>
<circle x="-10.2" y="8" radius="0.1" width="0.2" layer="51"/>
<circle x="-10.2" y="8" radius="0.1" width="0.2" layer="21"/>
<text x="-9" y="16" size="1.27" layer="25">&gt;NAME</text>
<text x="-9" y="-16.5" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
<wire x1="-9" y1="-15.3" x2="-9" y2="-15.5" width="0.127" layer="21"/>
<wire x1="-9" y1="-15.5" x2="-7.55" y2="-15.5" width="0.127" layer="21"/>
<wire x1="7.55" y1="-15.5" x2="9" y2="-15.5" width="0.127" layer="21"/>
<wire x1="9" y1="-15.5" x2="9" y2="-15.3" width="0.127" layer="21"/>
<wire x1="-9" y1="9" x2="-9" y2="15.5" width="0.127" layer="21"/>
<wire x1="-9" y1="15.5" x2="9" y2="15.5" width="0.127" layer="21"/>
<wire x1="9" y1="15.5" x2="9" y2="9" width="0.127" layer="21"/>
<smd name="1" x="-8.75" y="8" dx="1.5" dy="0.9" layer="1"/>
<smd name="43_5" x="-1.19" y="-0.05" dx="1.1" dy="1.1" layer="1"/>
<smd name="2" x="-8.75" y="6.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="3" x="-8.75" y="5" dx="1.5" dy="0.9" layer="1"/>
<smd name="4" x="-8.75" y="3.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="5" x="-8.75" y="2" dx="1.5" dy="0.9" layer="1"/>
<smd name="6" x="-8.75" y="0.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="7" x="-8.75" y="-1" dx="1.5" dy="0.9" layer="1"/>
<smd name="8" x="-8.75" y="-2.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="9" x="-8.75" y="-4" dx="1.5" dy="0.9" layer="1"/>
<smd name="10" x="-8.75" y="-5.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="11" x="-8.75" y="-7" dx="1.5" dy="0.9" layer="1"/>
<smd name="12" x="-8.75" y="-8.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="13" x="-8.75" y="-10" dx="1.5" dy="0.9" layer="1"/>
<smd name="14" x="-8.75" y="-11.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="15" x="-8.75" y="-13" dx="1.5" dy="0.9" layer="1"/>
<smd name="16" x="-8.75" y="-14.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="17" x="-6.75" y="-15.25" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="18" x="-5.25" y="-15.25" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="19" x="-3.75" y="-15.25" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="20" x="-2.25" y="-15.25" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="21" x="-0.75" y="-15.25" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="22" x="0.75" y="-15.25" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="23" x="2.25" y="-15.25" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="24" x="3.75" y="-15.25" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="25" x="5.25" y="-15.25" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="26" x="6.75" y="-15.25" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="27" x="8.75" y="-14.5" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="28" x="8.75" y="-13" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="29" x="8.75" y="-11.5" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="30" x="8.75" y="-10" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="31" x="8.75" y="-8.5" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="32" x="8.75" y="-7" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="33" x="8.75" y="-5.5" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="34" x="8.75" y="-4" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="35" x="8.75" y="-2.5" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="36" x="8.75" y="-1" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="37" x="8.75" y="0.5" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="38" x="8.75" y="2" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="39" x="8.75" y="3.5" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="40" x="8.75" y="5" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="41" x="8.75" y="6.5" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="42" x="8.75" y="8" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="43_1" x="-2.69" y="1.45" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_2" x="-1.19" y="1.45" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_3" x="0.31" y="1.45" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_4" x="-2.69" y="-0.05" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_6" x="0.31" y="-0.05" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_7" x="-2.69" y="-1.55" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_8" x="-1.19" y="-1.55" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_9" x="0.31" y="-1.55" dx="1.1" dy="1.1" layer="1"/>
<pad name="43_10" x="-1.94" y="1.45" drill="0.4"/>
<pad name="43_11" x="-0.44" y="1.45" drill="0.4"/>
<pad name="43_12" x="-2.69" y="0.7" drill="0.4"/>
<pad name="43_13" x="-1.19" y="0.7" drill="0.4"/>
<pad name="43_14" x="0.31" y="0.7" drill="0.4"/>
<pad name="43_15" x="-1.94" y="-0.05" drill="0.4"/>
<pad name="43_16" x="-0.44" y="-0.05" drill="0.4"/>
<pad name="43_17" x="-2.69" y="-0.8" drill="0.4"/>
<pad name="43_18" x="-1.19" y="-0.8" drill="0.4"/>
<pad name="43_19" x="0.31" y="-0.8" drill="0.4"/>
<pad name="43_20" x="-1.94" y="-1.55" drill="0.4"/>
<pad name="43_21" x="-0.44" y="-1.55" drill="0.4"/>
</package>
<package name="WS2812B-NARROW" urn="urn:adsk.eagle:footprint:6240318/1" library_version="2">
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="2.5" x2="-1.25" y2="2.85" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.85" x2="-1.7" y2="3.3" width="0.127" layer="21"/>
<wire x1="-1.7" y1="3.3" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<smd name="1-VDD" x="2.35" y="-1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<smd name="2-DOUT" x="2.35" y="1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<smd name="4-DIN" x="-2.35" y="-1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<smd name="3-GND" x="-2.35" y="1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<circle x="0" y="0" radius="1.7204625" width="0.127" layer="21"/>
<text x="3.4925" y="1.5875" size="0.8128" layer="25" ratio="10" rot="R270">&gt;NAME</text>
</package>
<package name="WS2812B" urn="urn:adsk.eagle:footprint:6240299/1" library_version="2">
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<smd name="1-VDD" x="2.45" y="-1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="2-DOUT" x="2.45" y="1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="4-DIN" x="-2.45" y="-1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="3-GND" x="-2.45" y="1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<circle x="0" y="0" radius="1.7204625" width="0.127" layer="21"/>
<text x="3.4925" y="1.5875" size="0.8128" layer="25" ratio="10" rot="R270">&gt;NAME</text>
<text x="-3.81" y="2.54" size="1.6764" layer="21" ratio="10">●</text>
</package>
<package name="SK6812" library_version="131">
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<smd name="1.VSS" x="2.45" y="-1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="2.DIN" x="2.45" y="1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="4.DOUT" x="-2.45" y="-1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="3.VDD" x="-2.45" y="1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<circle x="0" y="0" radius="1.7204625" width="0.127" layer="21"/>
<text x="3.4925" y="1.5875" size="0.8128" layer="25" ratio="10" rot="R270">&gt;NAME</text>
<text x="2.54" y="-3.81" size="1.6764" layer="21" ratio="10">●</text>
<wire x1="2.413" y1="-1.27" x2="1.524" y2="-2.413" width="0.127" layer="21"/>
</package>
<package name="16-LD-QFN" library_version="138">
<wire x1="4.5466" y1="4.0132" x2="4.5466" y2="4.5466" width="0.1524" layer="21"/>
<wire x1="4.0132" y1="0.5334" x2="4.5466" y2="0.5334" width="0.1524" layer="21"/>
<wire x1="1.0668" y1="4.5466" x2="0.5334" y2="4.5466" width="0.1524" layer="21"/>
<wire x1="0.5334" y1="0.5334" x2="1.0668" y2="0.5334" width="0.1524" layer="21"/>
<wire x1="4.5466" y1="0.5334" x2="4.5466" y2="1.0668" width="0.1524" layer="21"/>
<wire x1="4.5466" y1="4.5466" x2="4.0132" y2="4.5466" width="0.1524" layer="21"/>
<wire x1="0.5334" y1="4.5466" x2="0.5334" y2="4.0132" width="0.1524" layer="21"/>
<wire x1="0.5334" y1="1.0668" x2="0.5334" y2="0.5334" width="0.1524" layer="21"/>
<wire x1="3.683" y1="4.5466" x2="3.3274" y2="4.5466" width="0" layer="51"/>
<wire x1="3.048" y1="4.5466" x2="2.6924" y2="4.5466" width="0" layer="51"/>
<wire x1="2.3876" y1="4.5466" x2="2.032" y2="4.5466" width="0" layer="51"/>
<wire x1="1.7526" y1="4.5466" x2="1.397" y2="4.5466" width="0" layer="51"/>
<wire x1="0.5334" y1="3.683" x2="0.5334" y2="3.3274" width="0" layer="51"/>
<wire x1="0.5334" y1="3.048" x2="0.5334" y2="2.6924" width="0" layer="51"/>
<wire x1="0.5334" y1="2.3876" x2="0.5334" y2="2.032" width="0" layer="51"/>
<wire x1="0.5334" y1="1.7526" x2="0.5334" y2="1.397" width="0" layer="51"/>
<wire x1="1.397" y1="0.5334" x2="1.7526" y2="0.5334" width="0" layer="51"/>
<wire x1="2.032" y1="0.5334" x2="2.3876" y2="0.5334" width="0" layer="51"/>
<wire x1="2.6924" y1="0.5334" x2="3.048" y2="0.5334" width="0" layer="51"/>
<wire x1="3.3274" y1="0.5334" x2="3.683" y2="0.5334" width="0" layer="51"/>
<wire x1="4.5466" y1="1.397" x2="4.5466" y2="1.7526" width="0" layer="51"/>
<wire x1="4.5466" y1="2.032" x2="4.5466" y2="2.3876" width="0" layer="51"/>
<wire x1="4.5466" y1="2.6924" x2="4.5466" y2="3.048" width="0" layer="51"/>
<wire x1="4.5466" y1="3.3274" x2="4.5466" y2="3.683" width="0" layer="51"/>
<wire x1="0.5334" y1="0.5334" x2="4.5466" y2="0.5334" width="0" layer="51"/>
<wire x1="4.5466" y1="0.5334" x2="4.5466" y2="4.5466" width="0" layer="51"/>
<wire x1="4.5466" y1="4.5466" x2="0.5334" y2="4.5466" width="0" layer="51"/>
<wire x1="0.5334" y1="4.5466" x2="0.5334" y2="0.5334" width="0" layer="51"/>
<smd name="1" x="0.762" y="3.5052" dx="0.3556" dy="1.0668" layer="1" rot="R270"/>
<smd name="2" x="0.762" y="2.8702" dx="0.3556" dy="1.0668" layer="1" rot="R270"/>
<smd name="3" x="0.762" y="2.2098" dx="0.3556" dy="1.0668" layer="1" rot="R270"/>
<smd name="4" x="0.762" y="1.5748" dx="0.3556" dy="1.0668" layer="1" rot="R270"/>
<smd name="5" x="1.5748" y="0.762" dx="0.3556" dy="1.0668" layer="1" rot="R180"/>
<smd name="6" x="2.2098" y="0.762" dx="0.3556" dy="1.0668" layer="1" rot="R180"/>
<smd name="7" x="2.8702" y="0.762" dx="0.3556" dy="1.0668" layer="1" rot="R180"/>
<smd name="8" x="3.5052" y="0.762" dx="0.3556" dy="1.0668" layer="1" rot="R180"/>
<smd name="9" x="4.318" y="1.5748" dx="0.3556" dy="1.0668" layer="1" rot="R270"/>
<smd name="10" x="4.318" y="2.2098" dx="0.3556" dy="1.0668" layer="1" rot="R270"/>
<smd name="11" x="4.318" y="2.8702" dx="0.3556" dy="1.0668" layer="1" rot="R270"/>
<smd name="12" x="4.318" y="3.5052" dx="0.3556" dy="1.0668" layer="1" rot="R270"/>
<smd name="13" x="3.5052" y="4.318" dx="0.3556" dy="1.0668" layer="1" rot="R180"/>
<smd name="14" x="2.8702" y="4.318" dx="0.3556" dy="1.0668" layer="1" rot="R180"/>
<smd name="15" x="2.2098" y="4.318" dx="0.3556" dy="1.0668" layer="1" rot="R180"/>
<smd name="16" x="1.5748" y="4.318" dx="0.3556" dy="1.0668" layer="1" rot="R180"/>
<smd name="17" x="2.54" y="2.54" dx="1.9558" dy="1.9558" layer="1"/>
<text x="-0.127" y="4.318" size="1.27" layer="21">•</text>
</package>
<package name="LED3535" urn="urn:adsk.eagle:footprint:6240319/1" library_version="2">
<smd name="1" x="-1.75" y="0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="4" x="1.75" y="0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.75" y="-0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.75" y="-0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<wire x1="-1.75" y1="1.75" x2="1.75" y2="1.75" width="0.127" layer="51"/>
<wire x1="1.75" y1="1.75" x2="1.75" y2="-1.75" width="0.127" layer="51"/>
<wire x1="1.75" y1="-1.75" x2="-1.75" y2="-1.75" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.75" x2="-1.75" y2="1.75" width="0.127" layer="51"/>
<wire x1="-1.9" y1="1.6" x2="-1.9" y2="1.9" width="0.127" layer="21"/>
<wire x1="-1.9" y1="1.9" x2="1.9" y2="1.9" width="0.127" layer="21"/>
<wire x1="1.9" y1="1.9" x2="1.9" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.6" x2="-1.9" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.9" x2="1.9" y2="-1.9" width="0.127" layer="21"/>
<wire x1="1.9" y1="-1.9" x2="1.9" y2="-1.6" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.4" width="0.127" layer="51"/>
<text x="-1.905" y="2.159" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-2.54" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygonshape width="0.127" layer="21">
<polygonoutlineobjects>
<vertex x="-1.068096875" y="1.8901"/>
<vertex x="-1.100571875" y="1.9685"/>
<vertex x="-1.9685" y="1.9685"/>
<vertex x="-1.9685" y="1.4605"/>
<vertex x="-1.497696875" y="1.4605"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="-1.068096875" y="1.8901"/>
<vertex x="-1.100571875" y="1.9685"/>
<vertex x="-1.9685" y="1.9685"/>
<vertex x="-1.9685" y="1.4605"/>
<vertex x="-1.497696875" y="1.4605"/>
<vertex x="-1.068096875" y="1.8901"/>
</polygonoutlinesegments>
</polygonshape>
</package>
<package name="EG1213" urn="urn:adsk.eagle:footprint:6239681/1" library_version="2">
<wire x1="-5" y1="1.9" x2="-0.7" y2="1.9" width="0.127" layer="21"/>
<wire x1="-0.7" y1="1.9" x2="0.7" y2="1.9" width="0.127" layer="21"/>
<wire x1="0.7" y1="1.9" x2="5" y2="1.9" width="0.127" layer="21"/>
<wire x1="5" y1="1.9" x2="5" y2="-1.9" width="0.127" layer="21"/>
<wire x1="5" y1="-1.9" x2="2.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.9" x2="1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="1" y1="-1.9" x2="-1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.9" x2="-2.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.9" x2="-5" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-5" y1="-1.9" x2="-5" y2="1.9" width="0.127" layer="21"/>
<wire x1="-0.7" y1="1.9" x2="-0.7" y2="1.2" width="0.127" layer="21"/>
<wire x1="-0.7" y1="1.2" x2="0.7" y2="1.2" width="0.127" layer="21"/>
<wire x1="0.7" y1="1.2" x2="0.7" y2="1.9" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.9" x2="-2.1" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.6" x2="2.1" y2="-1.6" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.6" x2="2.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.9" x2="-1" y2="-7" width="0.127" layer="21"/>
<wire x1="-1" y1="-7" x2="1" y2="-7" width="0.127" layer="21"/>
<wire x1="1" y1="-7" x2="1" y2="-1.9" width="0.127" layer="21"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.4224"/>
<pad name="MT2" x="4.8" y="0" drill="1.4" diameter="2.54"/>
<pad name="MT1" x="-4.8" y="0" drill="1.4" diameter="2.54"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.4224"/>
<pad name="3" x="1.9" y="0" drill="0.7" diameter="1.4224"/>
<text x="-2.7" y="2.4" size="1.27" layer="21" font="vector">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="BNO080" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.j03Sz7myTxW003tCMTxK2w?version=2" footprint_name="BNO080" locally_modified="yes" type="model" library_version="26">
<packageinstances>
<packageinstance name="BNO080"/>
</packageinstances>
<metadata pins="28" pitch="0.5" bodyLength="3.95" bodyWidth="5.15" height="1" ipcFamily="QFN" ipcName="QFN50P395X515X100-28N" mountingType="SMD"/>
</package3d>
<package3d name="GCT_USB4105-GF-A" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.m2fzCX6STRWF-vn2nMNhaA?version=5" footprint_name="GCT_USB4105-GF-A" locally_modified="yes" type="model">
<packageinstances>
<packageinstance name="GCT_USB4105-GF-A"/>
</packageinstances>
</package3d>
<package3d name="SOT23-5L" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.FlSLHj1wRFKTmmfP59244Q?version=2" footprint_name="SOT23-5L" locally_modified="yes" type="model" library_version="58">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;&lt;p&gt;
package type OT</description>
<packageinstances>
<packageinstance name="SOT23-5L"/>
</packageinstances>
<metadata pins="5" pitch="0.95" bodyLength="2.9" bodyWidth="1.3" height="1.1" ipcFamily="SOT23" ipcName="SOT95P240X110-5N" mountingType="SMD"/>
</package3d>
<package3d name="JST-PH-2-SMT-3D" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.ELgLTILvRsqCS-OhAXe6AQ?version=2" footprint_name="JST-PH-2-SMT" locally_modified="yes" type="model" library_version="92">
<packageinstances>
<packageinstance name="JST-PH-2-SMT"/>
</packageinstances>
</package3d>
<package3d name="1X04_1MM_RA" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.KLYt6sxhTXiqlVjlj-H4zw?version=2" footprint_name="1X04_1MM_RA" locally_modified="yes" type="model" library_version="100">
<description>&lt;h3&gt;SMD- 4 Pin Right Angle &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="1X04_1MM_RA"/>
</packageinstances>
</package3d>
<package3d name="MODULE_ESP32-S2-WROVER" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.MgkoGtQ7RGWIVI9TGL5JwA?version=2" footprint_name="MODULE_ESP32-S2-WROVER" locally_modified="yes" type="model" library_version="130">
<packageinstances>
<packageinstance name="MODULE_ESP32-S2-WROVER"/>
</packageinstances>
</package3d>
<package3d name="WS2812B-NARROW" urn="urn:adsk.eagle:package:6240961/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="WS2812B-NARROW"/>
</packageinstances>
</package3d>
<package3d name="WS2812B" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.1ZJw8vIvSD6LvefTG9WiAA?version=2" footprint_name="WS2812B" locally_modified="yes" type="model" library_version="131">
<packageinstances>
<packageinstance name="WS2812B"/>
</packageinstances>
</package3d>
<package3d name="SK6812" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.F-iA7yWuT7-p5_ne7S9zIw?version=4" footprint_name="SK6812" locally_modified="yes" type="model" library_version="131">
<packageinstances>
<packageinstance name="SK6812"/>
</packageinstances>
</package3d>
<package3d name="16-LD-QFN" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.rYaKax5CTIqFnK3WFbQwkw?version=2" footprint_name="16-LD-QFN" locally_modified="yes" type="model" library_version="138">
<packageinstances>
<packageinstance name="16-LD-QFN"/>
</packageinstances>
<metadata pins="16" pitch="0.65" bodyLength="4.05" bodyWidth="4.05" height="1" ipcFamily="QFN" ipcName="QFN65P405X405X100-17T210N" mountingType="SMD"/>
</package3d>
<package3d name="LED3535-2" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.LFJRaa0_RZWZKQyEjbEAwg?version=3" footprint_name="LED3535" locally_modified="yes" type="model" library_version="166">
<packageinstances>
<packageinstance name="LED3535"/>
</packageinstances>
</package3d>
<package3d name="EG1213" urn="" wip_urn="urn:adsk.wipprod:fs.file:vf.B2GuiAVbSwiZekg0hz0_Jg?version=2" footprint_name="EG1213" locally_modified="yes" type="model" library_version="171">
<packageinstances>
<packageinstance name="EG1213"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="BNO080" library_version="26">
<pin name="VDD" x="-20.32" y="20.32" length="short" direction="pwr"/>
<pin name="VDDIO" x="-20.32" y="17.78" length="short" direction="pwr"/>
<pin name="H_SA0/MOSI" x="-20.32" y="7.62" length="short" direction="out"/>
<pin name="H_SDA/MISO/TX" x="-20.32" y="5.08" length="short"/>
<pin name="H_SCL/SCLK/RX" x="-20.32" y="2.54" length="short"/>
<pin name="H_INTN" x="-20.32" y="0" length="short" direction="out"/>
<pin name="BOOTN" x="-20.32" y="-2.54" length="short" direction="in"/>
<pin name="PS1" x="-20.32" y="-5.08" length="short" direction="in"/>
<pin name="PS0/WAKE" x="-20.32" y="-7.62" length="short" direction="in"/>
<pin name="CLKSEL0" x="-20.32" y="-10.16" length="short" direction="in"/>
<pin name="H_CSN" x="-20.32" y="-12.7" length="short" direction="in"/>
<pin name="GND" x="-20.32" y="-17.78" length="short" direction="pwr"/>
<pin name="GNDIO" x="-20.32" y="-20.32" length="short" direction="pwr"/>
<pin name="CAP" x="20.32" y="20.32" length="short" direction="pas" rot="R180"/>
<pin name="XIN32" x="20.32" y="12.7" length="short" direction="in" rot="R180"/>
<pin name="CLKSEL1/XOUT32" x="20.32" y="10.16" length="short" direction="out" rot="R180"/>
<pin name="S_SDA" x="20.32" y="7.62" length="short" rot="R180"/>
<pin name="S_SCL" x="20.32" y="5.08" length="short" rot="R180"/>
<pin name="NRST" x="20.32" y="-2.54" length="short" direction="in" rot="R180"/>
<wire x1="-17.78" y1="22.86" x2="17.78" y2="22.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="17.78" y1="22.86" x2="17.78" y2="-5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="-5.08" x2="17.78" y2="-7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="-7.62" x2="17.78" y2="-15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="-15.24" x2="17.78" y2="-17.78" width="0.254" layer="94"/>
<wire x1="17.78" y1="-17.78" x2="17.78" y2="-22.86" width="0.254" layer="94"/>
<wire x1="17.78" y1="-22.86" x2="-7.62" y2="-22.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="-7.62" y1="-22.86" x2="-17.78" y2="-22.86" width="0.254" layer="94" style="shortdash"/>
<wire x1="-17.78" y1="-22.86" x2="-17.78" y2="22.86" width="0.254" layer="94"/>
<wire x1="-17.78" y1="22.86" x2="-17.78" y2="27.94" width="0.254" layer="94"/>
<wire x1="-17.78" y1="27.94" x2="17.78" y2="27.94" width="0.254" layer="94"/>
<wire x1="17.78" y1="27.94" x2="17.78" y2="22.86" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-22.86" x2="-17.78" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-27.94" x2="17.78" y2="-27.94" width="0.254" layer="94"/>
<wire x1="17.78" y1="-27.94" x2="17.78" y2="-22.86" width="0.254" layer="94"/>
<text x="0" y="25.4" size="1.27" layer="94" align="center">BNO080
Motion Co-Processor</text>
<text x="-15.24" y="-25.4" size="1.27" layer="94" align="center-left">VDD:
VDDIO:</text>
<text x="5.08" y="-25.4" size="1.27" layer="94" align="center-left">Op. Temp:
-40~85°C</text>
<text x="-7.62" y="-25.4" size="1.27" layer="94" align="center-left">2.4-3.6
1.7-3.6</text>
<text x="-17.78" y="29.21" size="1.27" layer="95">&gt;NAME</text>
<text x="-17.78" y="-30.48" size="1.27" layer="96">&gt;VALUE</text>
<text x="-6.35" y="-20.32" size="0.8128" layer="94" align="center-left">Ext XTAL: CSEL0=0/NC,CSEL1=XTAL
Ext CLK: CSEL0=1,CSEL1=1
Int Osc: CSEL0=1,CSEL1=0/NC</text>
<text x="-6.35" y="-16.51" size="1.27" layer="94" align="center-left">32.768kHz Source</text>
<wire x1="-7.62" y1="-22.86" x2="-7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-17.78" x2="-7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-15.24" x2="1.27" y2="-15.24" width="0.254" layer="94"/>
<wire x1="1.27" y1="-15.24" x2="17.78" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-17.78" x2="17.78" y2="-17.78" width="0.254" layer="94" style="shortdash"/>
<text x="2.54" y="-11.43" size="0.8128" layer="94" align="center-left">PS1  PS0   MODE
0    0     I2C
0    1     UART-RVC
1    0     UART
1    1     SPI</text>
<text x="2.54" y="-6.35" size="1.27" layer="94" align="center-left">Bus Select</text>
<wire x1="17.78" y1="-5.08" x2="1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="-7.62" width="0.254" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="-15.24" width="0.254" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="17.78" y2="-7.62" width="0.254" layer="94" style="shortdash"/>
</symbol>
<symbol name="USB_TYPE-C_MINIMAL">
<text x="7.62" y="-2.54" size="1.016" layer="97" font="vector" align="center">USB PORT
Type C</text>
<wire x1="15.24" y1="0" x2="15.24" y2="-33.02" width="0.127" layer="94"/>
<wire x1="15.24" y1="-33.02" x2="0" y2="-33.02" width="0.127" layer="94"/>
<wire x1="0" y1="-33.02" x2="0" y2="0" width="0.127" layer="94"/>
<wire x1="0" y1="0" x2="15.24" y2="0" width="0.127" layer="94"/>
<text x="0" y="5.08" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="0" y="2.54" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<polygonshape layer="94">
<polygonoutlineobjects>
<vertex x="5.158490625" y="-5.334"/>
<vertex x="5.001509375" y="-5.334"/>
<vertex x="4.239509375" y="-6.858"/>
<vertex x="5.920490625" y="-6.858"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="5.158490625" y="-5.334"/>
<vertex x="5.001509375" y="-5.334"/>
<vertex x="4.239509375" y="-6.858"/>
<vertex x="5.920490625" y="-6.858"/>
<vertex x="5.158490625" y="-5.334"/>
</polygonoutlinesegments>
</polygonshape>
<polygonshape layer="94">
<polygonoutlineobjects>
<vertex x="3.274090625" y="-10.340140625"/>
<vertex x="3.3682125" y="-10.307203125"/>
<vertex x="3.45264375" y="-10.254153125"/>
<vertex x="3.523153125" y="-10.18364375"/>
<vertex x="3.576203125" y="-10.0992125"/>
<vertex x="3.609140625" y="-10.005090625"/>
<vertex x="3.620303125" y="-9.906"/>
<vertex x="3.609140625" y="-9.806909375"/>
<vertex x="3.576203125" y="-9.7127875"/>
<vertex x="3.523153125" y="-9.62835625"/>
<vertex x="3.45264375" y="-9.557846875"/>
<vertex x="3.3682125" y="-9.504796875"/>
<vertex x="3.274090625" y="-9.471859375"/>
<vertex x="3.175" y="-9.460696875"/>
<vertex x="3.075909375" y="-9.471859375"/>
<vertex x="2.9817875" y="-9.504796875"/>
<vertex x="2.89735625" y="-9.557846875"/>
<vertex x="2.826846875" y="-9.62835625"/>
<vertex x="2.773796875" y="-9.7127875"/>
<vertex x="2.740859375" y="-9.806909375"/>
<vertex x="2.729696875" y="-9.906"/>
<vertex x="2.740859375" y="-10.005090625"/>
<vertex x="2.773796875" y="-10.0992125"/>
<vertex x="2.826846875" y="-10.18364375"/>
<vertex x="2.89735625" y="-10.254153125"/>
<vertex x="2.9817875" y="-10.307203125"/>
<vertex x="3.075909375" y="-10.340140625"/>
<vertex x="3.175" y="-10.351303125"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="3.274090625" y="-10.340140625"/>
<vertex x="3.3682125" y="-10.307203125"/>
<vertex x="3.45264375" y="-10.254153125"/>
<vertex x="3.523153125" y="-10.18364375"/>
<vertex x="3.576203125" y="-10.0992125"/>
<vertex x="3.609140625" y="-10.005090625"/>
<vertex x="3.620303125" y="-9.906"/>
<vertex x="3.609140625" y="-9.806909375"/>
<vertex x="3.576203125" y="-9.7127875"/>
<vertex x="3.523153125" y="-9.62835625"/>
<vertex x="3.45264375" y="-9.557846875"/>
<vertex x="3.3682125" y="-9.504796875"/>
<vertex x="3.274090625" y="-9.471859375"/>
<vertex x="3.175" y="-9.460696875"/>
<vertex x="3.075909375" y="-9.471859375"/>
<vertex x="2.9817875" y="-9.504796875"/>
<vertex x="2.89735625" y="-9.557846875"/>
<vertex x="2.826846875" y="-9.62835625"/>
<vertex x="2.773796875" y="-9.7127875"/>
<vertex x="2.740859375" y="-9.806909375"/>
<vertex x="2.729696875" y="-9.906"/>
<vertex x="2.740859375" y="-10.005090625"/>
<vertex x="2.773796875" y="-10.0992125"/>
<vertex x="2.826846875" y="-10.18364375"/>
<vertex x="2.89735625" y="-10.254153125"/>
<vertex x="2.9817875" y="-10.307203125"/>
<vertex x="3.075909375" y="-10.340140625"/>
<vertex x="3.175" y="-10.351303125"/>
<vertex x="3.274090625" y="-10.340140625"/>
</polygonoutlinesegments>
</polygonshape>
<polygonshape layer="94">
<polygonoutlineobjects>
<vertex x="7.4295" y="-8.1915"/>
<vertex x="6.5405" y="-8.1915"/>
<vertex x="6.5405" y="-9.0805"/>
<vertex x="7.4295" y="-9.0805"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="7.4295" y="-8.1915"/>
<vertex x="6.5405" y="-8.1915"/>
<vertex x="6.5405" y="-9.0805"/>
<vertex x="7.4295" y="-9.0805"/>
<vertex x="7.4295" y="-8.1915"/>
</polygonoutlinesegments>
</polygonshape>
<wire x1="5.08" y1="-13.501840625" x2="5.08" y2="-12.231840625" width="0.254" layer="94"/>
<wire x1="5.08" y1="-12.231840625" x2="5.08" y2="-6.731" width="0.254" layer="94"/>
<wire x1="6.687421875" y1="-10.203578125" x2="5.377578125" y2="-11.513421875" width="0.254" layer="94"/>
<wire x1="3.175" y1="-9.906" x2="3.175" y2="-10.755159375" width="0.254" layer="94"/>
<wire x1="3.472578125" y1="-11.473578125" x2="4.782421875" y2="-12.783421875" width="0.254" layer="94"/>
<wire x1="3.175" y1="-10.755159375" x2="3.472578125" y2="-11.473578125" width="0.254" layer="94" curve="44.999875"/>
<wire x1="6.985" y1="-8.636" x2="6.985" y2="-9.485159375" width="0.254" layer="94"/>
<wire x1="6.985" y1="-9.485159375" x2="6.687421875" y2="-10.203578125" width="0.254" layer="94" curve="-44.999875"/>
<wire x1="5.08" y1="-13.716" x2="5.08" y2="-13.501840625" width="0.254" layer="94"/>
<wire x1="5.08" y1="-13.501840625" x2="4.782421875" y2="-12.783421875" width="0.254" layer="94" curve="44.999875"/>
<wire x1="5.08" y1="-12.231840625" x2="5.377578125" y2="-11.513421875" width="0.254" layer="94" curve="-44.999875"/>
<polygonshape layer="94">
<polygonoutlineobjects>
<vertex x="5.34183125" y="-15.72908125"/>
<vertex x="5.53254375" y="-15.671228125"/>
<vertex x="5.7083" y="-15.57728125"/>
<vertex x="5.86235625" y="-15.45085625"/>
<vertex x="5.98878125" y="-15.2968"/>
<vertex x="6.082728125" y="-15.12104375"/>
<vertex x="6.14058125" y="-14.93033125"/>
<vertex x="6.1595" y="-14.7382375"/>
<vertex x="6.1595" y="-14.5987625"/>
<vertex x="6.14058125" y="-14.40666875"/>
<vertex x="6.082728125" y="-14.21595625"/>
<vertex x="5.98878125" y="-14.0402"/>
<vertex x="5.86235625" y="-13.88614375"/>
<vertex x="5.7083" y="-13.75971875"/>
<vertex x="5.53254375" y="-13.665771875"/>
<vertex x="5.34183125" y="-13.60791875"/>
<vertex x="5.1497375" y="-13.589"/>
<vertex x="5.0102625" y="-13.589"/>
<vertex x="4.81816875" y="-13.60791875"/>
<vertex x="4.62745625" y="-13.665771875"/>
<vertex x="4.4517" y="-13.75971875"/>
<vertex x="4.29764375" y="-13.88614375"/>
<vertex x="4.17121875" y="-14.0402"/>
<vertex x="4.077271875" y="-14.21595625"/>
<vertex x="4.01941875" y="-14.40666875"/>
<vertex x="4.0005" y="-14.5987625"/>
<vertex x="4.0005" y="-14.7382375"/>
<vertex x="4.01941875" y="-14.93033125"/>
<vertex x="4.077271875" y="-15.12104375"/>
<vertex x="4.17121875" y="-15.2968"/>
<vertex x="4.29764375" y="-15.45085625"/>
<vertex x="4.4517" y="-15.57728125"/>
<vertex x="4.62745625" y="-15.671228125"/>
<vertex x="4.81816875" y="-15.72908125"/>
<vertex x="5.0102625" y="-15.748"/>
<vertex x="5.1497375" y="-15.748"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="5.34183125" y="-15.72908125"/>
<vertex x="5.53254375" y="-15.671228125"/>
<vertex x="5.7083" y="-15.57728125"/>
<vertex x="5.86235625" y="-15.45085625"/>
<vertex x="5.98878125" y="-15.2968"/>
<vertex x="6.082728125" y="-15.12104375"/>
<vertex x="6.14058125" y="-14.93033125"/>
<vertex x="6.1595" y="-14.7382375"/>
<vertex x="6.1595" y="-14.5987625"/>
<vertex x="6.14058125" y="-14.40666875"/>
<vertex x="6.082728125" y="-14.21595625"/>
<vertex x="5.98878125" y="-14.0402"/>
<vertex x="5.86235625" y="-13.88614375"/>
<vertex x="5.7083" y="-13.75971875"/>
<vertex x="5.53254375" y="-13.665771875"/>
<vertex x="5.34183125" y="-13.60791875"/>
<vertex x="5.1497375" y="-13.589"/>
<vertex x="5.0102625" y="-13.589"/>
<vertex x="4.81816875" y="-13.60791875"/>
<vertex x="4.62745625" y="-13.665771875"/>
<vertex x="4.4517" y="-13.75971875"/>
<vertex x="4.29764375" y="-13.88614375"/>
<vertex x="4.17121875" y="-14.0402"/>
<vertex x="4.077271875" y="-14.21595625"/>
<vertex x="4.01941875" y="-14.40666875"/>
<vertex x="4.0005" y="-14.5987625"/>
<vertex x="4.0005" y="-14.7382375"/>
<vertex x="4.01941875" y="-14.93033125"/>
<vertex x="4.077271875" y="-15.12104375"/>
<vertex x="4.17121875" y="-15.2968"/>
<vertex x="4.29764375" y="-15.45085625"/>
<vertex x="4.4517" y="-15.57728125"/>
<vertex x="4.62745625" y="-15.671228125"/>
<vertex x="4.81816875" y="-15.72908125"/>
<vertex x="5.0102625" y="-15.748"/>
<vertex x="5.1497375" y="-15.748"/>
<vertex x="5.34183125" y="-15.72908125"/>
</polygonoutlinesegments>
</polygonshape>
<pin name="SHLD" x="17.78" y="-30.48" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="17.78" y="-27.94" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="VBUS" x="17.78" y="-5.08" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="SBU1" x="17.78" y="-10.16" visible="off" length="short" rot="R180"/>
<pin name="SBU2" x="17.78" y="-12.7" visible="off" length="short" rot="R180"/>
<pin name="D+" x="17.78" y="-22.86" visible="off" length="short" rot="R180"/>
<pin name="D-" x="17.78" y="-20.32" visible="off" length="short" rot="R180"/>
<text x="13.97" y="-22.86" size="1.016" layer="97" font="vector" rot="R180" align="center-left">D+</text>
<text x="13.97" y="-20.32" size="1.016" layer="97" font="vector" rot="R180" align="center-left">D-</text>
<text x="13.97" y="-27.94" size="1.016" layer="97" font="vector" rot="R180" align="center-left">GND</text>
<text x="13.97" y="-10.16" size="1.016" layer="97" font="vector" rot="R180" align="center-left">SBU1</text>
<text x="13.97" y="-12.7" size="1.016" layer="97" font="vector" rot="R180" align="center-left">SBU2</text>
<text x="13.97" y="-30.48" size="1.016" layer="97" font="vector" rot="R180" align="center-left">SHLD</text>
<text x="13.97" y="-5.08" size="1.016" layer="97" font="vector" rot="R180" align="center-left">VBUS</text>
<pin name="CC1" x="17.78" y="-15.24" visible="off" length="short" rot="R180"/>
<pin name="CC2" x="17.78" y="-17.78" visible="off" length="short" rot="R180"/>
<text x="13.97" y="-15.24" size="1.016" layer="97" font="vector" rot="R180" align="center-left">CC1</text>
<text x="13.97" y="-17.78" size="1.016" layer="97" font="vector" rot="R180" align="center-left">CC2</text>
</symbol>
<symbol name="V-REG-APA2112" library_version="85">
<description>&lt;h3&gt; Voltage Regulator - No bypass&lt;/h3&gt;
5  pin, with Enable function.</description>
<wire x1="-7.62" y1="-7.62" x2="5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-7.62" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<text x="-7.62" y="7.874" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-7.62" y="-7.874" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="VIN" x="-10.16" y="5.08" visible="pin" length="short" direction="pwr"/>
<pin name="GND" x="-10.16" y="-5.08" visible="pin" length="short" direction="pwr"/>
<pin name="OUT" x="7.62" y="5.08" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="EN" x="-10.16" y="0" visible="pin" length="short"/>
<pin name="NC" x="7.62" y="-5.08" visible="pin" length="short" direction="nc" rot="R180"/>
</symbol>
<symbol name="PINHD2" library_version="92">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="POS" x="-2.54" y="2.54" visible="pad" length="short" direction="pwr" function="dot"/>
<pin name="NEG" x="-2.54" y="0" visible="pad" length="short" direction="pwr" function="dot"/>
<text x="1.778" y="2.032" size="1.27" layer="94">+</text>
<text x="2.032" y="-0.508" size="1.27" layer="94">-</text>
</symbol>
<symbol name="I2C_STANDARD-1" library_version="100">
<description>&lt;h3&gt;SparkFun I&lt;sup&gt;2&lt;/sup&gt;C Standard Pinout Header&lt;/h3&gt;
&lt;p&gt;SparkFun has standardized on a pinout for all I&lt;sup&gt;2&lt;/sup&gt;C based sensor breakouts.&lt;br&gt;</description>
<wire x1="3.81" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-5.334" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<text x="-5.08" y="7.874" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-4.572" y="2.54" size="1.778" layer="94" font="vector" align="center-left">SDA</text>
<text x="-4.572" y="0" size="1.778" layer="94" font="vector" align="center-left">VCC</text>
<text x="-4.572" y="-2.54" size="1.778" layer="94" font="vector" align="center-left">GND</text>
<text x="-4.572" y="5.08" size="1.778" layer="94" font="vector" align="center-left">SCL</text>
<pin name="GND" x="7.62" y="-2.54" visible="pad" length="middle" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="VCC" x="7.62" y="0" visible="pad" length="middle" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="SDA" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="SCL" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="ESP32-S2-WROVER" library_version="130">
<wire x1="-22.86" y1="33.02" x2="22.86" y2="33.02" width="0.254" layer="94"/>
<wire x1="22.86" y1="33.02" x2="22.86" y2="-35.56" width="0.254" layer="94"/>
<wire x1="22.86" y1="-35.56" x2="-22.86" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-35.56" x2="-22.86" y2="33.02" width="0.254" layer="94"/>
<text x="-22.6559" y="34.1345" size="1.778" layer="95">&gt;NAME</text>
<text x="-22.9067" y="-38.2081" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="27.94" y="-33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="3V3" x="27.94" y="30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="EN" x="-27.94" y="25.4" length="middle" direction="in"/>
<pin name="IO34" x="27.94" y="-5.08" length="middle" direction="in" rot="R180"/>
<pin name="IO35" x="27.94" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="IO33" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="IO26/RESERVED/PSRAM" x="27.94" y="0" length="middle" rot="R180"/>
<pin name="IO14" x="-27.94" y="-25.4" length="middle"/>
<pin name="IO12" x="-27.94" y="-20.32" length="middle"/>
<pin name="IO13" x="-27.94" y="-22.86" length="middle"/>
<pin name="IO15" x="27.94" y="17.78" length="middle" rot="R180"/>
<pin name="IO2" x="-27.94" y="5.08" length="middle"/>
<pin name="IO0_BOOT0" x="-27.94" y="12.7" length="middle"/>
<pin name="IO4" x="-27.94" y="0" length="middle"/>
<pin name="IO16" x="27.94" y="15.24" length="middle" rot="R180"/>
<pin name="IO17/DAC1" x="27.94" y="12.7" length="middle" rot="R180"/>
<pin name="IO5" x="-27.94" y="-2.54" length="middle"/>
<pin name="IO18/DAC2" x="27.94" y="10.16" length="middle" rot="R180"/>
<pin name="IO19/D-" x="27.94" y="7.62" length="middle" rot="R180"/>
<pin name="IO21" x="27.94" y="2.54" length="middle" rot="R180"/>
<pin name="RXD0" x="27.94" y="22.86" length="middle" rot="R180"/>
<pin name="TXD0" x="27.94" y="25.4" length="middle" rot="R180"/>
<pin name="IO1" x="-27.94" y="7.62" length="middle"/>
<pin name="IO3" x="-27.94" y="2.54" length="middle"/>
<pin name="IO6" x="-27.94" y="-5.08" length="middle"/>
<pin name="IO7" x="-27.94" y="-7.62" length="middle"/>
<pin name="IO8" x="-27.94" y="-10.16" length="middle"/>
<pin name="IO9" x="-27.94" y="-12.7" length="middle"/>
<pin name="IO10" x="-27.94" y="-15.24" length="middle"/>
<pin name="IO11" x="-27.94" y="-17.78" length="middle"/>
<pin name="IO20/D+" x="27.94" y="5.08" length="middle" rot="R180"/>
<pin name="IO36" x="27.94" y="-10.16" length="middle" rot="R180"/>
<pin name="IO37" x="27.94" y="-12.7" length="middle" rot="R180"/>
<pin name="IO38" x="27.94" y="-15.24" length="middle" rot="R180"/>
<pin name="IO39/TCK" x="27.94" y="-17.78" length="middle" rot="R180"/>
<pin name="IO40/TDO" x="27.94" y="-20.32" length="middle" rot="R180"/>
<pin name="IO41/TDI" x="27.94" y="-22.86" length="middle" rot="R180"/>
<pin name="IO42/TMS" x="27.94" y="-25.4" length="middle" rot="R180"/>
<pin name="IO45_SPIV" x="-27.94" y="15.24" length="middle"/>
<pin name="IO46_DEBUGOUT" x="-27.94" y="17.78" length="middle" direction="in"/>
<pin name="EXP" x="27.94" y="-30.48" length="middle" direction="pwr" rot="R180"/>
</symbol>
<symbol name="WS2812BLED" library_version="131">
<pin name="VDD" x="5.08" y="15.24" visible="pin" length="middle" direction="pwr" rot="R270"/>
<pin name="DI" x="-12.7" y="-2.54" visible="pin" length="middle" direction="in"/>
<pin name="GND" x="0" y="-10.16" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="DO" x="12.7" y="-2.54" visible="pin" length="middle" direction="out" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-6.35" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-4.064" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<text x="-4.064" y="8.382" size="1.27" layer="94">WS2812B</text>
</symbol>
<symbol name="ISL6292" library_version="138">
<pin name="VIN" x="0" y="25.4" length="middle" direction="pwr"/>
<pin name="VBAT" x="30.48" y="25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="0" y="5.08" length="middle" direction="pwr"/>
<pin name="FAULT" x="0" y="12.7" length="middle"/>
<pin name="STATUS" x="0" y="10.16" length="middle"/>
<pin name="IMIN" x="30.48" y="12.7" length="middle" rot="R180"/>
<pin name="IREF" x="30.48" y="10.16" length="middle" rot="R180"/>
<pin name="TEMP" x="30.48" y="22.86" length="middle" rot="R180"/>
<pin name="TIME" x="30.48" y="7.62" length="middle" rot="R180"/>
<pin name="V2P8" x="30.48" y="5.08" length="middle" direction="pwr" rot="R180"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="26.924" width="0.127" layer="94"/>
<wire x1="5.08" y1="26.924" x2="5.08" y2="33.782" width="0.127" layer="94"/>
<wire x1="5.08" y1="33.782" x2="25.4" y2="33.782" width="0.127" layer="94"/>
<wire x1="25.4" y1="33.782" x2="25.4" y2="26.924" width="0.127" layer="94"/>
<wire x1="25.4" y1="26.924" x2="25.4" y2="2.54" width="0.127" layer="94"/>
<wire x1="25.4" y1="2.54" x2="5.08" y2="2.54" width="0.127" layer="94"/>
<text x="7.62" y="27.94" size="1.27" layer="94" font="vector">ISL6292
4.2V/2.1A Li-Ion
Charger</text>
<wire x1="5.08" y1="26.924" x2="25.4" y2="26.924" width="0.127" layer="94"/>
<pin name="ENABLE" x="0" y="22.86" length="middle"/>
<pin name="TIMEOUT" x="0" y="20.32" length="middle"/>
<pin name="PAD" x="0" y="7.62" length="middle" direction="nc"/>
</symbol>
<symbol name="TSU" library_version="171">
<wire x1="0" y1="-3.175" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="3.175" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.175" width="0.254" layer="94"/>
<wire x1="-0.889" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="1.905" x2="-5.08" y2="1.905" width="0.254" layer="94"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="0" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-1.905" x2="-5.08" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-6.35" y1="0" x2="-4.445" y2="0" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="-1.905" width="0.254" layer="94"/>
<text x="-8.255" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-5.715" y="2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S" x="2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="O" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="MT" library_version="171">
<pin name="MT" x="0" y="0" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BNO080" prefix="U" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;BNO080&lt;/b&gt; - Motion Co-Processor&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="BNO080" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BNO080">
<connects>
<connect gate="G$1" pin="BOOTN" pad="4"/>
<connect gate="G$1" pin="CAP" pad="9"/>
<connect gate="G$1" pin="CLKSEL0" pad="10"/>
<connect gate="G$1" pin="CLKSEL1/XOUT32" pad="26"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="GNDIO" pad="25"/>
<connect gate="G$1" pin="H_CSN" pad="18"/>
<connect gate="G$1" pin="H_INTN" pad="14"/>
<connect gate="G$1" pin="H_SA0/MOSI" pad="17"/>
<connect gate="G$1" pin="H_SCL/SCLK/RX" pad="19"/>
<connect gate="G$1" pin="H_SDA/MISO/TX" pad="20"/>
<connect gate="G$1" pin="NRST" pad="11"/>
<connect gate="G$1" pin="PS0/WAKE" pad="6"/>
<connect gate="G$1" pin="PS1" pad="5"/>
<connect gate="G$1" pin="S_SCL" pad="15"/>
<connect gate="G$1" pin="S_SDA" pad="16"/>
<connect gate="G$1" pin="VDD" pad="3"/>
<connect gate="G$1" pin="VDDIO" pad="28"/>
<connect gate="G$1" pin="XIN32" pad="27"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.j03Sz7myTxW003tCMTxK2w?version=2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PN" value="1888-1006-1-ND" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB4105-GF-A" prefix="J">
<description>USB-C (USB TYPE-C) USB 2.0 Receptacle Connector 24 (16+8 Dummy) Position Surface Mount, Right Angle; Through Hole  &lt;a href="https://pricing.snapeda.com/parts/USB4105-GF-A/GCT/view-part?ref=eda"&gt;Check prices&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="USB_TYPE-C_MINIMAL" x="-7.62" y="17.78"/>
</gates>
<devices>
<device name="" package="GCT_USB4105-GF-A">
<connects>
<connect gate="G$1" pin="CC1" pad="A5"/>
<connect gate="G$1" pin="CC2" pad="B5"/>
<connect gate="G$1" pin="D+" pad="A6 B6"/>
<connect gate="G$1" pin="D-" pad="A7 B7" route="any"/>
<connect gate="G$1" pin="GND" pad="A1-B12 A12-B1"/>
<connect gate="G$1" pin="SBU1" pad="A8"/>
<connect gate="G$1" pin="SBU2" pad="B8"/>
<connect gate="G$1" pin="SHLD" pad="P$1 P$2 P$3 P$5 P$6 P$7 P$8 P$9"/>
<connect gate="G$1" pin="VBUS" pad="A4-B9 B4-A9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.m2fzCX6STRWF-vn2nMNhaA?version=5"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PN" value="670-DX07S016JA1R1500CT-ND"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AP2112-V3.3" prefix="U" library_version="85">
<description>&lt;h3&gt;AP2112 - 600mA CMOS LDO Regulator w/ Enable&lt;/h3&gt;
&lt;p&gt;The AP2112 is CMOS process low dropout linear regulator with enable function, the regulator delivers a guaranteed 600mA (min.) continuous load current.&lt;/p&gt;
&lt;p&gt;Features&lt;br&gt;
&lt;ul&gt;
&lt;li&gt;Output Voltage Accuracy: ±1.5% &lt;/li&gt;
&lt;li&gt;Output Current: 600mA (Min.) &lt;/li&gt;
&lt;li&gt;Foldback Short Current Protection: 50mA &lt;/li&gt;
&lt;li&gt;Enable Function to Turn ON/OFF VOUT&lt;/li&gt;
&lt;li&gt;Low Dropout Voltage (3.3V): 250mV (Typ.) @IOUT=600mA &lt;/li&gt;
&lt;li&gt;Excellent Load Regulation: 0.2%/A (Typ.) &lt;/li&gt;
&lt;li&gt;Excellent Line Regulation: 0.02%/V (Typ.) &lt;/li&gt;
&lt;li&gt;Low Quiescent Current: 55μA (Typ.)&lt;/li&gt;
&lt;li&gt;Low Standby Current: 0.01μA (Typ.)&lt;/li&gt;
&lt;li&gt;Low Output Noise: 50μVRMS &lt;/li&gt;
&lt;li&gt;PSRR: 100Hz -65dB, 1kHz -65dB &lt;/li&gt;
&lt;li&gt; OTSD Protection &lt;/li&gt;
&lt;li&gt;Stable  with  1.0μF Flexible Cap: Ceramic, Tantalum and Aluminum Electrolytic &lt;/li&gt;
&lt;li&gt;Operation Temperature Range: -40°C to 85°C &lt;/li&gt;
&lt;li&gt;ESD: MM 400V, HBM 4000V&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="V-REG-APA2112" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5L">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="NC" pad="4"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="VIN" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.FlSLHj1wRFKTmmfP59244Q?version=2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST_2PIN" prefix="CN" uservalue="yes">
<description>&lt;b&gt;JST 2-Pin Connectors of various flavors&lt;/b&gt;

&lt;ul&gt;
&lt;li&gt;SMT-RA (S2B-PH-SM4) 4UConnector #17311&lt;/li&gt;
&lt;li&gt;SMT  (B2B-PH-SM4)&lt;/li&gt;
&lt;li&gt;THM-RA (S2B-PH)&lt;/li&gt;
&lt;li&gt;THM  (B2B-PH)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="JST-PH-2-SMT">
<connects>
<connect gate="G$1" pin="NEG" pad="1"/>
<connect gate="G$1" pin="POS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.ELgLTILvRsqCS-OhAXe6AQ?version=2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="QWIIC_CONNECTOR" prefix="J" uservalue="yes" library_version="100">
<description>&lt;h3&gt;SparkFun I&lt;sup&gt;2&lt;/sup&gt;C Standard Qwiic Connector&lt;/h3&gt;
An SMD 1mm pitch JST connector makes it easy and quick (get it? Qwiic?) to connect I&lt;sup&gt;2&lt;/sup&gt;C devices to each other. The &lt;a href=”http://www.sparkfun.com/qwiic”&gt;Qwiic system&lt;/a&gt; enables fast and solderless connection between popular platforms and various sensors and actuators.

&lt;br&gt;&lt;br&gt;

We carry &lt;a href=”https://www.sparkfun.com/products/14204”&gt;200mm&lt;/a&gt;, &lt;a href=”https://www.sparkfun.com/products/14205”&gt;100mm&lt;/a&gt;, &lt;a href=”https://www.sparkfun.com/products/14206”&gt;50mm&lt;/a&gt;, and &lt;a href=”https://www.sparkfun.com/products/14207”&gt;breadboard friendly&lt;/a&gt; Qwiic cables. We also offer &lt;a href=”https://www.sparkfun.com/products/14323”&gt;10 pcs strips&lt;/a&gt; the SMD connectors.</description>
<gates>
<gate name="J1" symbol="I2C_STANDARD-1" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X04_1MM_RA">
<connects>
<connect gate="J1" pin="GND" pad="1"/>
<connect gate="J1" pin="SCL" pad="4"/>
<connect gate="J1" pin="SDA" pad="3"/>
<connect gate="J1" pin="VCC" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.KLYt6sxhTXiqlVjlj-H4zw?version=2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESP32-S2-WROVER" prefix="U" library_version="130">
<description>WiFi 802.11b/g/n Transceiver Module 2.4GHz Antenna Not Included Surface Mount &lt;a href="https://snapeda.com/parts/ESP32-S2-WROVER/Espressif%20Systems/view-part/?ref=eda"&gt;Check prices&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ESP32-S2-WROVER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MODULE_ESP32-S2-WROVER">
<connects>
<connect gate="G$1" pin="3V3" pad="2"/>
<connect gate="G$1" pin="EN" pad="41"/>
<connect gate="G$1" pin="EXP" pad="43_1 43_2 43_3 43_4 43_5 43_6 43_7 43_8 43_9 43_10 43_11 43_12 43_13 43_14 43_15 43_16 43_17 43_18 43_19 43_20 43_21"/>
<connect gate="G$1" pin="GND" pad="1 26 42"/>
<connect gate="G$1" pin="IO0_BOOT0" pad="3"/>
<connect gate="G$1" pin="IO1" pad="4"/>
<connect gate="G$1" pin="IO10" pad="13"/>
<connect gate="G$1" pin="IO11" pad="14"/>
<connect gate="G$1" pin="IO12" pad="15"/>
<connect gate="G$1" pin="IO13" pad="16"/>
<connect gate="G$1" pin="IO14" pad="17"/>
<connect gate="G$1" pin="IO15" pad="18"/>
<connect gate="G$1" pin="IO16" pad="19"/>
<connect gate="G$1" pin="IO17/DAC1" pad="20"/>
<connect gate="G$1" pin="IO18/DAC2" pad="21"/>
<connect gate="G$1" pin="IO19/D-" pad="22"/>
<connect gate="G$1" pin="IO2" pad="5"/>
<connect gate="G$1" pin="IO20/D+" pad="23"/>
<connect gate="G$1" pin="IO21" pad="24"/>
<connect gate="G$1" pin="IO26/RESERVED/PSRAM" pad="25"/>
<connect gate="G$1" pin="IO3" pad="6"/>
<connect gate="G$1" pin="IO33" pad="27"/>
<connect gate="G$1" pin="IO34" pad="28"/>
<connect gate="G$1" pin="IO35" pad="29"/>
<connect gate="G$1" pin="IO36" pad="30"/>
<connect gate="G$1" pin="IO37" pad="31"/>
<connect gate="G$1" pin="IO38" pad="32"/>
<connect gate="G$1" pin="IO39/TCK" pad="33"/>
<connect gate="G$1" pin="IO4" pad="7"/>
<connect gate="G$1" pin="IO40/TDO" pad="34"/>
<connect gate="G$1" pin="IO41/TDI" pad="35"/>
<connect gate="G$1" pin="IO42/TMS" pad="36"/>
<connect gate="G$1" pin="IO45_SPIV" pad="39"/>
<connect gate="G$1" pin="IO46_DEBUGOUT" pad="40"/>
<connect gate="G$1" pin="IO5" pad="8"/>
<connect gate="G$1" pin="IO6" pad="9"/>
<connect gate="G$1" pin="IO7" pad="10"/>
<connect gate="G$1" pin="IO8" pad="11"/>
<connect gate="G$1" pin="IO9" pad="12"/>
<connect gate="G$1" pin="RXD0" pad="38"/>
<connect gate="G$1" pin="TXD0" pad="37"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.MgkoGtQ7RGWIVI9TGL5JwA?version=2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Warning"/>
<attribute name="DESCRIPTION" value=" WiFi 802.11b/g/n Transceiver Module 2.4GHz Antenna Not Included Surface Mount "/>
<attribute name="DIGIKEY_PN" value="1965-ESP32-S2-WROVER-ICT-ND"/>
<attribute name="MF" value="Espressif Systems"/>
<attribute name="MP" value="ESP32-S2-WROVER"/>
<attribute name="PACKAGE" value="VFQFN-56 Espressif Systems"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WS2812B" prefix="LED" library_version="166">
<gates>
<gate name="G$1" symbol="WS2812BLED" x="0" y="-2.54"/>
</gates>
<devices>
<device name="_5050N" package="WS2812B-NARROW">
<connects>
<connect gate="G$1" pin="DI" pad="4-DIN"/>
<connect gate="G$1" pin="DO" pad="2-DOUT"/>
<connect gate="G$1" pin="GND" pad="3-GND"/>
<connect gate="G$1" pin="VDD" pad="1-VDD"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240961/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3535" package="LED3535">
<connects>
<connect gate="G$1" pin="DI" pad="1"/>
<connect gate="G$1" pin="DO" pad="3"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="VDD" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.LFJRaa0_RZWZKQyEjbEAwg?version=3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="WS2812B">
<connects>
<connect gate="G$1" pin="DI" pad="4-DIN"/>
<connect gate="G$1" pin="DO" pad="2-DOUT"/>
<connect gate="G$1" pin="GND" pad="3-GND"/>
<connect gate="G$1" pin="VDD" pad="1-VDD"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.1ZJw8vIvSD6LvefTG9WiAA?version=2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SK6812" package="SK6812">
<connects>
<connect gate="G$1" pin="DI" pad="2.DIN"/>
<connect gate="G$1" pin="DO" pad="4.DOUT"/>
<connect gate="G$1" pin="GND" pad="1.VSS"/>
<connect gate="G$1" pin="VDD" pad="3.VDD"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.F-iA7yWuT7-p5_ne7S9zIw?version=4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ISL6292" library_version="138">
<gates>
<gate name="G$1" symbol="ISL6292" x="0" y="0"/>
</gates>
<devices>
<device name="" package="16-LD-QFN">
<connects>
<connect gate="G$1" pin="ENABLE" pad="7"/>
<connect gate="G$1" pin="FAULT" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="IMIN" pad="10"/>
<connect gate="G$1" pin="IREF" pad="9"/>
<connect gate="G$1" pin="PAD" pad="17"/>
<connect gate="G$1" pin="STATUS" pad="3"/>
<connect gate="G$1" pin="TEMP" pad="11"/>
<connect gate="G$1" pin="TIME" pad="4"/>
<connect gate="G$1" pin="TIMEOUT" pad="6"/>
<connect gate="G$1" pin="V2P8" pad="8"/>
<connect gate="G$1" pin="VBAT" pad="12 13 14"/>
<connect gate="G$1" pin="VIN" pad="1 15 16"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.rYaKax5CTIqFnK3WFbQwkw?version=2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EG1213" prefix="S" library_version="171">
<description>&lt;b&gt;E-Switch right-angle slide SPDT&lt;/b&gt; Part #EG1213
&lt;p&gt;http://www.ladyada.net/library/eagle&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="TSU" x="0" y="0"/>
<gate name="G$1" symbol="MT" x="-7.62" y="-10.16"/>
<gate name="G$2" symbol="MT" x="-7.62" y="-12.7"/>
</gates>
<devices>
<device name="STANDARD" package="EG1213">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
<connect gate="G$1" pin="MT" pad="MT1"/>
<connect gate="G$2" pin="MT" pad="MT2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.B2GuiAVbSwiZekg0hz0_Jg?version=2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors" urn="urn:adsk.eagle:library:513">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SAMTECH_FTSH-105-01" urn="urn:adsk.eagle:footprint:37965/1" library_version="1">
<description>&lt;h3&gt;ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.05"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.samtec.com/ftppub/cpdf/FTSH-1XX-XX-XXX-DV-XXX-MKT.pdf"&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CORTEX_DEBUG&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.175" y1="1.7145" x2="3.175" y2="-1.7145" width="0.127" layer="51"/>
<wire x1="3.175" y1="-1.7145" x2="-3.175" y2="-1.7145" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.7145" x2="-3.175" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="1.7145" x2="3.175" y2="1.7145" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.7145" x2="-3.175" y2="1.7145" width="0.127" layer="51"/>
<wire x1="-3.175" y1="1.7145" x2="-3.0226" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="3.0226" y1="1.7145" x2="3.175" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.7145" x2="3.175" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.7145" x2="3.0226" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="-3.0226" y1="-1.7145" x2="-3.175" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="-2.0574" y1="1.7145" x2="-1.7526" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="-0.7874" y1="1.7145" x2="-0.4826" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="0.4826" y1="1.7145" x2="0.7874" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="1.7526" y1="1.7145" x2="2.0574" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="2.0574" y1="-1.7145" x2="1.7526" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="0.7874" y1="-1.7145" x2="0.4826" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="-0.4826" y1="-1.7145" x2="-0.7874" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="-1.7526" y1="-1.7145" x2="-2.0574" y2="-1.7145" width="0.2032" layer="21"/>
<rectangle x1="-0.2032" y1="1.7145" x2="0.2032" y2="2.921" layer="51"/>
<rectangle x1="1.0668" y1="1.7145" x2="1.4732" y2="2.921" layer="51"/>
<rectangle x1="2.3368" y1="1.7145" x2="2.7432" y2="2.921" layer="51"/>
<rectangle x1="-1.4732" y1="1.7145" x2="-1.0668" y2="2.921" layer="51"/>
<rectangle x1="-2.7432" y1="1.7145" x2="-2.3368" y2="2.921" layer="51"/>
<rectangle x1="-0.2032" y1="-2.921" x2="0.2032" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="-1.4732" y1="-2.921" x2="-1.0668" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="-2.7432" y1="-2.921" x2="-2.3368" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="1.0668" y1="-2.921" x2="1.4732" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="2.3368" y1="-2.921" x2="2.7432" y2="-1.7145" layer="51" rot="R180"/>
<smd name="6" x="0" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="8" x="1.27" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="10" x="2.54" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="4" x="-1.27" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="2" x="-2.54" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="1" x="-2.54" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="3" x="-1.27" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="5" x="0" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="7" x="1.27" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="9" x="2.54" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<text x="-1.3462" y="0.4572" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.7018" y="-0.9652" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-PTH-1.27MM" urn="urn:adsk.eagle:footprint:37966/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x5 ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;tDoc (51) layer border represents maximum dimensions of plastic housing.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://portal.fciconnect.com/Comergent//fci/drawing/20021111.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="8" x="1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="6" x="0" y="0.635" drill="0.508" diameter="1"/>
<pad name="4" x="-1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="2" x="-2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="10" x="2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="7" x="1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="5" x="0" y="-0.635" drill="0.508" diameter="1"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.508" diameter="1"/>
<pad name="9" x="2.54" y="-0.635" drill="0.508" diameter="1"/>
<wire x1="-3.403" y1="-1.021" x2="-3.403" y2="-0.259" width="0.254" layer="21"/>
<wire x1="3.175" y1="1.715" x2="-3.175" y2="1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="1.715" x2="-3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.715" x2="3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="3.175" y1="-1.715" x2="3.175" y2="1.715" width="0.127" layer="51"/>
<text x="-1.5748" y="1.9304" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.8288" y="-2.4638" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SAMTECH_FTSH-105-01" urn="urn:adsk.eagle:package:38289/1" type="box" library_version="1">
<description>ARM Cortex Debug Connector (10-pin)
Specifications:
Pin count:10
Pin pitch:0.05"

Datasheet referenced for footprint
Example device(s):
CORTEX_DEBUG
</description>
<packageinstances>
<packageinstance name="SAMTECH_FTSH-105-01"/>
</packageinstances>
</package3d>
<package3d name="2X5-PTH-1.27MM" urn="urn:adsk.eagle:package:38290/1" type="box" library_version="1">
<description>Plated Through Hole - 2x5 ARM Cortex Debug Connector (10-pin)
tDoc (51) layer border represents maximum dimensions of plastic housing.
Specifications:
Pin count:10
Pin pitch:1.27mm

Datasheet referenced for footprint
Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5-PTH-1.27MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CORTEX_DEBUG" urn="urn:adsk.eagle:symbol:37964/1" library_version="1">
<description>&lt;h3&gt;Cortex Debug Connector&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<pin name="VCC" x="-15.24" y="5.08" length="short"/>
<pin name="GND@3" x="-15.24" y="2.54" length="short"/>
<pin name="GND@5" x="-15.24" y="0" length="short"/>
<pin name="KEY" x="-15.24" y="-2.54" length="short"/>
<pin name="GNDDTCT" x="-15.24" y="-5.08" length="short"/>
<pin name="!RESET" x="17.78" y="-5.08" length="short" rot="R180"/>
<pin name="NC/TDI" x="17.78" y="-2.54" length="short" rot="R180"/>
<pin name="SWO/TDO" x="17.78" y="0" length="short" rot="R180"/>
<pin name="SWDCLK/TCK" x="17.78" y="2.54" length="short" rot="R180"/>
<pin name="SWDIO/TMS" x="17.78" y="5.08" length="short" rot="R180"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="15.24" y2="7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="7.62" x2="15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<text x="-12.7" y="7.874" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-12.7" y="-9.906" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CORTEX_DEBUG" urn="urn:adsk.eagle:component:38384/1" prefix="J" library_version="1">
<description>&lt;h3&gt;Cortex Debug Connector - 10 pin&lt;/h3&gt;
&lt;p&gt;Supports JTAG debug, Serial Wire debug, and Serial Wire Viewer.
PTH and SMD connector options available.&lt;/p&gt;
&lt;p&gt; &lt;ul&gt;&lt;a href=”http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf”&gt;General Connector Information&lt;/a&gt;
&lt;p&gt;&lt;b&gt; Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”http://www.digikey.com/product-detail/en/cnc-tech/3220-10-0100-00/1175-1627-ND/3883661”&gt;PTH Connector&lt;/a&gt; -via Digi-Key&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13229”&gt;SparkFun PSoc&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13810”&gt;SparkFun T&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CORTEX_DEBUG" x="0" y="0"/>
</gates>
<devices>
<device name="_SMD" package="SAMTECH_FTSH-105-01">
<connects>
<connect gate="G$1" pin="!RESET" pad="10"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GNDDTCT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38289/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_PTH" package="2X5-PTH-1.27MM">
<connects>
<connect gate="G$1" pin="!RESET" pad="10"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GNDDTCT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38290/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LED" urn="urn:adsk.eagle:library:22900745">
<description>&lt;B&gt;LED parts CHIP-Flat Top, Round Top</description>
<packages>
<package name="LEDC1608X65N_RND-R" urn="urn:adsk.eagle:footprint:24294738/1" library_version="7">
<description>Chip LED, 0.80 X 1.60 X 0.65 mm body
 &lt;p&gt;Chip LED package with body size 0.80 X 1.60 X 0.65 mm&lt;/p&gt;</description>
<smd name="C" x="-0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<wire x1="-1.3099" y1="0.7699" x2="0.8" y2="0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="0.7699" x2="-1.3099" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="-0.7699" x2="0.8" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.12" layer="51"/>
<text x="0" y="1.4049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC1608X95N_RND-R" urn="urn:adsk.eagle:footprint:24294740/1" library_version="7">
<description>Chip LED, 0.80 X 1.60 X 0.95 mm body
 &lt;p&gt;Chip LED package with body size 0.80 X 1.60 X 0.95 mm&lt;/p&gt;

Ref : https://www.kingbrightusa.com/images/catalog/SPEC/APTD1608QBC-D.pdf</description>
<smd name="C" x="-0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<wire x1="-1.3099" y1="0.7699" x2="0.8" y2="0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="0.7699" x2="-1.3099" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="-0.7699" x2="0.8" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.12" layer="51"/>
<text x="0" y="1.4049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC2012X105N_RND-R" urn="urn:adsk.eagle:footprint:24294741/1" library_version="7">
<description>Chip LED, 2.00 X 1.25 X 1.05 mm body
 &lt;p&gt;Chip LED package with body size 2.00 X 1.25 X 1.05 mm&lt;/p&gt;</description>
<smd name="C" x="-0.975" y="0" dx="0.8618" dy="1.3618" layer="1"/>
<smd name="A" x="0.975" y="0" dx="0.8618" dy="1.3618" layer="1"/>
<wire x1="-1.6599" y1="0.9949" x2="1" y2="0.9949" width="0.12" layer="21"/>
<wire x1="-1.6599" y1="0.9949" x2="-1.6599" y2="-0.9949" width="0.12" layer="21"/>
<wire x1="-1.6599" y1="-0.9949" x2="1" y2="-0.9949" width="0.12" layer="21"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.12" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.12" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.12" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.12" layer="51"/>
<text x="-0.127" y="1.6299" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-0.127" y="-1.6299" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC3216X180N_RND-R" urn="urn:adsk.eagle:footprint:24294743/1" library_version="7">
<description>Chip LED, 3.20 X 1.60 X 1.80 mm body
 &lt;p&gt;Chip LED package with body size 3.20 X 1.60 X 1.80 mm&lt;/p&gt;</description>
<smd name="C" x="-1.525" y="0" dx="0.9618" dy="1.7118" layer="1"/>
<smd name="A" x="1.525" y="0" dx="0.9618" dy="1.7118" layer="1"/>
<wire x1="-2.2599" y1="1.1699" x2="1.6" y2="1.1699" width="0.12" layer="21"/>
<wire x1="-2.2599" y1="1.1699" x2="-2.2599" y2="-1.1699" width="0.12" layer="21"/>
<wire x1="-2.2599" y1="-1.1699" x2="1.6" y2="-1.1699" width="0.12" layer="21"/>
<wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.6" y1="0.8" x2="1.6" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.12" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="-1.6" y2="-0.8" width="0.12" layer="51"/>
<text x="-0.127" y="1.8049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-0.127" y="-1.8049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC3224X250N_RND-R" urn="urn:adsk.eagle:footprint:24294745/1" library_version="7">
<description>Chip LED, 3.20 X 2.40 X 2.50 mm body
 &lt;p&gt;Chip LED package with body size 3.20 X 2.40 X 2.50 mm&lt;/p&gt;</description>
<smd name="C" x="-1.525" y="0" dx="0.9618" dy="2.5118" layer="1"/>
<smd name="A" x="1.525" y="0" dx="0.9618" dy="2.5118" layer="1"/>
<wire x1="-2.2599" y1="1.5699" x2="1.6" y2="1.5699" width="0.12" layer="21"/>
<wire x1="-2.2599" y1="1.5699" x2="-2.2599" y2="-1.5699" width="0.12" layer="21"/>
<wire x1="-2.2599" y1="-1.5699" x2="1.6" y2="-1.5699" width="0.12" layer="21"/>
<wire x1="-1.6" y1="-1.2" x2="-1.6" y2="1.2" width="0.12" layer="51"/>
<wire x1="-1.6" y1="1.2" x2="1.6" y2="1.2" width="0.12" layer="51"/>
<wire x1="1.6" y1="1.2" x2="1.6" y2="-1.2" width="0.12" layer="51"/>
<wire x1="1.6" y1="-1.2" x2="-1.6" y2="-1.2" width="0.12" layer="51"/>
<text x="-0.127" y="2.2049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-0.127" y="-2.2049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="LEDC1608X65N_RND-R" urn="urn:adsk.eagle:package:24294800/1" wip_urn="urn:adsk.wipprod:fs.file:vf.n1ru9s2WSdGmSoMZDXS2cg?version=1" type="model" library_version="7">
<description>Chip LED, 0.80 X 1.60 X 0.65 mm body
 &lt;p&gt;Chip LED package with body size 0.80 X 1.60 X 0.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC1608X65N_RND-R"/>
</packageinstances>
</package3d>
<package3d name="LEDC1608X95N_RND-R" urn="urn:adsk.eagle:package:24294804/1" wip_urn="urn:adsk.wipprod:fs.file:vf.mTFnh-NETk-dsQqn1o-rrA?version=1" type="model" library_version="7">
<description>Chip LED, 0.80 X 1.60 X 0.95 mm body
 &lt;p&gt;Chip LED package with body size 0.80 X 1.60 X 0.95 mm&lt;/p&gt;

Ref : https://www.kingbrightusa.com/images/catalog/SPEC/APTD1608QBC-D.pdf</description>
<packageinstances>
<packageinstance name="LEDC1608X95N_RND-R"/>
</packageinstances>
</package3d>
<package3d name="LEDC2012X105N_RND-R" urn="urn:adsk.eagle:package:24294805/1" wip_urn="urn:adsk.wipprod:fs.file:vf.DyQ-SXPGR1mR7-8goJM-gA?version=1" type="model" library_version="7">
<description>Chip LED, 2.00 X 1.25 X 1.05 mm body
 &lt;p&gt;Chip LED package with body size 2.00 X 1.25 X 1.05 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC2012X105N_RND-R"/>
</packageinstances>
</package3d>
<package3d name="LEDC3216X180N_RND-R" urn="urn:adsk.eagle:package:24294808/1" wip_urn="urn:adsk.wipprod:fs.file:vf.3Q1wa-ohSpmICe6RKd0phw?version=1" type="model" library_version="7">
<description>Chip LED, 3.20 X 1.60 X 1.80 mm body
 &lt;p&gt;Chip LED package with body size 3.20 X 1.60 X 1.80 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC3216X180N_RND-R"/>
</packageinstances>
</package3d>
<package3d name="LEDC3224X250N_RND-R" urn="urn:adsk.eagle:package:24294812/1" wip_urn="urn:adsk.wipprod:fs.file:vf.2cvZoe7CQlaHvHEL-OW1fA?version=1" type="model" library_version="7">
<description>Chip LED, 3.20 X 2.40 X 2.50 mm body
 &lt;p&gt;Chip LED package with body size 3.20 X 2.40 X 2.50 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC3224X250N_RND-R"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LED" urn="urn:adsk.eagle:symbol:22900757/3" locally_modified="yes" library_version="7">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="-0.762" size="1.778" layer="95" rot="R180" align="top-right">&gt;NAME</text>
<text x="2.54" y="-3.302" size="1.778" layer="96" rot="R180" align="top-right">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygonshape layer="94">
<polygonoutlineobjects>
<vertex x="-2.406534375" y="-1.803703125"/>
<vertex x="-3.073703125" y="-1.136534375"/>
<vertex x="-3.5193625" y="-2.176403125"/>
<vertex x="-3.446403125" y="-2.2493625"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="-2.406534375" y="-1.803703125"/>
<vertex x="-3.073703125" y="-1.136534375"/>
<vertex x="-3.5193625" y="-2.176403125"/>
<vertex x="-3.446403125" y="-2.2493625"/>
<vertex x="-2.406534375" y="-1.803703125"/>
</polygonoutlinesegments>
</polygonshape>
<polygonshape layer="94">
<polygonoutlineobjects>
<vertex x="-2.279534375" y="-2.946703125"/>
<vertex x="-2.946703125" y="-2.279534375"/>
<vertex x="-3.3923625" y="-3.319403125"/>
<vertex x="-3.319403125" y="-3.3923625"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="-2.279534375" y="-2.946703125"/>
<vertex x="-2.946703125" y="-2.279534375"/>
<vertex x="-3.3923625" y="-3.319403125"/>
<vertex x="-3.319403125" y="-3.3923625"/>
<vertex x="-2.279534375" y="-2.946703125"/>
</polygonoutlinesegments>
</polygonshape>
</symbol>
</symbols>
<devicesets>
<deviceset name="CHIP-ROUND-R" urn="urn:adsk.eagle:component:22900853/4" prefix="D" library_version="7">
<description>&lt;B&gt; LED - Generic</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="_0603" package="LEDC1608X65N_RND-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.n1ru9s2WSdGmSoMZDXS2cg?version=1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED ROUND" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="ROUND_TOP" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0603-1MM" package="LEDC1608X95N_RND-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.mTFnh-NETk-dsQqn1o-rrA?version=1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED ROUND" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="ROUND_TOP" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0805" package="LEDC2012X105N_RND-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.DyQ-SXPGR1mR7-8goJM-gA?version=1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED ROUND" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="ROUND_TOP" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1206" package="LEDC3216X180N_RND-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.3Q1wa-ohSpmICe6RKd0phw?version=1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED ROUND" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="ROUND_TOP" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1209" package="LEDC3224X250N_RND-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.wipprod:fs.file:vf.2cvZoe7CQlaHvHEL-OW1fA?version=1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED ROUND" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="ROUND_TOP" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Capacitor" urn="urn:adsk.eagle:library:16290819">
<description>&lt;B&gt;Capacitors - Fixed, Variable, Trimmers</description>
<packages>
<package name="CAPC1005X60" urn="urn:adsk.eagle:footprint:16290849/2" library_version="4">
<description>Chip, 1.00 X 0.50 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.60 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6286" x2="-0.55" y2="0.6286" width="0.127" layer="21"/>
<wire x1="0.55" y1="-0.6286" x2="-0.55" y2="-0.6286" width="0.127" layer="21"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.12" layer="51"/>
<smd name="1" x="-0.4846" y="0" dx="0.56" dy="0.6291" layer="1"/>
<smd name="2" x="0.4846" y="0" dx="0.56" dy="0.6291" layer="1"/>
<text x="0" y="1.2636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.2636" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1110X102" urn="urn:adsk.eagle:footprint:16290845/2" library_version="4">
<description>Chip, 1.17 X 1.02 X 1.02 mm body
&lt;p&gt;Chip package with body size 1.17 X 1.02 X 1.02 mm&lt;/p&gt;</description>
<wire x1="0.66" y1="0.9552" x2="-0.66" y2="0.9552" width="0.127" layer="21"/>
<wire x1="0.66" y1="-0.9552" x2="-0.66" y2="-0.9552" width="0.127" layer="21"/>
<wire x1="0.66" y1="-0.635" x2="-0.66" y2="-0.635" width="0.12" layer="51"/>
<wire x1="-0.66" y1="-0.635" x2="-0.66" y2="0.635" width="0.12" layer="51"/>
<wire x1="-0.66" y1="0.635" x2="0.66" y2="0.635" width="0.12" layer="51"/>
<wire x1="0.66" y1="0.635" x2="0.66" y2="-0.635" width="0.12" layer="51"/>
<smd name="1" x="-0.5388" y="0" dx="0.6626" dy="1.2823" layer="1"/>
<smd name="2" x="0.5388" y="0" dx="0.6626" dy="1.2823" layer="1"/>
<text x="0" y="1.5902" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.5902" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1608X85" urn="urn:adsk.eagle:footprint:16290847/2" library_version="4">
<description>Chip, 1.60 X 0.80 X 0.85 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.85 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.7991" x2="-0.875" y2="0.7991" width="0.127" layer="21"/>
<wire x1="0.875" y1="-0.7991" x2="-0.875" y2="-0.7991" width="0.127" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7746" y="0" dx="0.9209" dy="0.9702" layer="1"/>
<smd name="2" x="0.7746" y="0" dx="0.9209" dy="0.9702" layer="1"/>
<text x="0" y="1.4341" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4341" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC2012X110" urn="urn:adsk.eagle:footprint:16290848/2" library_version="4">
<description>Chip, 2.00 X 1.25 X 1.10 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 1.10 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="1.0467" x2="-1.1" y2="1.0467" width="0.127" layer="21"/>
<wire x1="1.1" y1="-1.0467" x2="-1.1" y2="-1.0467" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.725" x2="-1.1" y2="-0.725" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.725" x2="-1.1" y2="0.725" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.725" x2="1.1" y2="0.725" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.725" x2="1.1" y2="-0.725" width="0.12" layer="51"/>
<smd name="1" x="-0.8754" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<smd name="2" x="0.8754" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<text x="0" y="1.6817" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6817" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3216X135" urn="urn:adsk.eagle:footprint:16290836/2" library_version="4">
<description>Chip, 3.20 X 1.60 X 1.35 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 1.35 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.2217" x2="-1.7" y2="1.2217" width="0.127" layer="21"/>
<wire x1="1.7" y1="-1.2217" x2="-1.7" y2="-1.2217" width="0.127" layer="21"/>
<wire x1="1.7" y1="-0.9" x2="-1.7" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.9" x2="-1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.9" x2="1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.9" x2="1.7" y2="-0.9" width="0.12" layer="51"/>
<smd name="1" x="-1.4754" y="0" dx="1.1646" dy="1.8153" layer="1"/>
<smd name="2" x="1.4754" y="0" dx="1.1646" dy="1.8153" layer="1"/>
<text x="0" y="1.8567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3225X135" urn="urn:adsk.eagle:footprint:16290843/2" library_version="4">
<description>Chip, 3.20 X 2.50 X 1.35 mm body
&lt;p&gt;Chip package with body size 3.20 X 2.50 X 1.35 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.127" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4754" y="0" dx="1.1646" dy="2.7153" layer="1"/>
<smd name="2" x="1.4754" y="0" dx="1.1646" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4532X135" urn="urn:adsk.eagle:footprint:16290841/2" library_version="4">
<description>Chip, 4.50 X 3.20 X 1.35 mm body
&lt;p&gt;Chip package with body size 4.50 X 3.20 X 1.35 mm&lt;/p&gt;</description>
<wire x1="2.4" y1="2.0217" x2="-2.4" y2="2.0217" width="0.127" layer="21"/>
<wire x1="2.4" y1="-2.0217" x2="-2.4" y2="-2.0217" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.7" x2="-2.4" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.4" y1="-1.7" x2="-2.4" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.4" y1="1.7" x2="2.4" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.4" y1="1.7" x2="2.4" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.0565" y="0" dx="1.3973" dy="3.4153" layer="1"/>
<smd name="2" x="2.0565" y="0" dx="1.3973" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPM3216X180" urn="urn:adsk.eagle:footprint:16290835/2" library_version="4">
<description>Molded Body, 3.20 X 1.60 X 1.80 mm body
&lt;p&gt;Molded Body package with body size 3.20 X 1.60 X 1.80 mm&lt;/p&gt;</description>
<wire x1="-1.7" y1="0.9084" x2="1.7" y2="0.9084" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.9084" x2="1.7" y2="-0.9084" width="0.127" layer="21"/>
<wire x1="1.7" y1="-0.9" x2="-1.7" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.9" x2="-1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.9" x2="1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.9" x2="1.7" y2="-0.9" width="0.12" layer="51"/>
<smd name="1" x="-1.3099" y="0" dx="1.7955" dy="1.1887" layer="1"/>
<smd name="2" x="1.3099" y="0" dx="1.7955" dy="1.1887" layer="1"/>
<text x="0" y="1.5434" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.5434" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPM3528X210" urn="urn:adsk.eagle:footprint:16290844/2" library_version="4">
<description>Molded Body, 3.50 X 2.80 X 2.10 mm body
&lt;p&gt;Molded Body package with body size 3.50 X 2.80 X 2.10 mm&lt;/p&gt;</description>
<wire x1="-1.85" y1="1.5" x2="1.85" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.85" y1="-1.5" x2="1.85" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.85" y1="-1.5" x2="-1.85" y2="-1.5" width="0.12" layer="51"/>
<wire x1="-1.85" y1="-1.5" x2="-1.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="-1.85" y1="1.5" x2="1.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="1.85" y1="1.5" x2="1.85" y2="-1.5" width="0.12" layer="51"/>
<smd name="1" x="-1.4599" y="0" dx="1.7955" dy="2.2036" layer="1"/>
<smd name="2" x="1.4599" y="0" dx="1.7955" dy="2.2036" layer="1"/>
<text x="0" y="2.135" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.135" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPM6032X280" urn="urn:adsk.eagle:footprint:16290839/2" library_version="4">
<description>Molded Body, 6.00 X 3.20 X 2.80 mm body
&lt;p&gt;Molded Body package with body size 6.00 X 3.20 X 2.80 mm&lt;/p&gt;</description>
<wire x1="-3.15" y1="1.75" x2="3.15" y2="1.75" width="0.127" layer="21"/>
<wire x1="-3.15" y1="-1.75" x2="3.15" y2="-1.75" width="0.127" layer="21"/>
<wire x1="3.15" y1="-1.75" x2="-3.15" y2="-1.75" width="0.12" layer="51"/>
<wire x1="-3.15" y1="-1.75" x2="-3.15" y2="1.75" width="0.12" layer="51"/>
<wire x1="-3.15" y1="1.75" x2="3.15" y2="1.75" width="0.12" layer="51"/>
<wire x1="3.15" y1="1.75" x2="3.15" y2="-1.75" width="0.12" layer="51"/>
<smd name="1" x="-2.4712" y="0" dx="2.368" dy="2.2036" layer="1"/>
<smd name="2" x="2.4712" y="0" dx="2.368" dy="2.2036" layer="1"/>
<text x="0" y="2.385" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.385" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPM7343X310" urn="urn:adsk.eagle:footprint:16290840/2" library_version="4">
<description>Molded Body, 7.30 X 4.30 X 3.10 mm body
&lt;p&gt;Molded Body package with body size 7.30 X 4.30 X 3.10 mm&lt;/p&gt;</description>
<wire x1="-3.8" y1="2.3" x2="3.8" y2="2.3" width="0.127" layer="21"/>
<wire x1="-3.8" y1="-2.3" x2="3.8" y2="-2.3" width="0.127" layer="21"/>
<wire x1="3.8" y1="-2.3" x2="-3.8" y2="-2.3" width="0.12" layer="51"/>
<wire x1="-3.8" y1="-2.3" x2="-3.8" y2="2.3" width="0.12" layer="51"/>
<wire x1="-3.8" y1="2.3" x2="3.8" y2="2.3" width="0.12" layer="51"/>
<wire x1="3.8" y1="2.3" x2="3.8" y2="-2.3" width="0.12" layer="51"/>
<smd name="1" x="-3.1212" y="0" dx="2.368" dy="2.4036" layer="1"/>
<smd name="2" x="3.1212" y="0" dx="2.368" dy="2.4036" layer="1"/>
<text x="0" y="2.935" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.935" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4564X110" urn="urn:adsk.eagle:footprint:16290837/2" library_version="4">
<description>Chip, 4.50 X 6.40 X 1.10 mm body
&lt;p&gt;Chip package with body size 4.50 X 6.40 X 1.10 mm&lt;/p&gt;</description>
<wire x1="2.4" y1="3.7179" x2="-2.4" y2="3.7179" width="0.127" layer="21"/>
<wire x1="2.4" y1="-3.7179" x2="-2.4" y2="-3.7179" width="0.127" layer="21"/>
<wire x1="2.4" y1="-3.4" x2="-2.4" y2="-3.4" width="0.12" layer="51"/>
<wire x1="-2.4" y1="-3.4" x2="-2.4" y2="3.4" width="0.12" layer="51"/>
<wire x1="-2.4" y1="3.4" x2="2.4" y2="3.4" width="0.12" layer="51"/>
<wire x1="2.4" y1="3.4" x2="2.4" y2="-3.4" width="0.12" layer="51"/>
<smd name="1" x="-2.0565" y="0" dx="1.3973" dy="6.8078" layer="1"/>
<smd name="2" x="2.0565" y="0" dx="1.3973" dy="6.8078" layer="1"/>
<text x="0" y="4.3529" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.3529" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPRD550W60D1025H1250B" urn="urn:adsk.eagle:footprint:16290829/2" library_version="4">
<description>Radial Non-Polarized Capacitor, 5.50 mm pitch, 10.25 mm body diameter, 12.50 mm body height
&lt;p&gt;Radial Non-Polarized Capacitor package with 5.50 mm pitch (lead spacing), 0.60 mm lead diameter, 10.25 mm body diameter and 12.50 mm body height&lt;/p&gt;</description>
<circle x="0" y="0" radius="5.25" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5.25" width="0.12" layer="51"/>
<pad name="1" x="-2.75" y="0" drill="0.8" diameter="1.4"/>
<pad name="2" x="2.75" y="0" drill="0.8" diameter="1.4"/>
<text x="0" y="5.885" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.885" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPRD2261W240D5080H5555B" urn="urn:adsk.eagle:footprint:16290850/2" library_version="4">
<description>Radial Non-Polarized Capacitor, 22.61 mm pitch, 50.80 mm body diameter, 55.55 mm body height
&lt;p&gt;Radial Non-Polarized Capacitor package with 22.61 mm pitch (lead spacing), 2.40 mm lead diameter, 50.80 mm body diameter and 55.55 mm body height&lt;/p&gt;</description>
<circle x="0" y="0" radius="25.79" width="0.127" layer="21"/>
<circle x="0" y="0" radius="25.79" width="0.12" layer="51"/>
<pad name="1" x="-11.305" y="0" drill="2.6" diameter="3.9"/>
<pad name="2" x="11.305" y="0" drill="2.6" diameter="3.9"/>
<text x="0" y="26.425" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-26.425" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:16290895/2" type="model" library_version="4">
<description>Chip, 1.00 X 0.50 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X60"/>
</packageinstances>
</package3d>
<package3d name="CAPC1110X102" urn="urn:adsk.eagle:package:16290904/2" type="model" library_version="4">
<description>Chip, 1.17 X 1.02 X 1.02 mm body
&lt;p&gt;Chip package with body size 1.17 X 1.02 X 1.02 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1110X102"/>
</packageinstances>
</package3d>
<package3d name="CAPC1608X85" urn="urn:adsk.eagle:package:16290898/2" type="model" library_version="4">
<description>Chip, 1.60 X 0.80 X 0.85 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.85 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1608X85"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X110" urn="urn:adsk.eagle:package:16290897/2" type="model" library_version="4">
<description>Chip, 2.00 X 1.25 X 1.10 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X110"/>
</packageinstances>
</package3d>
<package3d name="CAPC3216X135" urn="urn:adsk.eagle:package:16290893/2" type="model" library_version="4">
<description>Chip, 3.20 X 1.60 X 1.35 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3216X135"/>
</packageinstances>
</package3d>
<package3d name="CAPC3225X135" urn="urn:adsk.eagle:package:16290903/2" type="model" library_version="4">
<description>Chip, 3.20 X 2.50 X 1.35 mm body
&lt;p&gt;Chip package with body size 3.20 X 2.50 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3225X135"/>
</packageinstances>
</package3d>
<package3d name="CAPC4532X135" urn="urn:adsk.eagle:package:16290900/2" type="model" library_version="4">
<description>Chip, 4.50 X 3.20 X 1.35 mm body
&lt;p&gt;Chip package with body size 4.50 X 3.20 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4532X135"/>
</packageinstances>
</package3d>
<package3d name="CAPM3216X180" urn="urn:adsk.eagle:package:16290894/2" type="model" library_version="4">
<description>Molded Body, 3.20 X 1.60 X 1.80 mm body
&lt;p&gt;Molded Body package with body size 3.20 X 1.60 X 1.80 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPM3216X180"/>
</packageinstances>
</package3d>
<package3d name="CAPM3528X210" urn="urn:adsk.eagle:package:16290902/2" type="model" library_version="4">
<description>Molded Body, 3.50 X 2.80 X 2.10 mm body
&lt;p&gt;Molded Body package with body size 3.50 X 2.80 X 2.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPM3528X210"/>
</packageinstances>
</package3d>
<package3d name="CAPM6032X280" urn="urn:adsk.eagle:package:16290896/2" type="model" library_version="4">
<description>Molded Body, 6.00 X 3.20 X 2.80 mm body
&lt;p&gt;Molded Body package with body size 6.00 X 3.20 X 2.80 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPM6032X280"/>
</packageinstances>
</package3d>
<package3d name="CAPM7343X310" urn="urn:adsk.eagle:package:16290891/2" type="model" library_version="4">
<description>Molded Body, 7.30 X 4.30 X 3.10 mm body
&lt;p&gt;Molded Body package with body size 7.30 X 4.30 X 3.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPM7343X310"/>
</packageinstances>
</package3d>
<package3d name="CAPC4564X110L" urn="urn:adsk.eagle:package:16290887/3" type="model" library_version="4">
<description>Chip, 4.50 X 6.40 X 1.10 mm body
&lt;p&gt;Chip package with body size 4.50 X 6.40 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4564X110"/>
</packageinstances>
</package3d>
<package3d name="CAPRD550W60D1025H1250B" urn="urn:adsk.eagle:package:16290858/2" type="model" library_version="4">
<description>Radial Non-Polarized Capacitor, 5.50 mm pitch, 10.25 mm body diameter, 12.50 mm body height
&lt;p&gt;Radial Non-Polarized Capacitor package with 5.50 mm pitch (lead spacing), 0.60 mm lead diameter, 10.25 mm body diameter and 12.50 mm body height&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPRD550W60D1025H1250B"/>
</packageinstances>
</package3d>
<package3d name="CAPRD2261W240D5080H5555B" urn="urn:adsk.eagle:package:16290864/2" type="model" library_version="4">
<description>Radial Non-Polarized Capacitor, 22.61 mm pitch, 50.80 mm body diameter, 55.55 mm body height
&lt;p&gt;Radial Non-Polarized Capacitor package with 22.61 mm pitch (lead spacing), 2.40 mm lead diameter, 50.80 mm body diameter and 55.55 mm body height&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPRD2261W240D5080H5555B"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="C" urn="urn:adsk.eagle:symbol:16290820/1" library_version="4">
<description>Capacitor</description>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="97">&gt;SPICEMODEL</text>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="-5.08" size="1.778" layer="97">&gt;SPICEEXTRA</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="C" urn="urn:adsk.eagle:component:16290909/4" prefix="C" uservalue="yes" library_version="4">
<description>&lt;B&gt;Capacitor - Generic</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="CHIP-0402(1005-METRIC)" package="CAPC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290895/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0504(1310-METRIC)" package="CAPC1110X102">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290904/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0603(1608-METRIC)" package="CAPC1608X85">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290898/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0805(2012-METRIC)" package="CAPC2012X110">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290897/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1206(3216-METRIC)" package="CAPC3216X135">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290893/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1210(3225-METRIC)" package="CAPC3225X135">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290903/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1812(4532-METRIC)" package="CAPC4532X135">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290900/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TANTALUM-1206(3216-METRIC)" package="CAPM3216X180">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290894/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TANTALUM-1411(3528-METRIC)" package="CAPM3528X210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290902/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TANTALUM-2412(6032-METRIC)" package="CAPM6032X280">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290896/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TANTALUM-2917(7343-METRIC)" package="CAPM7343X310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290891/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1825(4564-METRIC)" package="CAPC4564X110">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290887/3"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="RADIAL-12.5MM-DIA" package="CAPRD550W60D1025H1250B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290858/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="RADIAL-55.5MM-DIA" package="CAPRD2261W240D5080H5555B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290864/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="C">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
</devicesets>
</library>
<library name="Resistor">
<description>&lt;B&gt;Resistors, Potentiometers, TrimPot</description>
<packages>
<package name="RESC1005X40" urn="urn:adsk.eagle:footprint:16378540/2">
<description>Chip, 1.05 X 0.54 X 0.40 mm body
&lt;p&gt;Chip package with body size 1.05 X 0.54 X 0.40 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.636" x2="-0.55" y2="0.636" width="0.127" layer="21"/>
<wire x1="0.55" y1="-0.636" x2="-0.55" y2="-0.636" width="0.127" layer="21"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.12" layer="51"/>
<smd name="1" x="-0.5075" y="0" dx="0.5351" dy="0.644" layer="1"/>
<smd name="2" x="0.5075" y="0" dx="0.5351" dy="0.644" layer="1"/>
<text x="0" y="1.271" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.271" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC1608X60" urn="urn:adsk.eagle:footprint:16378537/2">
<description>Chip, 1.60 X 0.82 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.82 X 0.60 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.8009" x2="-0.85" y2="0.8009" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.8009" x2="-0.85" y2="-0.8009" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.475" x2="-0.85" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.475" x2="-0.85" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.475" x2="0.85" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.475" x2="0.85" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.8152" y="0" dx="0.7987" dy="0.9739" layer="1"/>
<smd name="2" x="0.8152" y="0" dx="0.7987" dy="0.9739" layer="1"/>
<text x="0" y="1.4359" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4359" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2012X65" urn="urn:adsk.eagle:footprint:16378532/2">
<description>Chip, 2.00 X 1.25 X 0.65 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.65 mm&lt;/p&gt;</description>
<wire x1="1.075" y1="1.0241" x2="-1.075" y2="1.0241" width="0.127" layer="21"/>
<wire x1="1.075" y1="-1.0241" x2="-1.075" y2="-1.0241" width="0.127" layer="21"/>
<wire x1="1.075" y1="-0.7" x2="-1.075" y2="-0.7" width="0.12" layer="51"/>
<wire x1="-1.075" y1="-0.7" x2="-1.075" y2="0.7" width="0.12" layer="51"/>
<wire x1="-1.075" y1="0.7" x2="1.075" y2="0.7" width="0.12" layer="51"/>
<wire x1="1.075" y1="0.7" x2="1.075" y2="-0.7" width="0.12" layer="51"/>
<smd name="1" x="-0.9195" y="0" dx="1.0312" dy="1.4202" layer="1"/>
<smd name="2" x="0.9195" y="0" dx="1.0312" dy="1.4202" layer="1"/>
<text x="0" y="1.6591" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6591" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3216X70" urn="urn:adsk.eagle:footprint:16378539/2">
<description>Chip, 3.20 X 1.60 X 0.70 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 0.70 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.2217" x2="-1.7" y2="1.2217" width="0.127" layer="21"/>
<wire x1="1.7" y1="-1.2217" x2="-1.7" y2="-1.2217" width="0.127" layer="21"/>
<wire x1="1.7" y1="-0.9" x2="-1.7" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.9" x2="-1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.9" x2="1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.9" x2="1.7" y2="-0.9" width="0.12" layer="51"/>
<smd name="1" x="-1.4754" y="0" dx="1.1646" dy="1.8153" layer="1"/>
<smd name="2" x="1.4754" y="0" dx="1.1646" dy="1.8153" layer="1"/>
<text x="0" y="1.8567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3224X71" urn="urn:adsk.eagle:footprint:16378536/2">
<description>Chip, 3.20 X 2.49 X 0.71 mm body
&lt;p&gt;Chip package with body size 3.20 X 2.49 X 0.71 mm&lt;/p&gt;</description>
<wire x1="1.675" y1="1.6441" x2="-1.675" y2="1.6441" width="0.127" layer="21"/>
<wire x1="1.675" y1="-1.6441" x2="-1.675" y2="-1.6441" width="0.127" layer="21"/>
<wire x1="1.675" y1="-1.32" x2="-1.675" y2="-1.32" width="0.12" layer="51"/>
<wire x1="-1.675" y1="-1.32" x2="-1.675" y2="1.32" width="0.12" layer="51"/>
<wire x1="-1.675" y1="1.32" x2="1.675" y2="1.32" width="0.12" layer="51"/>
<wire x1="1.675" y1="1.32" x2="1.675" y2="-1.32" width="0.12" layer="51"/>
<smd name="1" x="-1.4695" y="0" dx="1.1312" dy="2.6602" layer="1"/>
<smd name="2" x="1.4695" y="0" dx="1.1312" dy="2.6602" layer="1"/>
<text x="0" y="2.2791" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2791" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5025X71" urn="urn:adsk.eagle:footprint:16378538/2">
<description>Chip, 5.00 X 2.50 X 0.71 mm body
&lt;p&gt;Chip package with body size 5.00 X 2.50 X 0.71 mm&lt;/p&gt;</description>
<wire x1="2.575" y1="1.6491" x2="-2.575" y2="1.6491" width="0.127" layer="21"/>
<wire x1="2.575" y1="-1.6491" x2="-2.575" y2="-1.6491" width="0.127" layer="21"/>
<wire x1="2.575" y1="-1.325" x2="-2.575" y2="-1.325" width="0.12" layer="51"/>
<wire x1="-2.575" y1="-1.325" x2="-2.575" y2="1.325" width="0.12" layer="51"/>
<wire x1="-2.575" y1="1.325" x2="2.575" y2="1.325" width="0.12" layer="51"/>
<wire x1="2.575" y1="1.325" x2="2.575" y2="-1.325" width="0.12" layer="51"/>
<smd name="1" x="-2.3195" y="0" dx="1.2312" dy="2.6702" layer="1"/>
<smd name="2" x="2.3195" y="0" dx="1.2312" dy="2.6702" layer="1"/>
<text x="0" y="2.2841" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2841" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC6332X71" urn="urn:adsk.eagle:footprint:16378533/2">
<description>Chip, 6.30 X 3.20 X 0.71 mm body
&lt;p&gt;Chip package with body size 6.30 X 3.20 X 0.71 mm&lt;/p&gt;</description>
<wire x1="3.225" y1="1.9991" x2="-3.225" y2="1.9991" width="0.127" layer="21"/>
<wire x1="3.225" y1="-1.9991" x2="-3.225" y2="-1.9991" width="0.127" layer="21"/>
<wire x1="3.225" y1="-1.675" x2="-3.225" y2="-1.675" width="0.12" layer="51"/>
<wire x1="-3.225" y1="-1.675" x2="-3.225" y2="1.675" width="0.12" layer="51"/>
<wire x1="-3.225" y1="1.675" x2="3.225" y2="1.675" width="0.12" layer="51"/>
<wire x1="3.225" y1="1.675" x2="3.225" y2="-1.675" width="0.12" layer="51"/>
<smd name="1" x="-2.9695" y="0" dx="1.2312" dy="3.3702" layer="1"/>
<smd name="2" x="2.9695" y="0" dx="1.2312" dy="3.3702" layer="1"/>
<text x="0" y="2.6341" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6341" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESAD1176W63L850D250B" urn="urn:adsk.eagle:footprint:16378542/2">
<description>AXIAL Resistor, 11.76 mm pitch, 8.5 mm body length, 2.5 mm body diameter
&lt;p&gt;AXIAL Resistor package with 11.76 mm pitch, 0.63 mm lead diameter, 8.5 mm body length and 2.5 mm body diameter&lt;/p&gt;</description>
<wire x1="-4.25" y1="1.25" x2="-4.25" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.25" y1="-1.25" x2="4.25" y2="-1.25" width="0.127" layer="21"/>
<wire x1="4.25" y1="-1.25" x2="4.25" y2="1.25" width="0.127" layer="21"/>
<wire x1="4.25" y1="1.25" x2="-4.25" y2="1.25" width="0.127" layer="21"/>
<wire x1="-4.25" y1="0" x2="-4.911" y2="0" width="0.127" layer="21"/>
<wire x1="4.25" y1="0" x2="4.911" y2="0" width="0.127" layer="21"/>
<wire x1="4.25" y1="-1.25" x2="-4.25" y2="-1.25" width="0.12" layer="51"/>
<wire x1="-4.25" y1="-1.25" x2="-4.25" y2="1.25" width="0.12" layer="51"/>
<wire x1="-4.25" y1="1.25" x2="4.25" y2="1.25" width="0.12" layer="51"/>
<wire x1="4.25" y1="1.25" x2="4.25" y2="-1.25" width="0.12" layer="51"/>
<pad name="1" x="-5.88" y="0" drill="0.83" diameter="1.43"/>
<pad name="2" x="5.88" y="0" drill="0.83" diameter="1.43"/>
<text x="0" y="1.885" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.885" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESMELF3515" urn="urn:adsk.eagle:footprint:16378534/2">
<description>MELF, 3.50 mm length, 1.52 mm diameter
&lt;p&gt;MELF Resistor package with 3.50 mm length and 1.52 mm diameter&lt;/p&gt;</description>
<wire x1="1.105" y1="1.1825" x2="-1.105" y2="1.1825" width="0.127" layer="21"/>
<wire x1="-1.105" y1="-1.1825" x2="1.105" y2="-1.1825" width="0.127" layer="21"/>
<wire x1="1.85" y1="-0.8" x2="-1.85" y2="-0.8" width="0.12" layer="51"/>
<wire x1="-1.85" y1="-0.8" x2="-1.85" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.85" y1="0.8" x2="1.85" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.85" y1="0.8" x2="1.85" y2="-0.8" width="0.12" layer="51"/>
<smd name="1" x="-1.6813" y="0" dx="1.1527" dy="1.7371" layer="1"/>
<smd name="2" x="1.6813" y="0" dx="1.1527" dy="1.7371" layer="1"/>
<text x="0" y="1.8175" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8175" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESMELF2014" urn="urn:adsk.eagle:footprint:16378535/2">
<description>MELF, 2.00 mm length, 1.40 mm diameter
&lt;p&gt;MELF Resistor package with 2.00 mm length and 1.40 mm diameter&lt;/p&gt;</description>
<wire x1="0.5189" y1="1.114" x2="-0.5189" y2="1.114" width="0.127" layer="21"/>
<wire x1="-0.5189" y1="-1.114" x2="0.5189" y2="-1.114" width="0.127" layer="21"/>
<wire x1="1.05" y1="-0.725" x2="-1.05" y2="-0.725" width="0.12" layer="51"/>
<wire x1="-1.05" y1="-0.725" x2="-1.05" y2="0.725" width="0.12" layer="51"/>
<wire x1="-1.05" y1="0.725" x2="1.05" y2="0.725" width="0.12" layer="51"/>
<wire x1="1.05" y1="0.725" x2="1.05" y2="-0.725" width="0.12" layer="51"/>
<smd name="1" x="-0.9918" y="0" dx="0.9456" dy="1.6" layer="1"/>
<smd name="2" x="0.9918" y="0" dx="0.9456" dy="1.6" layer="1"/>
<text x="0" y="1.749" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.749" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESMELF5924" urn="urn:adsk.eagle:footprint:16378541/2">
<description>MELF, 5.90 mm length, 2.45 mm diameter
&lt;p&gt;MELF Resistor package with 5.90 mm length and 2.45 mm diameter&lt;/p&gt;</description>
<wire x1="2.1315" y1="1.639" x2="-2.1315" y2="1.639" width="0.127" layer="21"/>
<wire x1="-2.1315" y1="-1.639" x2="2.1315" y2="-1.639" width="0.127" layer="21"/>
<wire x1="3.05" y1="-1.25" x2="-3.05" y2="-1.25" width="0.12" layer="51"/>
<wire x1="-3.05" y1="-1.25" x2="-3.05" y2="1.25" width="0.12" layer="51"/>
<wire x1="-3.05" y1="1.25" x2="3.05" y2="1.25" width="0.12" layer="51"/>
<wire x1="3.05" y1="1.25" x2="3.05" y2="-1.25" width="0.12" layer="51"/>
<smd name="1" x="-2.7946" y="0" dx="1.3261" dy="2.65" layer="1"/>
<smd name="2" x="2.7946" y="0" dx="1.3261" dy="2.65" layer="1"/>
<text x="0" y="2.274" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.274" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESMELF3218" urn="urn:adsk.eagle:footprint:16378531/2">
<description>MELF, 3.20 mm length, 1.80 mm diameter
&lt;p&gt;MELF Resistor package with 3.20 mm length and 1.80 mm diameter&lt;/p&gt;</description>
<wire x1="0.8815" y1="1.314" x2="-0.8815" y2="1.314" width="0.127" layer="21"/>
<wire x1="-0.8815" y1="-1.314" x2="0.8815" y2="-1.314" width="0.127" layer="21"/>
<wire x1="1.7" y1="-0.925" x2="-1.7" y2="-0.925" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.925" x2="-1.7" y2="0.925" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.925" x2="1.7" y2="0.925" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.925" x2="1.7" y2="-0.925" width="0.12" layer="51"/>
<smd name="1" x="-1.4946" y="0" dx="1.2261" dy="2" layer="1"/>
<smd name="2" x="1.4946" y="0" dx="1.2261" dy="2" layer="1"/>
<text x="0" y="1.949" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.949" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESAD724W46L381D178B" urn="urn:adsk.eagle:footprint:16378530/2">
<description>Axial Resistor, 7.24 mm pitch, 3.81 mm body length, 1.78 mm body diameter
&lt;p&gt;Axial Resistor package with 7.24 mm pitch (lead spacing), 0.46 mm lead diameter, 3.81 mm body length and 1.78 mm body diameter&lt;/p&gt;</description>
<wire x1="-2.16" y1="1.015" x2="-2.16" y2="-1.015" width="0.127" layer="21"/>
<wire x1="-2.16" y1="-1.015" x2="2.16" y2="-1.015" width="0.127" layer="21"/>
<wire x1="2.16" y1="-1.015" x2="2.16" y2="1.015" width="0.127" layer="21"/>
<wire x1="2.16" y1="1.015" x2="-2.16" y2="1.015" width="0.127" layer="21"/>
<wire x1="-2.16" y1="0" x2="-2.736" y2="0" width="0.127" layer="21"/>
<wire x1="2.16" y1="0" x2="2.736" y2="0" width="0.127" layer="21"/>
<wire x1="2.16" y1="-1.015" x2="-2.16" y2="-1.015" width="0.12" layer="51"/>
<wire x1="-2.16" y1="-1.015" x2="-2.16" y2="1.015" width="0.12" layer="51"/>
<wire x1="-2.16" y1="1.015" x2="2.16" y2="1.015" width="0.12" layer="51"/>
<wire x1="2.16" y1="1.015" x2="2.16" y2="-1.015" width="0.12" layer="51"/>
<pad name="1" x="-3.62" y="0" drill="0.66" diameter="1.26"/>
<pad name="2" x="3.62" y="0" drill="0.66" diameter="1.26"/>
<text x="0" y="1.65" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.65" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="RESC1005X40" urn="urn:adsk.eagle:package:16378568/2" type="model">
<description>Chip, 1.05 X 0.54 X 0.40 mm body
&lt;p&gt;Chip package with body size 1.05 X 0.54 X 0.40 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1005X40"/>
</packageinstances>
</package3d>
<package3d name="RESC1608X60" urn="urn:adsk.eagle:package:16378565/2" type="model">
<description>Chip, 1.60 X 0.82 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.82 X 0.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1608X60"/>
</packageinstances>
</package3d>
<package3d name="RESC2012X65" urn="urn:adsk.eagle:package:16378559/2" type="model">
<description>Chip, 2.00 X 1.25 X 0.65 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2012X65"/>
</packageinstances>
</package3d>
<package3d name="RESC3216X70" urn="urn:adsk.eagle:package:16378566/2" type="model">
<description>Chip, 3.20 X 1.60 X 0.70 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3216X70"/>
</packageinstances>
</package3d>
<package3d name="RESC3224X71" urn="urn:adsk.eagle:package:16378563/3" type="model">
<description>Chip, 3.20 X 2.49 X 0.71 mm body
&lt;p&gt;Chip package with body size 3.20 X 2.49 X 0.71 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3224X71"/>
</packageinstances>
</package3d>
<package3d name="RESC5025X71" urn="urn:adsk.eagle:package:16378564/2" type="model">
<description>Chip, 5.00 X 2.50 X 0.71 mm body
&lt;p&gt;Chip package with body size 5.00 X 2.50 X 0.71 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5025X71"/>
</packageinstances>
</package3d>
<package3d name="RESC6332X71L" urn="urn:adsk.eagle:package:16378557/3" type="model">
<description>Chip, 6.30 X 3.20 X 0.71 mm body
&lt;p&gt;Chip package with body size 6.30 X 3.20 X 0.71 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC6332X71"/>
</packageinstances>
</package3d>
<package3d name="RESAD1176W63L850D250B" urn="urn:adsk.eagle:package:16378560/2" type="model">
<description>AXIAL Resistor, 11.76 mm pitch, 8.5 mm body length, 2.5 mm body diameter
&lt;p&gt;AXIAL Resistor package with 11.76 mm pitch, 0.63 mm lead diameter, 8.5 mm body length and 2.5 mm body diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESAD1176W63L850D250B"/>
</packageinstances>
</package3d>
<package3d name="RESMELF3515" urn="urn:adsk.eagle:package:16378562/2" type="model">
<description>MELF, 3.50 mm length, 1.52 mm diameter
&lt;p&gt;MELF Resistor package with 3.50 mm length and 1.52 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESMELF3515"/>
</packageinstances>
</package3d>
<package3d name="RESMELF2014" urn="urn:adsk.eagle:package:16378558/2" type="model">
<description>MELF, 2.00 mm length, 1.40 mm diameter
&lt;p&gt;MELF Resistor package with 2.00 mm length and 1.40 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESMELF2014"/>
</packageinstances>
</package3d>
<package3d name="RESMELF5924" urn="urn:adsk.eagle:package:16378567/3" type="model">
<description>MELF, 5.90 mm length, 2.45 mm diameter
&lt;p&gt;MELF Resistor package with 5.90 mm length and 2.45 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESMELF5924"/>
</packageinstances>
</package3d>
<package3d name="RESMELF3218" urn="urn:adsk.eagle:package:16378556/2" type="model">
<description>MELF, 3.20 mm length, 1.80 mm diameter
&lt;p&gt;MELF Resistor package with 3.20 mm length and 1.80 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESMELF3218"/>
</packageinstances>
</package3d>
<package3d name="RESAD724W46L381D178B" urn="urn:adsk.eagle:package:16378561/2" type="model">
<description>Axial Resistor, 7.24 mm pitch, 3.81 mm body length, 1.78 mm body diameter
&lt;p&gt;Axial Resistor package with 7.24 mm pitch (lead spacing), 0.46 mm lead diameter, 3.81 mm body length and 1.78 mm body diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESAD724W46L381D178B"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R" urn="urn:adsk.eagle:symbol:16378529/2">
<description>RESISTOR</description>
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="0" y="2.54" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-5.08" size="1.778" layer="95" align="center">&gt;SPICEMODEL</text>
<text x="0" y="-2.54" size="1.778" layer="95" align="center">&gt;VALUE</text>
<text x="0" y="-7.62" size="1.778" layer="95" align="center">&gt;SPICEEXTRA</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" urn="urn:adsk.eagle:component:16378570/4" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor Fixed - Generic</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="CHIP-0402(1005-METRIC)" package="RESC1005X40">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378568/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0603(1608-METRIC)" package="RESC1608X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378565/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0805(2012-METRIC)" package="RESC2012X65">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378559/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1206(3216-METRIC)" package="RESC3216X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378566/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1210(3225-METRIC)" package="RESC3224X71">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378563/3"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-2010(5025-METRIC)" package="RESC5025X71">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378564/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-2512(6332-METRIC)" package="RESC6332X71">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378557/3"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="AXIAL-11.7MM-PITCH" package="RESAD1176W63L850D250B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378560/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF(3515-METRIC)" package="RESMELF3515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378562/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF(2014-METRIC)" package="RESMELF2014">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378558/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF(5924-METRIC)" package="RESMELF5924">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378567/3"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF(3218-METRIC)" package="RESMELF3218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378556/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="AXIAL-7.2MM-PITCH" package="RESAD724W46L381D178B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378561/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="R">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols" urn="urn:adsk.eagle:library:530">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:39418/1" library_version="1">
<description>&lt;h3&gt;VCC Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="VCC" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="V_BATT" urn="urn:adsk.eagle:symbol:39428/1" library_version="1">
<description>&lt;h3&gt;Battery Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="V_BATT" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="3.3V" urn="urn:adsk.eagle:symbol:39411/1" library_version="1">
<description>&lt;h3&gt;3.3V Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="VCCIO" urn="urn:adsk.eagle:symbol:39421/1" library_version="1">
<description>&lt;h3&gt;VCC I/O Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="VCCIO" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" urn="urn:adsk.eagle:component:39449/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;VCC Voltage Supply&lt;/h3&gt;
&lt;p&gt;Positive voltage supply (traditionally for a BJT device, C=collector).&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V_BATT" urn="urn:adsk.eagle:component:39448/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;Battery Voltage Supply&lt;/h3&gt;
&lt;p&gt;Generic symbol for the battery input to a system.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="V_BATT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V" urn="urn:adsk.eagle:component:39435/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;3.3V Supply Symbol&lt;/h3&gt;
&lt;p&gt;Power supply symbol for a specifically-stated 3.3V source.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCCIO" urn="urn:adsk.eagle:component:39451/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;VCC I/O Supply&lt;/h3&gt;
&lt;p&gt;Power supply for a chip's input and output pins.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VCCIO" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Power_Symbols" urn="urn:adsk.eagle:library:16502351">
<description>&lt;B&gt;Supply &amp; Ground symbols</description>
<packages>
</packages>
<symbols>
<symbol name="GND-BAR" urn="urn:adsk.eagle:symbol:16502372/2" library_version="9">
<description>Ground (GND) Bar</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="0" y="-1.905" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND-BAR" urn="urn:adsk.eagle:component:16502419/4" prefix="SUPPLY" library_version="9">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt; - Ground (GND) Bar</description>
<gates>
<gate name="G$1" symbol="GND-BAR" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="GND" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connector" urn="urn:adsk.eagle:library:16378166">
<description>&lt;b&gt;Pin Headers,Terminal blocks, D-Sub, Backplane, FFC/FPC, Socket</description>
<packages>
<package name="1X02" urn="urn:adsk.eagle:footprint:22309/1" library_version="8">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90" urn="urn:adsk.eagle:footprint:22310/1" library_version="8">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
<package name="1X01" urn="urn:adsk.eagle:footprint:22382/1" library_version="8">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="1X02" urn="urn:adsk.eagle:package:22435/2" type="model" library_version="8">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X02"/>
</packageinstances>
</package3d>
<package3d name="1X02/90" urn="urn:adsk.eagle:package:22437/2" type="model" library_version="8">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X02/90"/>
</packageinstances>
</package3d>
<package3d name="1X01" urn="urn:adsk.eagle:package:22485/2" type="model" library_version="8">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X01"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD2" urn="urn:adsk.eagle:symbol:22308/1" library_version="8">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD1" urn="urn:adsk.eagle:symbol:22381/1" library_version="8">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X2" urn="urn:adsk.eagle:component:16494866/2" prefix="JP" library_version="8">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22435/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="CONNECTOR" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="PIN-HEADER" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22437/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="CONNECTOR" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="PIN-HEADER" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X1" urn="urn:adsk.eagle:component:16378168/3" prefix="JP" library_version="8">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22485/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="CONNECTOR" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="PIN-HEADER" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="testpad" urn="urn:adsk.eagle:library:385">
<description>&lt;b&gt;Test Pins/Pads&lt;/b&gt;&lt;p&gt;
Cream on SMD OFF.&lt;br&gt;
new: Attribute TP_SIGNAL_NAME&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="B1,27" urn="urn:adsk.eagle:footprint:27900/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.0024" layer="37"/>
<smd name="TP" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" cream="no"/>
<text x="-0.635" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-0.635" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="B2,54" urn="urn:adsk.eagle:footprint:27901/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.0024" layer="37"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="37"/>
<smd name="TP" x="0" y="0" dx="2.54" dy="2.54" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.397" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="P1-13" urn="urn:adsk.eagle:footprint:27902/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.3208" diameter="2.159" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-13Y" urn="urn:adsk.eagle:footprint:27903/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.3208" diameter="1.905" shape="long" rot="R90"/>
<text x="-0.889" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.81" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-17" urn="urn:adsk.eagle:footprint:27904/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.8128" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.7018" diameter="2.54" shape="octagon"/>
<text x="-1.143" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-17Y" urn="urn:adsk.eagle:footprint:27905/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.8128" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.7018" diameter="2.1208" shape="long" rot="R90"/>
<text x="-1.143" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.81" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-20" urn="urn:adsk.eagle:footprint:27906/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="2.0066" diameter="3.1496" shape="octagon"/>
<text x="-1.524" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-20Y" urn="urn:adsk.eagle:footprint:27907/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="2.0066" diameter="2.54" shape="long" rot="R90"/>
<text x="-1.27" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-4.445" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="TP06R" urn="urn:adsk.eagle:footprint:27908/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.6" dy="0.6" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP06SQ" urn="urn:adsk.eagle:footprint:27909/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.5996" dy="0.5996" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP07R" urn="urn:adsk.eagle:footprint:27910/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.7" dy="0.7" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP07SQ" urn="urn:adsk.eagle:footprint:27911/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.7" dy="0.7" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP08R" urn="urn:adsk.eagle:footprint:27912/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8" dy="0.8" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP08SQ" urn="urn:adsk.eagle:footprint:27913/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8" dy="0.8" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP09R" urn="urn:adsk.eagle:footprint:27914/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.9" dy="0.9" layer="1" roundness="100" cream="no"/>
<text x="-0.4501" y="0.5001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP09SQ" urn="urn:adsk.eagle:footprint:27915/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8998" dy="0.8998" layer="1" cream="no"/>
<text x="-0.4501" y="0.5001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP10R" urn="urn:adsk.eagle:footprint:27916/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" cream="no"/>
<text x="-0.5001" y="0.5499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP10SQ" urn="urn:adsk.eagle:footprint:27917/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1" dy="1" layer="1" cream="no"/>
<text x="-0.5001" y="0.5499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP11R" urn="urn:adsk.eagle:footprint:27918/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1" dy="1.1" layer="1" roundness="100" cream="no"/>
<text x="-0.5499" y="0.5999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP11SQ" urn="urn:adsk.eagle:footprint:27919/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1" dy="1.1" layer="1" cream="no"/>
<text x="-0.5499" y="0.5999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP12SQ" urn="urn:adsk.eagle:footprint:27920/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1998" dy="1.1998" layer="1" cream="no"/>
<text x="-0.5999" y="0.65" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP12R" urn="urn:adsk.eagle:footprint:27921/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.2" dy="1.2" layer="1" roundness="100" cream="no"/>
<text x="-0.5999" y="0.65" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP13R" urn="urn:adsk.eagle:footprint:27922/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.3" dy="1.3" layer="1" roundness="100" cream="no"/>
<text x="-0.65" y="0.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP14R" urn="urn:adsk.eagle:footprint:27923/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.4" dy="1.4" layer="1" roundness="100" cream="no"/>
<text x="-0.7" y="0.7501" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP15R" urn="urn:adsk.eagle:footprint:27924/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5" dy="1.5" layer="1" roundness="100" cream="no"/>
<text x="-0.7501" y="0.8001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP16R" urn="urn:adsk.eagle:footprint:27925/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.6" dy="1.6" layer="1" roundness="100" cream="no"/>
<text x="-0.8001" y="0.8499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP17R" urn="urn:adsk.eagle:footprint:27926/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.7" dy="1.7" layer="1" roundness="100" cream="no"/>
<text x="-0.8499" y="0.8999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP18R" urn="urn:adsk.eagle:footprint:27927/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8" dy="1.8" layer="1" roundness="100" cream="no"/>
<text x="-0.8999" y="0.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP19R" urn="urn:adsk.eagle:footprint:27928/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100" cream="no"/>
<text x="-0.95" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP20R" urn="urn:adsk.eagle:footprint:27929/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="2" dy="2" layer="1" roundness="100" cream="no"/>
<text x="-1" y="1.05" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP13SQ" urn="urn:adsk.eagle:footprint:27930/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.3" dy="1.3" layer="1" cream="no"/>
<text x="-0.65" y="0.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP14SQ" urn="urn:adsk.eagle:footprint:27931/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.4" dy="1.4" layer="1" cream="no"/>
<text x="-0.7" y="0.7501" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP15SQ" urn="urn:adsk.eagle:footprint:27932/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5" dy="1.5" layer="1" cream="no"/>
<text x="-0.7501" y="0.8001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP16SQ" urn="urn:adsk.eagle:footprint:27933/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5996" dy="1.5996" layer="1" cream="no"/>
<text x="-0.8001" y="0.8499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP17SQ" urn="urn:adsk.eagle:footprint:27934/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.7" dy="1.7" layer="1" cream="no"/>
<text x="-0.8499" y="0.8999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP18SQ" urn="urn:adsk.eagle:footprint:27935/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8" dy="1.8" layer="1" cream="no"/>
<text x="-0.8999" y="0.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP19SQ" urn="urn:adsk.eagle:footprint:27936/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8998" dy="1.8998" layer="1" cream="no"/>
<text x="-0.95" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP20SQ" urn="urn:adsk.eagle:footprint:27937/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="2" dy="2" layer="1" cream="no"/>
<text x="-1" y="1.05" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
</packages>
<packages3d>
<package3d name="B1,27" urn="urn:adsk.eagle:package:27944/2" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="B1,27"/>
</packageinstances>
</package3d>
<package3d name="B2,54" urn="urn:adsk.eagle:package:27948/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="B2,54"/>
</packageinstances>
</package3d>
<package3d name="P1-13" urn="urn:adsk.eagle:package:27946/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-13"/>
</packageinstances>
</package3d>
<package3d name="P1-13Y" urn="urn:adsk.eagle:package:27947/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-13Y"/>
</packageinstances>
</package3d>
<package3d name="P1-17" urn="urn:adsk.eagle:package:27949/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-17"/>
</packageinstances>
</package3d>
<package3d name="P1-17Y" urn="urn:adsk.eagle:package:27953/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-17Y"/>
</packageinstances>
</package3d>
<package3d name="P1-20" urn="urn:adsk.eagle:package:27950/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-20"/>
</packageinstances>
</package3d>
<package3d name="P1-20Y" urn="urn:adsk.eagle:package:27951/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-20Y"/>
</packageinstances>
</package3d>
<package3d name="TP06R" urn="urn:adsk.eagle:package:27954/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP06R"/>
</packageinstances>
</package3d>
<package3d name="TP06SQ" urn="urn:adsk.eagle:package:27952/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP06SQ"/>
</packageinstances>
</package3d>
<package3d name="TP07R" urn="urn:adsk.eagle:package:27970/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP07R"/>
</packageinstances>
</package3d>
<package3d name="TP07SQ" urn="urn:adsk.eagle:package:27955/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP07SQ"/>
</packageinstances>
</package3d>
<package3d name="TP08R" urn="urn:adsk.eagle:package:27956/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP08R"/>
</packageinstances>
</package3d>
<package3d name="TP08SQ" urn="urn:adsk.eagle:package:27960/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP08SQ"/>
</packageinstances>
</package3d>
<package3d name="TP09R" urn="urn:adsk.eagle:package:27958/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP09R"/>
</packageinstances>
</package3d>
<package3d name="TP09SQ" urn="urn:adsk.eagle:package:27957/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP09SQ"/>
</packageinstances>
</package3d>
<package3d name="TP10R" urn="urn:adsk.eagle:package:27959/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP10R"/>
</packageinstances>
</package3d>
<package3d name="TP10SQ" urn="urn:adsk.eagle:package:27962/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP10SQ"/>
</packageinstances>
</package3d>
<package3d name="TP11R" urn="urn:adsk.eagle:package:27961/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP11R"/>
</packageinstances>
</package3d>
<package3d name="TP11SQ" urn="urn:adsk.eagle:package:27965/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP11SQ"/>
</packageinstances>
</package3d>
<package3d name="TP12SQ" urn="urn:adsk.eagle:package:27964/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP12SQ"/>
</packageinstances>
</package3d>
<package3d name="TP12R" urn="urn:adsk.eagle:package:27963/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP12R"/>
</packageinstances>
</package3d>
<package3d name="TP13R" urn="urn:adsk.eagle:package:27967/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP13R"/>
</packageinstances>
</package3d>
<package3d name="TP14R" urn="urn:adsk.eagle:package:27966/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP14R"/>
</packageinstances>
</package3d>
<package3d name="TP15R" urn="urn:adsk.eagle:package:27968/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP15R"/>
</packageinstances>
</package3d>
<package3d name="TP16R" urn="urn:adsk.eagle:package:27969/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP16R"/>
</packageinstances>
</package3d>
<package3d name="TP17R" urn="urn:adsk.eagle:package:27971/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP17R"/>
</packageinstances>
</package3d>
<package3d name="TP18R" urn="urn:adsk.eagle:package:27981/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP18R"/>
</packageinstances>
</package3d>
<package3d name="TP19R" urn="urn:adsk.eagle:package:27972/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP19R"/>
</packageinstances>
</package3d>
<package3d name="TP20R" urn="urn:adsk.eagle:package:27973/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP20R"/>
</packageinstances>
</package3d>
<package3d name="TP13SQ" urn="urn:adsk.eagle:package:27974/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP13SQ"/>
</packageinstances>
</package3d>
<package3d name="TP14SQ" urn="urn:adsk.eagle:package:27984/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP14SQ"/>
</packageinstances>
</package3d>
<package3d name="TP15SQ" urn="urn:adsk.eagle:package:27975/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP15SQ"/>
</packageinstances>
</package3d>
<package3d name="TP16SQ" urn="urn:adsk.eagle:package:27976/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP16SQ"/>
</packageinstances>
</package3d>
<package3d name="TP17SQ" urn="urn:adsk.eagle:package:27977/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP17SQ"/>
</packageinstances>
</package3d>
<package3d name="TP18SQ" urn="urn:adsk.eagle:package:27979/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP18SQ"/>
</packageinstances>
</package3d>
<package3d name="TP19SQ" urn="urn:adsk.eagle:package:27978/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP19SQ"/>
</packageinstances>
</package3d>
<package3d name="TP20SQ" urn="urn:adsk.eagle:package:27980/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP20SQ"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PPAD" urn="urn:adsk.eagle:symbol:27939/1" library_version="3">
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="-1.27" radius="0.635" width="0.1524" layer="94"/>
<text x="-2.54" y="-4.445" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.778" layer="97">&gt;TP_SIGNAL_NAME</text>
<pin name="TP" x="0" y="2.54" visible="off" length="short" direction="in" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TPSQ" urn="urn:adsk.eagle:component:27991/3" prefix="TP" library_version="3">
<description>&lt;b&gt;Test pad&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PPAD" x="0" y="0"/>
</gates>
<devices>
<device name="B1,27" package="B1,27">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27944/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="B2,54" package="B2,54">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27948/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-13" package="P1-13">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27946/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-13Y" package="P1-13Y">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27947/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-17" package="P1-17">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27949/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-17Y" package="P1-17Y">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27953/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-20" package="P1-20">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27950/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-20Y" package="P1-20Y">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27951/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP06R" package="TP06R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27954/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP06SQ" package="TP06SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27952/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP07R" package="TP07R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27970/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP07SQ" package="TP07SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27955/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP08R" package="TP08R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27956/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP08SQ" package="TP08SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27960/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP09R" package="TP09R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27958/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP09SQ" package="TP09SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27957/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP10R" package="TP10R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27959/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP10SQ" package="TP10SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27962/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP11R" package="TP11R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27961/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP11SQ" package="TP11SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27965/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP12SQ" package="TP12SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27964/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP12R" package="TP12R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27963/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP13R" package="TP13R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27967/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP14R" package="TP14R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27966/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP15R" package="TP15R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27968/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP16R" package="TP16R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27969/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP17R" package="TP17R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27971/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP18R" package="TP18R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27981/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP19R" package="TP19R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27972/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP20R" package="TP20R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27973/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP13SQ" package="TP13SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27974/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP14SQ" package="TP14SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27984/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP15SQ" package="TP15SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27975/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP16SQ" package="TP16SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27976/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP17SQ" package="TP17SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27977/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP18SQ" package="TP18SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27979/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP19SQ" package="TP19SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27978/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP20SQ" package="TP20SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27980/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Switch" urn="urn:adsk.eagle:library:11396471">
<description>&lt;h3&gt; PCBLayout.com - Frequently Used &lt;i&gt;Switches&lt;/i&gt;&lt;/h3&gt;

Visit us at &lt;a href="http://www.PCBLayout.com"&gt;PCBLayout.com&lt;/a&gt; for quick and hassle-free PCB Layout/Manufacturing ordering experience.
&lt;BR&gt;
&lt;BR&gt;
This library has been generated by our experienced pcb layout engineers using current IPC and/or industry standards. We &lt;b&gt;believe&lt;/b&gt; the content to be accurate, complete and current. But, this content is provided as a courtesy and &lt;u&gt;user assumes all risk and responsiblity of it's usage&lt;/u&gt;.
&lt;BR&gt;
&lt;BR&gt;
Feel free to contact us at &lt;a href="mailto:Support@PCBLayout.com"&gt;Support@PCBLayout.com&lt;/a&gt; if you have any questions/concerns regarding any of our content or services.</description>
<packages>
<package name="1571610-2" urn="urn:adsk.eagle:footprint:17224137/2" library_version="4">
<pad name="1" x="-2.25" y="0" drill="1"/>
<pad name="2" x="2.25" y="0" drill="1"/>
<pad name="3" x="-3.5" y="-2.5" drill="1.3"/>
<pad name="4" x="3.5" y="-2.5" drill="1.3"/>
<wire x1="-3.45" y1="3.85" x2="-3.45" y2="-4" width="0.127" layer="51"/>
<wire x1="-3.45" y1="-4" x2="3.45" y2="-4" width="0.127" layer="51"/>
<wire x1="3.45" y1="-4" x2="3.45" y2="3.85" width="0.127" layer="51"/>
<wire x1="3.45" y1="3.85" x2="-3.45" y2="3.85" width="0.127" layer="51"/>
<wire x1="-3.45" y1="-1.2" x2="-3.45" y2="3.85" width="0.127" layer="21"/>
<wire x1="-3.45" y1="3.85" x2="3.45" y2="3.85" width="0.127" layer="21"/>
<wire x1="3.45" y1="3.85" x2="3.45" y2="-1.2" width="0.127" layer="21"/>
<wire x1="-3.45" y1="-3.7" x2="-3.45" y2="-4" width="0.127" layer="21"/>
<wire x1="-3.45" y1="-4" x2="3.45" y2="-4" width="0.127" layer="21"/>
<wire x1="3.45" y1="-4" x2="3.45" y2="-3.7" width="0.127" layer="21"/>
<wire x1="-4.75" y1="4.1" x2="-4.75" y2="-4.25" width="0.05" layer="39"/>
<wire x1="-4.75" y1="-4.25" x2="4.75" y2="-4.25" width="0.05" layer="39"/>
<wire x1="4.75" y1="-4.25" x2="4.75" y2="4.1" width="0.05" layer="39"/>
<wire x1="4.75" y1="4.1" x2="-4.75" y2="4.1" width="0.05" layer="39"/>
<text x="-5" y="5" size="1.778" layer="25">&gt;NAME</text>
<text x="-5" y="-7" size="1.778" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="1571610-2" urn="urn:adsk.eagle:package:17224151/3" type="model" library_version="4">
<packageinstances>
<packageinstance name="1571610-2"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="1571610-2" urn="urn:adsk.eagle:symbol:17276113/2" library_version="4">
<wire x1="1.905" y1="0" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<text x="-4.826" y="3.556" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-7.874" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="SH" x="7.62" y="-2.54" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1571610-2" urn="urn:adsk.eagle:component:17276127/2" prefix="SW" library_version="4">
<description>&lt;h3&gt; SWITCH TACTILE SPST-NO 0.05A 24V &lt;/h3&gt;
&lt;BR&gt;
&lt;a href="https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&amp;DocNm=1571610&amp;DocType=Customer+Drawing&amp;DocLang=English"&gt; Manufacturer's datasheet&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="1571610-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1571610-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="SH" pad="3 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17224151/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CREATED_BY" value="PCBLayout.com" constant="no"/>
<attribute name="DIGIKEY_PN" value="A117269-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TE Connectivity ALCOSWITCH Switches" constant="no"/>
<attribute name="MPN" value="1571610-2" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Crystals_Oscillators" urn="urn:adsk.eagle:library:24014151">
<description>&lt;h3&gt; PCBLayout.com - Frequently Used &lt;i&gt;Crystals/Oscillators&lt;/i&gt;&lt;/h3&gt;

Visit us at &lt;a href="http://www.PCBLayout.com"&gt;PCBLayout.com&lt;/a&gt; for quick and hassle-free PCB Layout/Manufacturing ordering experience.
&lt;BR&gt;
&lt;BR&gt;
This library has been generated by our experienced pcb layout engineers using current IPC and/or industry standards. We &lt;b&gt;believe&lt;/b&gt; the content to be accurate, complete and current. But, this content is provided as a courtesy and &lt;u&gt;user assumes all risk and responsiblity of it's usage&lt;/u&gt;.
&lt;BR&gt;
&lt;BR&gt;
Feel free to contact us at &lt;a href="mailto:Support@PCBLayout.com"&gt;Support@PCBLayout.com&lt;/a&gt; if you have any questions/concerns regarding any of our content or services.</description>
<packages>
<package name="9HT10-32.768KDZF-T" urn="urn:adsk.eagle:footprint:10899582/1" library_version="1">
<smd name="1" x="-1.25" y="0" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="2" x="1.25" y="0" dx="1.8" dy="1" layer="1" rot="R90"/>
<wire x1="-1.6" y1="0.75" x2="1.6" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.75" x2="1.6" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.75" x2="-1.6" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.75" x2="-1.6" y2="0.75" width="0.127" layer="51"/>
<wire x1="-0.58" y1="0.75" x2="0.58" y2="0.75" width="0.127" layer="21"/>
<wire x1="-0.56" y1="-0.75" x2="0.6" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2.05" y1="1.2" x2="2.05" y2="1.2" width="0.05" layer="39"/>
<wire x1="2.05" y1="1.2" x2="2.05" y2="-1.2" width="0.05" layer="39"/>
<wire x1="2.05" y1="-1.2" x2="-2.05" y2="-1.2" width="0.05" layer="39"/>
<wire x1="-2.05" y1="-1.2" x2="-2.05" y2="1.2" width="0.05" layer="39"/>
<text x="-2.75" y="1.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5" y="-2.75" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="9HT10-32.768KDZF-T" urn="urn:adsk.eagle:package:10899605/3" type="model" library_version="1">
<packageinstances>
<packageinstance name="9HT10-32.768KDZF-T"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="Q" urn="urn:adsk.eagle:symbol:24014181/1" library_version="1">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-3.302" y="2.286" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.556" y="-4.572" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="9HT10-32.768KDZF-T" urn="urn:adsk.eagle:component:24014212/1" prefix="X" library_version="1">
<description>&lt;h3&gt; CRYSTAL 32.7680KHZ 12.5PF SMD &lt;/h3&gt;
&lt;BR&gt;
&lt;a href="http://www.txccrystal.com/images/pdf/9ht10.pdf"&gt; Manufacturer's datasheet&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="Q" x="0" y="0"/>
</gates>
<devices>
<device name="" package="9HT10-32.768KDZF-T">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10899605/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CREATED_BY" value="PCBLayout.com" constant="no"/>
<attribute name="DIGIKEY_PART_NUMBER" value="887-1504-2-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TXC CORPORATION" constant="no"/>
<attribute name="MPN" value="9HT10-32.768KDZF-T" constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Circuit-Protection" urn="urn:adsk.eagle:library:16378135">
<description>&lt;B&gt;Fuse, TVS, Thermistor, Thyristor, Varistor, ESD Suppression</description>
<packages>
<package name="FUSC3215X82N" urn="urn:adsk.eagle:footprint:16378141/1" library_version="5">
<description>Chip, 3.20 X 1.58 X 0.82 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.58 X 0.82 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.2211" x2="-1.7" y2="1.2211" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.2211" x2="-1.7" y2="-1.2211" width="0.12" layer="21"/>
<wire x1="1.7" y1="-0.9" x2="-1.7" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.9" x2="-1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.9" x2="1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.9" x2="1.7" y2="-0.9" width="0.12" layer="51"/>
<smd name="1" x="-1.4754" y="0" dx="1.1646" dy="1.8143" layer="1"/>
<smd name="2" x="1.4754" y="0" dx="1.1646" dy="1.8143" layer="1"/>
<text x="0" y="1.8561" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8561" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="FUSAD2590W70L2150D550B" urn="urn:adsk.eagle:footprint:16378145/1" library_version="5">
<description>Axial Fuse, 25.90 mm pitch, 21.50 mm body length, 5.50 mm body diameter
&lt;p&gt;Axial Fuse package with 25.90 mm pitch (lead spacing), 0.70 mm lead diameter, 21.50 mm body length and 5.50 mm body diameter&lt;/p&gt;</description>
<wire x1="-11.25" y1="2.9" x2="-11.25" y2="-2.9" width="0.12" layer="21"/>
<wire x1="-11.25" y1="-2.9" x2="11.25" y2="-2.9" width="0.12" layer="21"/>
<wire x1="11.25" y1="-2.9" x2="11.25" y2="2.9" width="0.12" layer="21"/>
<wire x1="11.25" y1="2.9" x2="-11.25" y2="2.9" width="0.12" layer="21"/>
<wire x1="-11.25" y1="0" x2="-11.946" y2="0" width="0.12" layer="21"/>
<wire x1="11.25" y1="0" x2="11.946" y2="0" width="0.12" layer="21"/>
<wire x1="11.25" y1="-2.9" x2="-11.25" y2="-2.9" width="0.12" layer="51"/>
<wire x1="-11.25" y1="-2.9" x2="-11.25" y2="2.9" width="0.12" layer="51"/>
<wire x1="-11.25" y1="2.9" x2="11.25" y2="2.9" width="0.12" layer="51"/>
<wire x1="11.25" y1="2.9" x2="11.25" y2="-2.9" width="0.12" layer="51"/>
<pad name="1" x="-12.95" y="0" drill="0.9" diameter="1.5"/>
<pad name="2" x="12.95" y="0" drill="0.9" diameter="1.5"/>
<text x="0" y="3.535" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.535" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="FUSC1608X60N" urn="urn:adsk.eagle:footprint:16378142/1" library_version="5">
<description>Chip, 1.60 X 0.82 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.82 X 0.60 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.8009" x2="-0.85" y2="0.8009" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.8009" x2="-0.85" y2="-0.8009" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.475" x2="-0.85" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.475" x2="-0.85" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.475" x2="0.85" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.475" x2="0.85" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.8152" y="0" dx="0.7987" dy="0.9739" layer="1"/>
<smd name="2" x="0.8152" y="0" dx="0.7987" dy="0.9739" layer="1"/>
<text x="0" y="1.4359" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4359" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="FUSC6126X294N" urn="urn:adsk.eagle:footprint:16378144/1" library_version="5">
<description>Chip, 6.10 X 2.69 X 2.94 mm body
&lt;p&gt;Chip package with body size 6.10 X 2.69 X 2.94 mm&lt;/p&gt;</description>
<wire x1="3.15" y1="1.7902" x2="-3.15" y2="1.7902" width="0.12" layer="21"/>
<wire x1="3.15" y1="-1.7902" x2="-3.15" y2="-1.7902" width="0.12" layer="21"/>
<wire x1="3.15" y1="-1.47" x2="-3.15" y2="-1.47" width="0.12" layer="51"/>
<wire x1="-3.15" y1="-1.47" x2="-3.15" y2="1.47" width="0.12" layer="51"/>
<wire x1="-3.15" y1="1.47" x2="3.15" y2="1.47" width="0.12" layer="51"/>
<wire x1="3.15" y1="1.47" x2="3.15" y2="-1.47" width="0.12" layer="51"/>
<smd name="1" x="-2.5" y="0" dx="2.0153" dy="2.9523" layer="1"/>
<smd name="2" x="2.5" y="0" dx="2.0153" dy="2.9523" layer="1"/>
<text x="0" y="2.4252" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.4252" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="FUSC3215X82N" urn="urn:adsk.eagle:package:16378157/1" type="model" library_version="5">
<description>Chip, 3.20 X 1.58 X 0.82 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.58 X 0.82 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="FUSC3215X82N"/>
</packageinstances>
</package3d>
<package3d name="FUSAD2590W70L2150D550B" urn="urn:adsk.eagle:package:16378155/1" type="model" library_version="5">
<description>Axial Fuse, 25.90 mm pitch, 21.50 mm body length, 5.50 mm body diameter
&lt;p&gt;Axial Fuse package with 25.90 mm pitch (lead spacing), 0.70 mm lead diameter, 21.50 mm body length and 5.50 mm body diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="FUSAD2590W70L2150D550B"/>
</packageinstances>
</package3d>
<package3d name="FUSC1608X60N" urn="urn:adsk.eagle:package:16378156/1" type="model" library_version="5">
<description>Chip, 1.60 X 0.82 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.82 X 0.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="FUSC1608X60N"/>
</packageinstances>
</package3d>
<package3d name="FUSC6126X294N" urn="urn:adsk.eagle:package:16378154/1" type="model" library_version="5">
<description>Chip, 6.10 X 2.69 X 2.94 mm body
&lt;p&gt;Chip package with body size 6.10 X 2.69 X 2.94 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="FUSC6126X294N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="FUSE" urn="urn:adsk.eagle:symbol:16378138/1" library_version="5">
<wire x1="-3.81" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0.762" x2="-3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.397" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FUSE" urn="urn:adsk.eagle:component:16378165/5" prefix="F" library_version="5">
<description>&lt;b&gt; Fuse - Generic</description>
<gates>
<gate name="G$1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="AXIAL-26MM-PITCH" package="FUSAD2590W70L2150D550B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378155/1"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Circuit-Protection" constant="no"/>
<attribute name="CURRENT_RATING" value="" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fuse" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="FUSE" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0603(1608-METRIC)" package="FUSC1608X60N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378156/1"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Circuit-Protection" constant="no"/>
<attribute name="CURRENT_RATING" value="" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fuse" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="FUSE" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP(6126-METRIC)" package="FUSC6126X294N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378154/1"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Circuit-Protection" constant="no"/>
<attribute name="CURRENT_RATING" value="" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fuse" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="FUSE" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1206(3216-METRIC)" package="FUSC3215X82N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378157/1"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Circuit-Protection" constant="no"/>
<attribute name="CURRENT_RATING" value="" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fuse" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="FUSE" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diodes" urn="urn:adsk.eagle:library:11396254">
<description>&lt;h3&gt; PCBLayout.com - Frequently Used &lt;i&gt;Diodes&lt;/i&gt;&lt;/h3&gt;

Visit us at &lt;a href="http://www.PCBLayout.com"&gt;PCBLayout.com&lt;/a&gt; for quick and hassle-free PCB Layout/Manufacturing ordering experience.
&lt;BR&gt;
&lt;BR&gt;
This library has been generated by our experienced pcb layout engineers using current IPC and/or industry standards. We &lt;b&gt;believe&lt;/b&gt; the content to be accurate, complete and current. But, this content is provided as a courtesy and &lt;u&gt;user assumes all risk and responsiblity of it's usage&lt;/u&gt;.
&lt;BR&gt;
&lt;BR&gt;
Feel free to contact us at &lt;a href="mailto:Support@PCBLayout.com"&gt;Support@PCBLayout.com&lt;/a&gt; if you have any questions/concerns regarding any of our content or services.</description>
<packages>
<package name="SOD-123" urn="urn:adsk.eagle:footprint:10898382/1" locally_modified="yes" library_version="1">
<description>Diode, Small Outline Diode (SOD); 2.69 mm L X 1.60 mm W X 1.35 mm H body&lt;p&gt;&lt;i&gt;PCB Libraries Packages&lt;/i&gt;</description>
<smd name="C" x="-1.68" y="0" dx="1.02" dy="0.86" layer="1" roundness="51" rot="R180" stop="no" cream="no"/>
<smd name="A" x="1.68" y="0" dx="1.02" dy="0.86" layer="1" roundness="51" stop="no" cream="no"/>
<polygonshape width="0.01" layer="29">
<polygonoutlineobjects>
<vertex x="-1.3548125" y="-0.432253125"/>
<vertex x="-1.32045" y="-0.42396875"/>
<vertex x="-1.287825" y="-0.41046875"/>
<vertex x="-1.25775625" y="-0.392059375"/>
<vertex x="-1.230853125" y="-0.369146875"/>
<vertex x="-1.207940625" y="-0.34224375"/>
<vertex x="-1.18953125" y="-0.312175"/>
<vertex x="-1.17603125" y="-0.27955"/>
<vertex x="-1.167746875" y="-0.2451875"/>
<vertex x="-1.165" y="-0.21018125"/>
<vertex x="-1.167703125" y="0.245009375"/>
<vertex x="-1.17603125" y="0.27955"/>
<vertex x="-1.18953125" y="0.312175"/>
<vertex x="-1.207940625" y="0.34224375"/>
<vertex x="-1.230853125" y="0.369146875"/>
<vertex x="-1.25775625" y="0.392059375"/>
<vertex x="-1.287825" y="0.41046875"/>
<vertex x="-1.32045" y="0.42396875"/>
<vertex x="-1.3548125" y="0.432253125"/>
<vertex x="-1.389803125" y="0.435"/>
<vertex x="-1.970196875" y="0.435"/>
<vertex x="-2.0051875" y="0.432253125"/>
<vertex x="-2.03955" y="0.42396875"/>
<vertex x="-2.072175" y="0.41046875"/>
<vertex x="-2.10224375" y="0.392059375"/>
<vertex x="-2.129146875" y="0.369146875"/>
<vertex x="-2.152059375" y="0.34224375"/>
<vertex x="-2.17046875" y="0.312175"/>
<vertex x="-2.18396875" y="0.27955"/>
<vertex x="-2.192253125" y="0.2451875"/>
<vertex x="-2.195" y="0.21018125"/>
<vertex x="-2.192296875" y="-0.245009375"/>
<vertex x="-2.18396875" y="-0.27955"/>
<vertex x="-2.17046875" y="-0.312175"/>
<vertex x="-2.152059375" y="-0.34224375"/>
<vertex x="-2.129146875" y="-0.369146875"/>
<vertex x="-2.10224375" y="-0.392059375"/>
<vertex x="-2.072175" y="-0.41046875"/>
<vertex x="-2.03955" y="-0.42396875"/>
<vertex x="-2.0051875" y="-0.432253125"/>
<vertex x="-1.970196875" y="-0.435"/>
<vertex x="-1.389803125" y="-0.435"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="-1.3548125" y="-0.432253125"/>
<vertex x="-1.32045" y="-0.42396875"/>
<vertex x="-1.287825" y="-0.41046875"/>
<vertex x="-1.25775625" y="-0.392059375"/>
<vertex x="-1.230853125" y="-0.369146875"/>
<vertex x="-1.207940625" y="-0.34224375"/>
<vertex x="-1.18953125" y="-0.312175"/>
<vertex x="-1.17603125" y="-0.27955"/>
<vertex x="-1.167746875" y="-0.2451875"/>
<vertex x="-1.165" y="-0.21018125"/>
<vertex x="-1.167703125" y="0.245009375"/>
<vertex x="-1.17603125" y="0.27955"/>
<vertex x="-1.18953125" y="0.312175"/>
<vertex x="-1.207940625" y="0.34224375"/>
<vertex x="-1.230853125" y="0.369146875"/>
<vertex x="-1.25775625" y="0.392059375"/>
<vertex x="-1.287825" y="0.41046875"/>
<vertex x="-1.32045" y="0.42396875"/>
<vertex x="-1.3548125" y="0.432253125"/>
<vertex x="-1.389803125" y="0.435"/>
<vertex x="-1.970196875" y="0.435"/>
<vertex x="-2.0051875" y="0.432253125"/>
<vertex x="-2.03955" y="0.42396875"/>
<vertex x="-2.072175" y="0.41046875"/>
<vertex x="-2.10224375" y="0.392059375"/>
<vertex x="-2.129146875" y="0.369146875"/>
<vertex x="-2.152059375" y="0.34224375"/>
<vertex x="-2.17046875" y="0.312175"/>
<vertex x="-2.18396875" y="0.27955"/>
<vertex x="-2.192253125" y="0.2451875"/>
<vertex x="-2.195" y="0.21018125"/>
<vertex x="-2.192296875" y="-0.245009375"/>
<vertex x="-2.18396875" y="-0.27955"/>
<vertex x="-2.17046875" y="-0.312175"/>
<vertex x="-2.152059375" y="-0.34224375"/>
<vertex x="-2.129146875" y="-0.369146875"/>
<vertex x="-2.10224375" y="-0.392059375"/>
<vertex x="-2.072175" y="-0.41046875"/>
<vertex x="-2.03955" y="-0.42396875"/>
<vertex x="-2.0051875" y="-0.432253125"/>
<vertex x="-1.970196875" y="-0.435"/>
<vertex x="-1.389803125" y="-0.435"/>
<vertex x="-1.3548125" y="-0.432253125"/>
</polygonoutlinesegments>
</polygonshape>
<polygonshape width="0.01" layer="31">
<polygonoutlineobjects>
<vertex x="-1.3548125" y="-0.432253125"/>
<vertex x="-1.32045" y="-0.42396875"/>
<vertex x="-1.287825" y="-0.41046875"/>
<vertex x="-1.25775625" y="-0.392059375"/>
<vertex x="-1.230853125" y="-0.369146875"/>
<vertex x="-1.207940625" y="-0.34224375"/>
<vertex x="-1.18953125" y="-0.312175"/>
<vertex x="-1.17603125" y="-0.27955"/>
<vertex x="-1.167746875" y="-0.2451875"/>
<vertex x="-1.165" y="-0.21018125"/>
<vertex x="-1.167703125" y="0.245009375"/>
<vertex x="-1.17603125" y="0.27955"/>
<vertex x="-1.18953125" y="0.312175"/>
<vertex x="-1.207940625" y="0.34224375"/>
<vertex x="-1.230853125" y="0.369146875"/>
<vertex x="-1.25775625" y="0.392059375"/>
<vertex x="-1.287825" y="0.41046875"/>
<vertex x="-1.32045" y="0.42396875"/>
<vertex x="-1.3548125" y="0.432253125"/>
<vertex x="-1.389803125" y="0.435"/>
<vertex x="-1.970196875" y="0.435"/>
<vertex x="-2.0051875" y="0.432253125"/>
<vertex x="-2.03955" y="0.42396875"/>
<vertex x="-2.072175" y="0.41046875"/>
<vertex x="-2.10224375" y="0.392059375"/>
<vertex x="-2.129146875" y="0.369146875"/>
<vertex x="-2.152059375" y="0.34224375"/>
<vertex x="-2.17046875" y="0.312175"/>
<vertex x="-2.18396875" y="0.27955"/>
<vertex x="-2.192253125" y="0.2451875"/>
<vertex x="-2.195" y="0.21018125"/>
<vertex x="-2.192296875" y="-0.245009375"/>
<vertex x="-2.18396875" y="-0.27955"/>
<vertex x="-2.17046875" y="-0.312175"/>
<vertex x="-2.152059375" y="-0.34224375"/>
<vertex x="-2.129146875" y="-0.369146875"/>
<vertex x="-2.10224375" y="-0.392059375"/>
<vertex x="-2.072175" y="-0.41046875"/>
<vertex x="-2.03955" y="-0.42396875"/>
<vertex x="-2.0051875" y="-0.432253125"/>
<vertex x="-1.970196875" y="-0.435"/>
<vertex x="-1.389803125" y="-0.435"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="-1.3548125" y="-0.432253125"/>
<vertex x="-1.32045" y="-0.42396875"/>
<vertex x="-1.287825" y="-0.41046875"/>
<vertex x="-1.25775625" y="-0.392059375"/>
<vertex x="-1.230853125" y="-0.369146875"/>
<vertex x="-1.207940625" y="-0.34224375"/>
<vertex x="-1.18953125" y="-0.312175"/>
<vertex x="-1.17603125" y="-0.27955"/>
<vertex x="-1.167746875" y="-0.2451875"/>
<vertex x="-1.165" y="-0.21018125"/>
<vertex x="-1.167703125" y="0.245009375"/>
<vertex x="-1.17603125" y="0.27955"/>
<vertex x="-1.18953125" y="0.312175"/>
<vertex x="-1.207940625" y="0.34224375"/>
<vertex x="-1.230853125" y="0.369146875"/>
<vertex x="-1.25775625" y="0.392059375"/>
<vertex x="-1.287825" y="0.41046875"/>
<vertex x="-1.32045" y="0.42396875"/>
<vertex x="-1.3548125" y="0.432253125"/>
<vertex x="-1.389803125" y="0.435"/>
<vertex x="-1.970196875" y="0.435"/>
<vertex x="-2.0051875" y="0.432253125"/>
<vertex x="-2.03955" y="0.42396875"/>
<vertex x="-2.072175" y="0.41046875"/>
<vertex x="-2.10224375" y="0.392059375"/>
<vertex x="-2.129146875" y="0.369146875"/>
<vertex x="-2.152059375" y="0.34224375"/>
<vertex x="-2.17046875" y="0.312175"/>
<vertex x="-2.18396875" y="0.27955"/>
<vertex x="-2.192253125" y="0.2451875"/>
<vertex x="-2.195" y="0.21018125"/>
<vertex x="-2.192296875" y="-0.245009375"/>
<vertex x="-2.18396875" y="-0.27955"/>
<vertex x="-2.17046875" y="-0.312175"/>
<vertex x="-2.152059375" y="-0.34224375"/>
<vertex x="-2.129146875" y="-0.369146875"/>
<vertex x="-2.10224375" y="-0.392059375"/>
<vertex x="-2.072175" y="-0.41046875"/>
<vertex x="-2.03955" y="-0.42396875"/>
<vertex x="-2.0051875" y="-0.432253125"/>
<vertex x="-1.970196875" y="-0.435"/>
<vertex x="-1.389803125" y="-0.435"/>
<vertex x="-1.3548125" y="-0.432253125"/>
</polygonoutlinesegments>
</polygonshape>
<polygonshape width="0.01" layer="29">
<polygonoutlineobjects>
<vertex x="2.0051875" y="-0.432253125"/>
<vertex x="2.03955" y="-0.42396875"/>
<vertex x="2.072175" y="-0.41046875"/>
<vertex x="2.10224375" y="-0.392059375"/>
<vertex x="2.129146875" y="-0.369146875"/>
<vertex x="2.152059375" y="-0.34224375"/>
<vertex x="2.17046875" y="-0.312175"/>
<vertex x="2.18396875" y="-0.27955"/>
<vertex x="2.192253125" y="-0.2451875"/>
<vertex x="2.195" y="-0.21018125"/>
<vertex x="2.192296875" y="0.245009375"/>
<vertex x="2.18396875" y="0.27955"/>
<vertex x="2.17046875" y="0.312175"/>
<vertex x="2.152059375" y="0.34224375"/>
<vertex x="2.129146875" y="0.369146875"/>
<vertex x="2.10224375" y="0.392059375"/>
<vertex x="2.072175" y="0.41046875"/>
<vertex x="2.03955" y="0.42396875"/>
<vertex x="2.0051875" y="0.432253125"/>
<vertex x="1.970196875" y="0.435"/>
<vertex x="1.389803125" y="0.435"/>
<vertex x="1.3548125" y="0.432253125"/>
<vertex x="1.32045" y="0.42396875"/>
<vertex x="1.287825" y="0.41046875"/>
<vertex x="1.25775625" y="0.392059375"/>
<vertex x="1.230853125" y="0.369146875"/>
<vertex x="1.207940625" y="0.34224375"/>
<vertex x="1.18953125" y="0.312175"/>
<vertex x="1.17603125" y="0.27955"/>
<vertex x="1.167746875" y="0.2451875"/>
<vertex x="1.165" y="0.21018125"/>
<vertex x="1.167703125" y="-0.245009375"/>
<vertex x="1.17603125" y="-0.27955"/>
<vertex x="1.18953125" y="-0.312175"/>
<vertex x="1.207940625" y="-0.34224375"/>
<vertex x="1.230853125" y="-0.369146875"/>
<vertex x="1.25775625" y="-0.392059375"/>
<vertex x="1.287825" y="-0.41046875"/>
<vertex x="1.32045" y="-0.42396875"/>
<vertex x="1.3548125" y="-0.432253125"/>
<vertex x="1.389803125" y="-0.435"/>
<vertex x="1.970196875" y="-0.435"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="2.0051875" y="-0.432253125"/>
<vertex x="2.03955" y="-0.42396875"/>
<vertex x="2.072175" y="-0.41046875"/>
<vertex x="2.10224375" y="-0.392059375"/>
<vertex x="2.129146875" y="-0.369146875"/>
<vertex x="2.152059375" y="-0.34224375"/>
<vertex x="2.17046875" y="-0.312175"/>
<vertex x="2.18396875" y="-0.27955"/>
<vertex x="2.192253125" y="-0.2451875"/>
<vertex x="2.195" y="-0.21018125"/>
<vertex x="2.192296875" y="0.245009375"/>
<vertex x="2.18396875" y="0.27955"/>
<vertex x="2.17046875" y="0.312175"/>
<vertex x="2.152059375" y="0.34224375"/>
<vertex x="2.129146875" y="0.369146875"/>
<vertex x="2.10224375" y="0.392059375"/>
<vertex x="2.072175" y="0.41046875"/>
<vertex x="2.03955" y="0.42396875"/>
<vertex x="2.0051875" y="0.432253125"/>
<vertex x="1.970196875" y="0.435"/>
<vertex x="1.389803125" y="0.435"/>
<vertex x="1.3548125" y="0.432253125"/>
<vertex x="1.32045" y="0.42396875"/>
<vertex x="1.287825" y="0.41046875"/>
<vertex x="1.25775625" y="0.392059375"/>
<vertex x="1.230853125" y="0.369146875"/>
<vertex x="1.207940625" y="0.34224375"/>
<vertex x="1.18953125" y="0.312175"/>
<vertex x="1.17603125" y="0.27955"/>
<vertex x="1.167746875" y="0.2451875"/>
<vertex x="1.165" y="0.21018125"/>
<vertex x="1.167703125" y="-0.245009375"/>
<vertex x="1.17603125" y="-0.27955"/>
<vertex x="1.18953125" y="-0.312175"/>
<vertex x="1.207940625" y="-0.34224375"/>
<vertex x="1.230853125" y="-0.369146875"/>
<vertex x="1.25775625" y="-0.392059375"/>
<vertex x="1.287825" y="-0.41046875"/>
<vertex x="1.32045" y="-0.42396875"/>
<vertex x="1.3548125" y="-0.432253125"/>
<vertex x="1.389803125" y="-0.435"/>
<vertex x="1.970196875" y="-0.435"/>
<vertex x="2.0051875" y="-0.432253125"/>
</polygonoutlinesegments>
</polygonshape>
<polygonshape width="0.01" layer="31">
<polygonoutlineobjects>
<vertex x="2.0051875" y="-0.432253125"/>
<vertex x="2.03955" y="-0.42396875"/>
<vertex x="2.072175" y="-0.41046875"/>
<vertex x="2.10224375" y="-0.392059375"/>
<vertex x="2.129146875" y="-0.369146875"/>
<vertex x="2.152059375" y="-0.34224375"/>
<vertex x="2.17046875" y="-0.312175"/>
<vertex x="2.18396875" y="-0.27955"/>
<vertex x="2.192253125" y="-0.2451875"/>
<vertex x="2.195" y="-0.21018125"/>
<vertex x="2.192296875" y="0.245009375"/>
<vertex x="2.18396875" y="0.27955"/>
<vertex x="2.17046875" y="0.312175"/>
<vertex x="2.152059375" y="0.34224375"/>
<vertex x="2.129146875" y="0.369146875"/>
<vertex x="2.10224375" y="0.392059375"/>
<vertex x="2.072175" y="0.41046875"/>
<vertex x="2.03955" y="0.42396875"/>
<vertex x="2.0051875" y="0.432253125"/>
<vertex x="1.970196875" y="0.435"/>
<vertex x="1.389803125" y="0.435"/>
<vertex x="1.3548125" y="0.432253125"/>
<vertex x="1.32045" y="0.42396875"/>
<vertex x="1.287825" y="0.41046875"/>
<vertex x="1.25775625" y="0.392059375"/>
<vertex x="1.230853125" y="0.369146875"/>
<vertex x="1.207940625" y="0.34224375"/>
<vertex x="1.18953125" y="0.312175"/>
<vertex x="1.17603125" y="0.27955"/>
<vertex x="1.167746875" y="0.2451875"/>
<vertex x="1.165" y="0.21018125"/>
<vertex x="1.167703125" y="-0.245009375"/>
<vertex x="1.17603125" y="-0.27955"/>
<vertex x="1.18953125" y="-0.312175"/>
<vertex x="1.207940625" y="-0.34224375"/>
<vertex x="1.230853125" y="-0.369146875"/>
<vertex x="1.25775625" y="-0.392059375"/>
<vertex x="1.287825" y="-0.41046875"/>
<vertex x="1.32045" y="-0.42396875"/>
<vertex x="1.3548125" y="-0.432253125"/>
<vertex x="1.389803125" y="-0.435"/>
<vertex x="1.970196875" y="-0.435"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="2.0051875" y="-0.432253125"/>
<vertex x="2.03955" y="-0.42396875"/>
<vertex x="2.072175" y="-0.41046875"/>
<vertex x="2.10224375" y="-0.392059375"/>
<vertex x="2.129146875" y="-0.369146875"/>
<vertex x="2.152059375" y="-0.34224375"/>
<vertex x="2.17046875" y="-0.312175"/>
<vertex x="2.18396875" y="-0.27955"/>
<vertex x="2.192253125" y="-0.2451875"/>
<vertex x="2.195" y="-0.21018125"/>
<vertex x="2.192296875" y="0.245009375"/>
<vertex x="2.18396875" y="0.27955"/>
<vertex x="2.17046875" y="0.312175"/>
<vertex x="2.152059375" y="0.34224375"/>
<vertex x="2.129146875" y="0.369146875"/>
<vertex x="2.10224375" y="0.392059375"/>
<vertex x="2.072175" y="0.41046875"/>
<vertex x="2.03955" y="0.42396875"/>
<vertex x="2.0051875" y="0.432253125"/>
<vertex x="1.970196875" y="0.435"/>
<vertex x="1.389803125" y="0.435"/>
<vertex x="1.3548125" y="0.432253125"/>
<vertex x="1.32045" y="0.42396875"/>
<vertex x="1.287825" y="0.41046875"/>
<vertex x="1.25775625" y="0.392059375"/>
<vertex x="1.230853125" y="0.369146875"/>
<vertex x="1.207940625" y="0.34224375"/>
<vertex x="1.18953125" y="0.312175"/>
<vertex x="1.17603125" y="0.27955"/>
<vertex x="1.167746875" y="0.2451875"/>
<vertex x="1.165" y="0.21018125"/>
<vertex x="1.167703125" y="-0.245009375"/>
<vertex x="1.17603125" y="-0.27955"/>
<vertex x="1.18953125" y="-0.312175"/>
<vertex x="1.207940625" y="-0.34224375"/>
<vertex x="1.230853125" y="-0.369146875"/>
<vertex x="1.25775625" y="-0.392059375"/>
<vertex x="1.287825" y="-0.41046875"/>
<vertex x="1.32045" y="-0.42396875"/>
<vertex x="1.3548125" y="-0.432253125"/>
<vertex x="1.389803125" y="-0.435"/>
<vertex x="1.970196875" y="-0.435"/>
<vertex x="2.0051875" y="-0.432253125"/>
</polygonoutlinesegments>
</polygonshape>
<wire x1="-1.605" y1="0.305" x2="-1.605" y2="-0.305" width="0.025" layer="51"/>
<wire x1="-1.605" y1="-0.305" x2="-1.855" y2="-0.305" width="0.025" layer="51"/>
<wire x1="-1.855" y1="-0.305" x2="-1.855" y2="0.305" width="0.025" layer="51"/>
<wire x1="-1.855" y1="0.305" x2="-1.605" y2="0.305" width="0.025" layer="51"/>
<wire x1="1.605" y1="-0.305" x2="1.605" y2="0.305" width="0.025" layer="51"/>
<wire x1="1.605" y1="0.305" x2="1.855" y2="0.305" width="0.025" layer="51"/>
<wire x1="1.855" y1="0.305" x2="1.855" y2="-0.305" width="0.025" layer="51"/>
<wire x1="1.855" y1="-0.305" x2="1.605" y2="-0.305" width="0.025" layer="51"/>
<wire x1="-1.42" y1="-0.9" x2="-1.42" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.42" y1="0.9" x2="1.42" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.42" y1="0.9" x2="1.42" y2="-0.9" width="0.12" layer="51"/>
<wire x1="1.42" y1="-0.9" x2="-1.42" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.42" y1="0.61" x2="-1.42" y2="0.9" width="0.12" layer="21"/>
<wire x1="1.42" y1="-0.61" x2="1.42" y2="-0.9" width="0.12" layer="21"/>
<wire x1="0.35" y1="0" x2="-0.35" y2="0" width="0.05" layer="39"/>
<wire x1="0" y1="0.35" x2="0" y2="-0.35" width="0.05" layer="39"/>
<wire x1="1.42" y1="-0.9" x2="-1.42" y2="-0.9" width="0.12" layer="21"/>
<wire x1="-1.42" y1="-0.9" x2="-1.42" y2="-0.61" width="0.12" layer="21"/>
<wire x1="-1.42" y1="0.9" x2="1.42" y2="0.9" width="0.12" layer="21"/>
<wire x1="1.42" y1="0.9" x2="1.42" y2="0.61" width="0.12" layer="21"/>
<wire x1="-1.62" y1="-1.1" x2="1.62" y2="-1.1" width="0.05" layer="39"/>
<wire x1="1.62" y1="-1.1" x2="1.62" y2="-0.63" width="0.05" layer="39"/>
<wire x1="1.62" y1="-0.63" x2="2.39" y2="-0.63" width="0.05" layer="39"/>
<wire x1="2.39" y1="-0.63" x2="2.39" y2="0.63" width="0.05" layer="39"/>
<wire x1="2.39" y1="0.63" x2="1.62" y2="0.63" width="0.05" layer="39"/>
<wire x1="1.62" y1="0.63" x2="1.62" y2="1.1" width="0.05" layer="39"/>
<wire x1="1.62" y1="1.1" x2="-1.62" y2="1.1" width="0.05" layer="39"/>
<wire x1="-1.62" y1="1.1" x2="-1.62" y2="0.63" width="0.05" layer="39"/>
<wire x1="-1.62" y1="0.63" x2="-2.39" y2="0.63" width="0.05" layer="39"/>
<wire x1="-2.39" y1="0.63" x2="-2.39" y2="-0.63" width="0.05" layer="39"/>
<wire x1="-2.39" y1="-0.63" x2="-1.62" y2="-0.63" width="0.05" layer="39"/>
<wire x1="-1.62" y1="-0.63" x2="-1.62" y2="-1.1" width="0.05" layer="39"/>
<wire x1="0.25" y1="0.25" x2="0.25" y2="-0.25" width="0.05" layer="21"/>
<wire x1="0.25" y1="-0.25" x2="-0.25" y2="0" width="0.05" layer="21"/>
<wire x1="-0.25" y1="0" x2="0.25" y2="0.25" width="0.05" layer="21"/>
<wire x1="-0.25" y1="0.25" x2="-0.25" y2="-0.25" width="0.05" layer="21"/>
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<text x="-2.54" y="1.524" size="1.32" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.2" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SOD-123" urn="urn:adsk.eagle:package:10898395/2" type="model" library_version="1">
<description>Diode, Small Outline Diode (SOD); 2.69 mm L X 1.60 mm W X 1.35 mm H body&lt;p&gt;&lt;i&gt;PCB Libraries Packages&lt;/i&gt;</description>
<packageinstances>
<packageinstance name="SOD-123"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="D-ZENER" urn="urn:adsk.eagle:symbol:10898385/1" library_version="1">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.27" x2="1.397" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.397" y1="-1.27" x2="0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.27" x2="1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="-2.9464" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.4704" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CESD5V0D1" urn="urn:adsk.eagle:component:10898402/9" prefix="D" library_version="1">
<description>&lt;h3&gt;SOD-123 Plastic-Encapsulate Diodes Uni-direction ESD Protection Diode &lt;/h3&gt;
&lt;BR&gt;
&lt;a href="https://seeeddoc.github.io/Open_parts_library/res/304060001.pdf"&gt; Manufacturer's datasheet&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="D-ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10898395/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CREATED_BY" value="PCBLayout.com" constant="no"/>
<attribute name="MANUFACTURER" value="JCET" constant="no"/>
<attribute name="MPN" value="CESD5V0D1" constant="no"/>
<attribute name="PACKAGE" value="SOD-123" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Jumpers" urn="urn:adsk.eagle:library:528">
<description>&lt;h3&gt;SparkFun Jumpers&lt;/h3&gt;
In this library you'll find jumpers, or other semipermanent means of changing current paths. The least permanent form is the solder jumper. These can be changed by adding, removing, or moving solder. In cases that are less likely to be changed we have jumpers that are connected with traces. These can be cut with a razor, or reconnected with solder. Reference designator JP.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SMT-JUMPER_2_NC_TRACE_NO-SILK" urn="urn:adsk.eagle:footprint:39257/1" locally_modified="yes" library_version="1">
<smd name="1" x="-0.508" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0.508" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-0.508" y1="0" x2="0.508" y2="0" width="0.254" layer="1"/>
<polygonshape width="0.127" layer="29">
<polygonoutlineobjects>
<vertex x="0.254" y="0.1905"/>
<vertex x="-0.254" y="0.1905"/>
<vertex x="-0.254" y="-0.1905"/>
<vertex x="0.254" y="-0.1905"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="0.254" y="0.1905"/>
<vertex x="-0.254" y="0.1905"/>
<vertex x="-0.254" y="-0.1905"/>
<vertex x="0.254" y="-0.1905"/>
<vertex x="0.254" y="0.1905"/>
</polygonoutlinesegments>
</polygonshape>
</package>
<package name="SMT-JUMPER_2_NC_TRACE_SILK" urn="urn:adsk.eagle:footprint:39258/1" locally_modified="yes" library_version="1">
<wire x1="0.762" y1="-1.016" x2="-0.762" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="0.762" y1="1.016" x2="1.2192" y2="0.5588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.2192" y1="0.5588" x2="-0.762" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.2192" y1="-0.5588" x2="-0.762" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="-1.016" x2="1.2192" y2="-0.5588" width="0.1524" layer="21" curve="90"/>
<wire x1="1.2192" y1="-0.5588" x2="1.2192" y2="0.5588" width="0.1524" layer="21"/>
<wire x1="-1.2192" y1="-0.5588" x2="-1.2192" y2="0.5588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.016" x2="0.762" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0" x2="0.508" y2="0" width="0.254" layer="1"/>
<smd name="1" x="-0.508" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0.508" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygonshape width="0.127" layer="29">
<polygonoutlineobjects>
<vertex x="0.254" y="0.1905"/>
<vertex x="-0.254" y="0.1905"/>
<vertex x="-0.254" y="-0.1905"/>
<vertex x="0.254" y="-0.1905"/>
</polygonoutlineobjects>
<polygonoutlinesegments>
<vertex x="0.254" y="0.1905"/>
<vertex x="-0.254" y="0.1905"/>
<vertex x="-0.254" y="-0.1905"/>
<vertex x="0.254" y="-0.1905"/>
<vertex x="0.254" y="0.1905"/>
</polygonoutlinesegments>
</polygonshape>
</package>
</packages>
<packages3d>
<package3d name="SMT-JUMPER_2_NC_TRACE_NO-SILK" urn="urn:adsk.eagle:package:39286/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="SMT-JUMPER_2_NC_TRACE_NO-SILK"/>
</packageinstances>
</package3d>
<package3d name="SMT-JUMPER_2_NC_TRACE_SILK" urn="urn:adsk.eagle:package:39281/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="SMT-JUMPER_2_NC_TRACE_SILK"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SMT-JUMPER_2_NC_TRACE" urn="urn:adsk.eagle:symbol:39256/1" library_version="1">
<wire x1="0.381" y1="0.635" x2="1.016" y2="0" width="1.27" layer="94" curve="-90" cap="flat"/>
<wire x1="1.016" y1="0" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-90" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="1.016" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JUMPER-SMT_2_NC_TRACE" urn="urn:adsk.eagle:component:39295/1" prefix="JP" library_version="1">
<description>&lt;h3&gt;Normally closed trace jumper&lt;/h3&gt;
&lt;p&gt;This jumper has a trace between two pads so it's normally closed (NC). Use a razor knife to open the connection. For best results follow the IPC guidelines for cutting traces:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Cutout at least 0.063 mm (0.005 in).&lt;/li&gt;
&lt;li&gt;Remove all loose material to clean up the cut area.&lt;/li&gt;
&lt;li&gt;Seal the cut with an approved epoxy.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Reapply solder to reclose the connection.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SMT-JUMPER_2_NC_TRACE" x="0" y="0"/>
</gates>
<devices>
<device name="_NO-SILK" package="SMT-JUMPER_2_NC_TRACE_NO-SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39286/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SILK" package="SMT-JUMPER_2_NC_TRACE_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39281/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-small-signal" urn="urn:adsk.eagle:library:401">
<description>&lt;b&gt;Small Signal Transistors&lt;/b&gt;&lt;p&gt;
Packages from :&lt;br&gt;
www.infineon.com; &lt;br&gt;
www.semiconductors.com;&lt;br&gt;
www.irf.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT23" urn="urn:adsk.eagle:footprint:28669/1" library_version="5">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="SOT23" urn="urn:adsk.eagle:package:28738/2" type="model" library_version="5">
<description>SOT-23</description>
<packageinstances>
<packageinstance name="SOT23"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="P-MOS" urn="urn:adsk.eagle:symbol:29640/1" library_version="5">
<wire x1="0" y1="0" x2="-1.016" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.381" x2="-1.016" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-0.381" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.381" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.27" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-3.048" x2="1.27" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.794" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="1.778" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="0.762" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="-0.889" y2="-0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-0.127" x2="-0.889" y2="0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="0.127" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.635" x2="1.524" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.635" x2="1.27" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.016" y2="-0.635" width="0.254" layer="94"/>
<circle x="0" y="2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-3.048" radius="0.3592" width="0" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BSS84" urn="urn:adsk.eagle:component:29809/4" prefix="Q" library_version="5">
<description>&lt;b&gt;P-CHANNEL MOS FET&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="P-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:28738/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="8" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="HigherPower" width="0.254" drill="0">
</class>
<class number="2" name="LowerPower" width="0.2032" drill="0">
</class>
<class number="3" name="MidPower" width="0.254" drill="0">
</class>
<class number="4" name="Ground" width="0.254" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="alicedee" library_urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA" deviceset="BNO080" device="" package3d_urn="urn:adsk.wipprod:fs.file:vf.j03Sz7myTxW003tCMTxK2w?version=2"/>
<part name="J1" library="alicedee" library_urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA" deviceset="USB4105-GF-A" device="" package3d_urn="urn:adsk.wipprod:fs.file:vf.m2fzCX6STRWF-vn2nMNhaA?version=5"/>
<part name="U3" library="alicedee" library_urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA" deviceset="AP2112-V3.3" device="" package3d_urn="urn:adsk.wipprod:fs.file:vf.FlSLHj1wRFKTmmfP59244Q?version=2" value="AP2112-V3.3"/>
<part name="Q1" library="transistor-small-signal" library_urn="urn:adsk.eagle:library:401" deviceset="BSS84" device="" package3d_urn="urn:adsk.eagle:package:28738/2" value="SSM3J328R,LF"/>
<part name="D3" library="LED" library_urn="urn:adsk.eagle:library:22900745" deviceset="CHIP-ROUND-R" device="_0603" package3d_urn="urn:adsk.wipprod:fs.file:vf.n1ru9s2WSdGmSoMZDXS2cg?version=1"/>
<part name="C1" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="10uF"/>
<part name="R1" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="1K"/>
<part name="R2" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="100K"/>
<part name="SUPPLY1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device=""/>
<part name="SUPPLY2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="SUPPLY3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="3.3V" device=""/>
<part name="SUPPLY4" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY5" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="SUPPLY6" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device=""/>
<part name="SUPPLY7" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="D4" library="Diodes" library_urn="urn:adsk.eagle:library:11396254" deviceset="CESD5V0D1" device="" package3d_urn="urn:adsk.eagle:package:10898395/2" value="1A/23V/620mV"/>
<part name="C2" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="10uF"/>
<part name="R3" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="100k"/>
<part name="SUPPLY8" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="C3" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="1uF"/>
<part name="C4" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="10uF"/>
<part name="C5" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="0.1uF"/>
<part name="SUPPLY9" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="3.3V" device=""/>
<part name="SUPPLY10" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="R4" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="100k"/>
<part name="R5" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="5.1K"/>
<part name="R6" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="5.1K"/>
<part name="SUPPLY11" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY12" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="F1" library="Circuit-Protection" library_urn="urn:adsk.eagle:library:16378135" deviceset="FUSE" device="CHIP-1206(3216-METRIC)" package3d_urn="urn:adsk.eagle:package:16378157/1" technology="_" value="FUSE"/>
<part name="SUPPLY13" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device=""/>
<part name="C6" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="10uF"/>
<part name="C7" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="1uF"/>
<part name="J4" library="alicedee" library_urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA" deviceset="JST_2PIN" device="" package3d_urn="urn:adsk.wipprod:fs.file:vf.ELgLTILvRsqCS-OhAXe6AQ?version=2"/>
<part name="SUPPLY14" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY15" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="SUPPLY16" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY17" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY20" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="3.3V" device=""/>
<part name="SUPPLY23" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="3.3V" device=""/>
<part name="SUPPLY26" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY27" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="C8" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="1uF"/>
<part name="R7" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="100k"/>
<part name="SUPPLY28" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="3.3V" device=""/>
<part name="SUPPLY29" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="C9" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="22pF"/>
<part name="C10" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="22pF"/>
<part name="SUPPLY30" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="C11" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="0.1uF"/>
<part name="SUPPLY31" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="C12" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="0.1uF"/>
<part name="SUPPLY32" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY33" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY34" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="D5" library="LED" library_urn="urn:adsk.eagle:library:22900745" deviceset="CHIP-ROUND-R" device="_0603" package3d_urn="urn:adsk.wipprod:fs.file:vf.n1ru9s2WSdGmSoMZDXS2cg?version=1"/>
<part name="R8" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="1K"/>
<part name="J3" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="CORTEX_DEBUG" device="_PTH" package3d_urn="urn:adsk.eagle:package:38290/1"/>
<part name="SUPPLY36" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="R9" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="1M"/>
<part name="C13" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="1uF"/>
<part name="SUPPLY37" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="R10" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="10k"/>
<part name="R11" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="10k"/>
<part name="JP1" library="Connector" library_urn="urn:adsk.eagle:library:16378166" deviceset="PINHD-1X2" device="" package3d_urn="urn:adsk.eagle:package:22435/2"/>
<part name="JP2" library="Connector" library_urn="urn:adsk.eagle:library:16378166" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="U5" library="alicedee" library_urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA" deviceset="AP2112-V3.3" device="" package3d_urn="urn:adsk.wipprod:fs.file:vf.FlSLHj1wRFKTmmfP59244Q?version=2" value="AP2112-V3.3"/>
<part name="SUPPLY40" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY41" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCCIO" device=""/>
<part name="C14" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="1uF"/>
<part name="C15" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="10uF"/>
<part name="C16" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="0.1uF"/>
<part name="SUPPLY42" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY19" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCCIO" device=""/>
<part name="SUPPLY21" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCCIO" device=""/>
<part name="SUPPLY38" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCCIO" device=""/>
<part name="TP1" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP2" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP3" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP4" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP5" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP7" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP8" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="SW4" library="Switch" library_urn="urn:adsk.eagle:library:11396471" deviceset="1571610-2" device="" package3d_urn="urn:adsk.eagle:package:17224151/3"/>
<part name="SW1" library="Switch" library_urn="urn:adsk.eagle:library:11396471" deviceset="1571610-2" device="" package3d_urn="urn:adsk.eagle:package:17224151/3"/>
<part name="SW2" library="Switch" library_urn="urn:adsk.eagle:library:11396471" deviceset="1571610-2" device="" package3d_urn="urn:adsk.eagle:package:17224151/3"/>
<part name="TP10" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP11" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP13" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP14" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP15" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="SUPPLY35" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device=""/>
<part name="X1" library="Crystals_Oscillators" library_urn="urn:adsk.eagle:library:24014151" deviceset="9HT10-32.768KDZF-T" device="" package3d_urn="urn:adsk.eagle:package:10899605/3"/>
<part name="J2" library="alicedee" library_urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA" deviceset="QWIIC_CONNECTOR" device="" package3d_urn="urn:adsk.wipprod:fs.file:vf.KLYt6sxhTXiqlVjlj-H4zw?version=2"/>
<part name="JP4" library="SparkFun-Jumpers" library_urn="urn:adsk.eagle:library:528" deviceset="JUMPER-SMT_2_NC_TRACE" device="_SILK" package3d_urn="urn:adsk.eagle:package:39281/1"/>
<part name="SUPPLY43" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCCIO" device=""/>
<part name="R13" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="10k"/>
<part name="R14" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="10k"/>
<part name="TP16" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="TP17" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TPSQ" device="TP11R" package3d_urn="urn:adsk.eagle:package:27961/1"/>
<part name="U2" library="alicedee" library_urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA" deviceset="ESP32-S2-WROVER" device="" package3d_urn="urn:adsk.wipprod:fs.file:vf.MgkoGtQ7RGWIVI9TGL5JwA?version=2"/>
<part name="LED2" library="alicedee" library_urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA" deviceset="WS2812B" device="3535" package3d_urn="urn:adsk.wipprod:fs.file:vf.LFJRaa0_RZWZKQyEjbEAwg?version=3" value="WS2812B3535"/>
<part name="U$2" library="alicedee" library_urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA" deviceset="ISL6292" device="" package3d_urn="urn:adsk.wipprod:fs.file:vf.rYaKax5CTIqFnK3WFbQwkw?version=2"/>
<part name="D1" library="LED" library_urn="urn:adsk.eagle:library:22900745" deviceset="CHIP-ROUND-R" device="_0603" package3d_urn="urn:adsk.wipprod:fs.file:vf.n1ru9s2WSdGmSoMZDXS2cg?version=1"/>
<part name="R17" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="1K"/>
<part name="SUPPLY44" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="R18" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="100K"/>
<part name="C17" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="15nF"/>
<part name="C18" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="0.1uF"/>
<part name="C19" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="1uF"/>
<part name="R19" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="10K"/>
<part name="R15" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="5K"/>
<part name="D2" library="LED" library_urn="urn:adsk.eagle:library:22900745" deviceset="CHIP-ROUND-R" device="_0603" package3d_urn="urn:adsk.wipprod:fs.file:vf.n1ru9s2WSdGmSoMZDXS2cg?version=1"/>
<part name="D6" library="LED" library_urn="urn:adsk.eagle:library:22900745" deviceset="CHIP-ROUND-R" device="_0603" package3d_urn="urn:adsk.wipprod:fs.file:vf.n1ru9s2WSdGmSoMZDXS2cg?version=1"/>
<part name="R16" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="1k"/>
<part name="R20" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="1k"/>
<part name="SUPPLY25" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCCIO" device=""/>
<part name="SUPPLY39" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="3.3V" device=""/>
<part name="SUPPLY45" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="R12" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="100k"/>
<part name="R21" library="Resistor" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="100k"/>
<part name="SUPPLY18" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="GND-BAR" device="" value="GND"/>
<part name="SUPPLY22" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="S1" library="alicedee" library_urn="urn:adsk.wipprod:fs.file:vf.RghxQGbPThCgxrk-_NJcgA" deviceset="EG1213" device="STANDARD" package3d_urn="urn:adsk.wipprod:fs.file:vf.B2GuiAVbSwiZekg0hz0_Jg?version=2"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="167.64" y="48.26" smashed="yes">
<attribute name="NAME" x="149.86" y="77.47" size="2.1844" layer="95"/>
<attribute name="VALUE" x="149.86" y="17.78" size="1.27" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="15.24" y="142.24" smashed="yes">
<attribute name="NAME" x="15.24" y="142.24" size="2.1844" layer="95"/>
</instance>
<instance part="U3" gate="G$1" x="281.94" y="129.54" smashed="yes">
<attribute name="NAME" x="274.32" y="137.414" size="2.1844" layer="95"/>
</instance>
<instance part="Q1" gate="G$1" x="248.92" y="137.16" smashed="yes" rot="R180">
<attribute name="NAME" x="247.65" y="142.24" size="2.1844" layer="95" ratio="10" rot="R180"/>
</instance>
<instance part="D3" gate="G$1" x="264.16" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="264.922" y="68.58" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="C1" gate="G$1" x="325.12" y="73.66" smashed="yes">
<attribute name="NAME" x="327.66" y="76.2" size="2.1844" layer="95"/>
<attribute name="VALUE" x="327.66" y="73.66" size="2.1844" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="259.08" y="71.12" smashed="yes" rot="R270">
<attribute name="NAME" x="255.778" y="72.136" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="260.604" y="72.39" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="R2" gate="G$1" x="309.88" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="312.42" y="58.42" size="2.1844" layer="95"/>
<attribute name="VALUE" x="307.34" y="60.96" size="2.1844" layer="95" rot="R180"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="246.38" y="78.74" smashed="yes">
<attribute name="VALUE" x="246.38" y="81.534" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="325.12" y="83.82" smashed="yes">
<attribute name="VALUE" x="325.12" y="86.614" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="86.36" y="132.08" smashed="yes">
<attribute name="VALUE" x="86.36" y="134.874" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY4" gate="G$1" x="325.12" y="50.8" smashed="yes">
<attribute name="VALUE" x="328.422" y="50.165" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="248.92" y="149.86" smashed="yes">
<attribute name="VALUE" x="248.92" y="152.654" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY6" gate="G$1" x="238.76" y="149.86" smashed="yes">
<attribute name="VALUE" x="238.76" y="152.654" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY7" gate="G$1" x="238.76" y="114.3" smashed="yes">
<attribute name="VALUE" x="241.3" y="113.665" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="D4" gate="G$1" x="243.84" y="129.54" smashed="yes">
<attribute name="NAME" x="241.3" y="132.08" size="2.1844" layer="95"/>
</instance>
<instance part="C2" gate="G$1" x="248.92" y="124.46" smashed="yes">
<attribute name="NAME" x="251.46" y="127" size="2.1844" layer="95"/>
<attribute name="VALUE" x="251.46" y="124.46" size="2.1844" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="259.08" y="121.92" smashed="yes" rot="R270">
<attribute name="NAME" x="261.62" y="121.92" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="256.54" y="116.84" size="2.1844" layer="95" rot="R90"/>
</instance>
<instance part="SUPPLY8" gate="G$1" x="269.24" y="96.52" smashed="yes">
<attribute name="VALUE" x="271.78" y="95.885" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="C3" gate="G$1" x="297.18" y="129.54" smashed="yes">
<attribute name="NAME" x="297.942" y="132.334" size="2.1844" layer="95"/>
<attribute name="VALUE" x="297.942" y="129.54" size="2.1844" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="304.8" y="129.54" smashed="yes">
<attribute name="NAME" x="305.562" y="132.334" size="2.1844" layer="95"/>
<attribute name="VALUE" x="305.562" y="129.54" size="2.1844" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="312.42" y="129.54" smashed="yes">
<attribute name="NAME" x="313.182" y="132.334" size="2.1844" layer="95"/>
<attribute name="VALUE" x="313.182" y="129.54" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY9" gate="G$1" x="317.5" y="137.16" smashed="yes">
<attribute name="VALUE" x="317.5" y="139.954" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY10" gate="G$1" x="304.8" y="116.84" smashed="yes">
<attribute name="VALUE" x="307.34" y="116.205" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="R4" gate="G$1" x="238.76" y="124.46" smashed="yes" rot="R270">
<attribute name="NAME" x="241.3" y="124.46" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="236.22" y="124.46" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="R5" gate="G$1" x="53.34" y="121.92" smashed="yes">
<attribute name="NAME" x="51.308" y="123.19" size="2.1844" layer="95"/>
<attribute name="VALUE" x="50.292" y="118.618" size="2.1844" layer="95"/>
</instance>
<instance part="R6" gate="G$1" x="55.88" y="114.3" smashed="yes">
<attribute name="NAME" x="54.102" y="115.824" size="2.1844" layer="95"/>
<attribute name="VALUE" x="53.34" y="110.744" size="2.1844" layer="95"/>
</instance>
<instance part="SUPPLY11" gate="G$1" x="66.04" y="106.68" smashed="yes">
<attribute name="VALUE" x="69.85" y="105.791" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY12" gate="G$1" x="38.1" y="106.68" smashed="yes">
<attribute name="VALUE" x="41.402" y="106.045" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="F1" gate="G$1" x="50.8" y="137.16" smashed="yes">
<attribute name="VALUE" x="46.99" y="133.35" size="2.1844" layer="96"/>
<attribute name="NAME" x="47.244" y="138.684" size="2.1844" layer="95"/>
</instance>
<instance part="SUPPLY13" gate="G$1" x="68.58" y="139.7" smashed="yes">
<attribute name="VALUE" x="68.58" y="142.494" size="2.1844" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="60.96" y="132.08" smashed="yes">
<attribute name="NAME" x="63.5" y="134.62" size="2.1844" layer="95"/>
<attribute name="VALUE" x="63.5" y="132.08" size="2.1844" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="162.56" y="147.32" smashed="yes">
<attribute name="NAME" x="165.1" y="149.86" size="2.1844" layer="95"/>
<attribute name="VALUE" x="165.1" y="147.32" size="2.1844" layer="96"/>
</instance>
<instance part="J4" gate="G$1" x="144.78" y="147.32" smashed="yes" rot="R180">
<attribute name="NAME" x="151.13" y="141.605" size="2.1844" layer="95" rot="R180"/>
<attribute name="VALUE" x="151.13" y="152.4" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY14" gate="G$1" x="157.48" y="137.16" smashed="yes">
<attribute name="VALUE" x="157.48" y="135.255" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY15" gate="G$1" x="157.48" y="152.4" smashed="yes">
<attribute name="VALUE" x="157.48" y="155.194" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY16" gate="G$1" x="167.64" y="114.3" smashed="yes">
<attribute name="VALUE" x="170.434" y="113.411" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY17" gate="G$1" x="142.24" y="22.86" smashed="yes">
<attribute name="VALUE" x="145.796" y="22.225" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY20" gate="G$1" x="96.52" y="86.36" smashed="yes">
<attribute name="VALUE" x="96.52" y="89.154" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY23" gate="G$1" x="91.44" y="195.58" smashed="yes">
<attribute name="VALUE" x="91.44" y="198.374" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY26" gate="G$1" x="86.36" y="152.4" smashed="yes">
<attribute name="VALUE" x="89.662" y="151.511" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY27" gate="G$1" x="-7.62" y="53.34" smashed="yes">
<attribute name="VALUE" x="-4.572" y="52.705" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="C8" gate="G$1" x="10.16" y="76.2" smashed="yes">
<attribute name="NAME" x="12.7" y="78.74" size="2.1844" layer="95"/>
<attribute name="VALUE" x="12.7" y="76.2" size="2.1844" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="10.16" y="88.9" smashed="yes" rot="R270">
<attribute name="NAME" x="11.684" y="91.44" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="6.096" y="91.694" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="SUPPLY28" gate="G$1" x="10.16" y="96.52" smashed="yes">
<attribute name="VALUE" x="10.16" y="99.314" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY29" gate="G$1" x="10.16" y="66.04" smashed="yes">
<attribute name="VALUE" x="7.62" y="67.945" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="C9" gate="G$1" x="203.2" y="40.64" smashed="yes">
<attribute name="NAME" x="205.74" y="43.18" size="2.1844" layer="95"/>
<attribute name="VALUE" x="205.74" y="40.64" size="2.1844" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="218.44" y="40.64" smashed="yes">
<attribute name="NAME" x="220.98" y="43.18" size="2.1844" layer="95"/>
<attribute name="VALUE" x="220.98" y="40.64" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY30" gate="G$1" x="210.82" y="30.48" smashed="yes">
<attribute name="VALUE" x="213.36" y="29.845" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="C11" gate="G$1" x="129.54" y="60.96" smashed="yes">
<attribute name="NAME" x="129.54" y="63.5" size="2.1844" layer="95" rot="R180"/>
<attribute name="VALUE" x="129.54" y="58.42" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY31" gate="G$1" x="129.54" y="48.26" smashed="yes">
<attribute name="VALUE" x="132.842" y="47.625" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="C12" gate="G$1" x="195.58" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="193.04" y="71.12" size="2.1844" layer="95" rot="R90"/>
<attribute name="VALUE" x="195.58" y="71.12" size="2.1844" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY32" gate="G$1" x="208.28" y="63.5" smashed="yes">
<attribute name="VALUE" x="210.82" y="62.865" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY33" gate="G$1" x="144.78" y="38.1" smashed="yes" rot="R270">
<attribute name="VALUE" x="142.113" y="40.64" size="2.1844" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY34" gate="G$1" x="96.52" y="7.62" smashed="yes">
<attribute name="VALUE" x="99.568" y="6.477" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="D5" gate="G$1" x="121.92" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="122.682" y="25.4" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="R8" gate="G$1" x="114.3" y="22.86" smashed="yes">
<attribute name="NAME" x="112.268" y="24.13" size="2.1844" layer="95"/>
<attribute name="VALUE" x="112.268" y="19.304" size="2.1844" layer="95"/>
</instance>
<instance part="J3" gate="G$1" x="106.68" y="124.46" smashed="yes">
<attribute name="NAME" x="93.98" y="132.334" size="2.1844" layer="95"/>
</instance>
<instance part="SUPPLY36" gate="G$1" x="86.36" y="111.76" smashed="yes">
<attribute name="VALUE" x="89.408" y="110.617" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="R9" gate="G$1" x="116.84" y="167.64" smashed="yes" rot="R270">
<attribute name="NAME" x="118.364" y="169.672" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="113.03" y="169.672" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="C13" gate="G$1" x="124.46" y="170.18" smashed="yes">
<attribute name="NAME" x="127" y="172.72" size="2.1844" layer="95"/>
<attribute name="VALUE" x="127" y="170.18" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY37" gate="G$1" x="119.38" y="154.94" smashed="yes">
<attribute name="VALUE" x="122.936" y="154.051" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="R10" gate="G$1" x="182.88" y="149.86" smashed="yes" rot="R270">
<attribute name="NAME" x="184.404" y="152.4" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="179.07" y="152.146" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="R11" gate="G$1" x="193.04" y="149.86" smashed="yes" rot="R270">
<attribute name="NAME" x="194.564" y="152.146" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="189.23" y="152.146" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="JP1" gate="G$1" x="106.68" y="71.12" smashed="yes">
<attribute name="NAME" x="100.33" y="76.835" size="2.1844" layer="95"/>
</instance>
<instance part="JP2" gate="G$1" x="-2.54" y="38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="3.81" y="34.925" size="2.1844" layer="95" rot="R180"/>
</instance>
<instance part="U5" gate="G$1" x="281.94" y="175.26" smashed="yes">
<attribute name="NAME" x="274.32" y="183.134" size="2.1844" layer="95"/>
</instance>
<instance part="SUPPLY40" gate="G$1" x="261.62" y="162.56" smashed="yes">
<attribute name="VALUE" x="264.414" y="161.671" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY41" gate="G$1" x="309.88" y="185.42" smashed="yes">
<attribute name="VALUE" x="309.88" y="188.214" size="2.1844" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="294.64" y="177.8" smashed="yes">
<attribute name="NAME" x="297.18" y="180.34" size="2.1844" layer="95"/>
<attribute name="VALUE" x="297.18" y="177.8" size="2.1844" layer="96"/>
</instance>
<instance part="C15" gate="G$1" x="302.26" y="177.8" smashed="yes">
<attribute name="NAME" x="304.8" y="180.34" size="2.1844" layer="95"/>
<attribute name="VALUE" x="304.8" y="177.8" size="2.1844" layer="96"/>
</instance>
<instance part="C16" gate="G$1" x="309.88" y="177.8" smashed="yes">
<attribute name="NAME" x="312.42" y="180.34" size="2.1844" layer="95"/>
<attribute name="VALUE" x="312.42" y="177.8" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY42" gate="G$1" x="302.26" y="162.56" smashed="yes">
<attribute name="VALUE" x="305.562" y="161.671" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY19" gate="G$1" x="129.54" y="73.66" smashed="yes">
<attribute name="VALUE" x="129.54" y="76.454" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY21" gate="G$1" x="167.64" y="129.54" smashed="yes">
<attribute name="VALUE" x="163.576" y="132.334" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY38" gate="G$1" x="187.96" y="170.18" smashed="yes">
<attribute name="VALUE" x="187.96" y="172.974" size="2.1844" layer="96"/>
</instance>
<instance part="TP1" gate="G$1" x="127" y="68.58" smashed="yes" rot="R270">
<attribute name="TP_SIGNAL_NAME" x="125.73" y="66.04" size="1.778" layer="97" rot="R270"/>
</instance>
<instance part="TP2" gate="G$1" x="162.56" y="127" smashed="yes" rot="R90">
<attribute name="TP_SIGNAL_NAME" x="163.83" y="129.54" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP3" gate="G$1" x="162.56" y="124.46" smashed="yes" rot="R90">
<attribute name="TP_SIGNAL_NAME" x="163.83" y="127" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP4" gate="G$1" x="40.64" y="121.92" smashed="yes" rot="R90">
<attribute name="TP_SIGNAL_NAME" x="41.91" y="124.46" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP5" gate="G$1" x="40.64" y="119.38" smashed="yes" rot="R90">
<attribute name="TP_SIGNAL_NAME" x="41.91" y="121.92" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP7" gate="G$1" x="259.08" y="132.08" smashed="yes" rot="R180">
<attribute name="TP_SIGNAL_NAME" x="256.54" y="133.35" size="1.778" layer="97" rot="R180"/>
</instance>
<instance part="TP8" gate="G$1" x="251.46" y="147.32" smashed="yes" rot="R90">
<attribute name="TP_SIGNAL_NAME" x="252.73" y="149.86" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="SW4" gate="G$1" x="17.78" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="22.606" y="57.404" size="2.1844" layer="95" rot="R180"/>
</instance>
<instance part="SW1" gate="G$1" x="12.7" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="7.874" y="49.276" size="2.1844" layer="95"/>
</instance>
<instance part="SW2" gate="G$1" x="-2.54" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="-1.27" y="89.154" size="2.1844" layer="95" rot="R180"/>
</instance>
<instance part="TP10" gate="G$1" x="259.08" y="175.26" smashed="yes" rot="R270">
<attribute name="TP_SIGNAL_NAME" x="257.81" y="172.72" size="1.778" layer="97" rot="R270"/>
</instance>
<instance part="TP11" gate="G$1" x="66.04" y="172.72" smashed="yes" rot="R180">
<attribute name="NAME" x="68.58" y="177.165" size="2.1844" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="63.5" y="173.99" size="1.778" layer="97" rot="R180"/>
</instance>
<instance part="TP13" gate="G$1" x="27.94" y="58.42" smashed="yes">
<attribute name="TP_SIGNAL_NAME" x="30.48" y="57.15" size="1.778" layer="97"/>
</instance>
<instance part="TP14" gate="G$1" x="22.86" y="45.72" smashed="yes" rot="R180">
<attribute name="TP_SIGNAL_NAME" x="20.32" y="46.99" size="1.778" layer="97" rot="R180"/>
</instance>
<instance part="TP15" gate="G$1" x="25.4" y="81.28" smashed="yes" rot="R90">
<attribute name="TP_SIGNAL_NAME" x="26.67" y="83.82" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="SUPPLY35" gate="G$1" x="254" y="139.7" smashed="yes">
<attribute name="VALUE" x="254" y="142.494" size="2.1844" layer="96"/>
</instance>
<instance part="X1" gate="G$1" x="210.82" y="48.26" smashed="yes">
<attribute name="NAME" x="210.058" y="50.546" size="2.1844" layer="95"/>
</instance>
<instance part="J2" gate="J1" x="147.32" y="121.92" smashed="yes">
<attribute name="VALUE" x="142.24" y="116.586" size="1.778" layer="96" font="vector" align="top-left"/>
<attribute name="NAME" x="142.24" y="129.794" size="2.1844" layer="95"/>
</instance>
<instance part="JP4" gate="G$1" x="187.96" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="185.42" y="160.02" size="2.1844" layer="95" rot="R90"/>
</instance>
<instance part="SUPPLY43" gate="G$1" x="139.7" y="38.1" smashed="yes">
<attribute name="VALUE" x="139.7" y="40.894" size="2.1844" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="203.2" y="149.86" smashed="yes" rot="R270">
<attribute name="NAME" x="204.47" y="152.4" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="199.39" y="152.146" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="R14" gate="G$1" x="213.36" y="149.86" smashed="yes" rot="R270">
<attribute name="NAME" x="214.63" y="152.654" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="209.55" y="152.146" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="TP16" gate="G$1" x="215.9" y="142.24" smashed="yes" rot="R90">
<attribute name="TP_SIGNAL_NAME" x="217.17" y="144.78" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP17" gate="G$1" x="205.74" y="144.78" smashed="yes" rot="R90">
<attribute name="TP_SIGNAL_NAME" x="207.01" y="147.32" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="U2" gate="G$1" x="63.5" y="48.26" smashed="yes">
<attribute name="NAME" x="40.8441" y="82.3945" size="2.1844" layer="95"/>
<attribute name="VALUE" x="40.5933" y="10.0519" size="2.1844" layer="96"/>
</instance>
<instance part="LED2" gate="G$1" x="86.36" y="172.72" smashed="yes"/>
<instance part="U$2" gate="G$1" x="271.78" y="50.8" smashed="yes"/>
<instance part="D1" gate="G$1" x="256.54" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="254.762" y="66.04" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="R17" gate="G$1" x="251.46" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="248.158" y="73.914" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="252.73" y="72.644" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="SUPPLY44" gate="G$1" x="266.7" y="48.26" smashed="yes">
<attribute name="VALUE" x="270.002" y="47.371" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="R18" gate="G$1" x="309.88" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="312.42" y="60.96" size="2.1844" layer="95"/>
<attribute name="VALUE" x="307.34" y="63.5" size="2.1844" layer="95" rot="R180"/>
</instance>
<instance part="C17" gate="G$1" x="309.88" y="58.42" smashed="yes" rot="R270">
<attribute name="NAME" x="309.88" y="55.88" size="2.1844" layer="95"/>
<attribute name="VALUE" x="307.34" y="58.42" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="C18" gate="G$1" x="309.88" y="55.88" smashed="yes" rot="R270">
<attribute name="NAME" x="309.88" y="53.34" size="2.1844" layer="95"/>
<attribute name="VALUE" x="307.34" y="55.88" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="C19" gate="G$1" x="246.38" y="68.58" smashed="yes">
<attribute name="NAME" x="246.38" y="71.12" size="2.1844" layer="95" rot="R180"/>
<attribute name="VALUE" x="246.38" y="66.04" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="R19" gate="G$1" x="309.88" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="312.42" y="68.58" size="2.1844" layer="95"/>
<attribute name="VALUE" x="307.34" y="71.12" size="2.1844" layer="95" rot="R180"/>
</instance>
<instance part="R15" gate="G$1" x="309.88" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="312.42" y="66.04" size="2.1844" layer="95"/>
<attribute name="VALUE" x="307.34" y="68.58" size="2.1844" layer="95" rot="R180"/>
</instance>
<instance part="D2" gate="G$1" x="149.86" y="182.88" smashed="yes">
<attribute name="NAME" x="152.4" y="181.102" size="2.1844" layer="95"/>
</instance>
<instance part="D6" gate="G$1" x="162.56" y="182.88" smashed="yes">
<attribute name="NAME" x="165.1" y="181.102" size="2.1844" layer="95"/>
</instance>
<instance part="R16" gate="G$1" x="149.86" y="193.04" smashed="yes" rot="R270">
<attribute name="NAME" x="151.13" y="195.58" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="146.304" y="194.31" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="R20" gate="G$1" x="162.56" y="193.04" smashed="yes" rot="R270">
<attribute name="NAME" x="163.83" y="195.326" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="158.75" y="194.31" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="SUPPLY25" gate="G$1" x="162.56" y="200.66" smashed="yes">
<attribute name="VALUE" x="162.56" y="203.454" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY39" gate="G$1" x="149.86" y="200.66" smashed="yes">
<attribute name="VALUE" x="149.86" y="203.454" size="2.1844" layer="96"/>
</instance>
<instance part="SUPPLY45" gate="G$1" x="157.48" y="167.64" smashed="yes">
<attribute name="VALUE" x="157.48" y="165.735" size="2.1844" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="210.82" y="104.14" smashed="yes" rot="R270">
<attribute name="NAME" x="212.344" y="106.68" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="207.01" y="106.426" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="R21" gate="G$1" x="210.82" y="88.9" smashed="yes" rot="R270">
<attribute name="NAME" x="212.344" y="91.44" size="2.1844" layer="95" rot="R270"/>
<attribute name="VALUE" x="207.01" y="91.186" size="2.1844" layer="95" rot="R270"/>
</instance>
<instance part="SUPPLY18" gate="G$1" x="210.82" y="76.2" smashed="yes">
<attribute name="VALUE" x="213.614" y="75.311" size="2.1844" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY22" gate="G$1" x="210.82" y="114.3" smashed="yes">
<attribute name="VALUE" x="210.82" y="117.094" size="2.1844" layer="96"/>
</instance>
<instance part="S1" gate="1" x="256.54" y="109.22" smashed="yes" rot="R270">
<attribute name="NAME" x="254.635" y="104.775" size="1.778" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="4">
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="SUPPLY8" gate="G$1" pin="GND"/>
<wire x1="271.78" y1="124.46" x2="269.24" y2="124.46" width="0.1524" layer="91"/>
<wire x1="269.24" y1="124.46" x2="269.24" y2="99.06" width="0.1524" layer="91"/>
<wire x1="248.92" y1="99.06" x2="269.24" y2="99.06" width="0.1524" layer="91"/>
<junction x="269.24" y="99.06"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="248.92" y1="119.38" x2="248.92" y2="109.22" width="0.1524" layer="91"/>
<pinref part="S1" gate="1" pin="P"/>
<wire x1="248.92" y1="109.22" x2="248.92" y2="99.06" width="0.1524" layer="91"/>
<wire x1="251.46" y1="109.22" x2="248.92" y2="109.22" width="0.1524" layer="91"/>
<junction x="248.92" y="109.22"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="297.18" y1="124.46" x2="297.18" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="297.18" y1="121.92" x2="304.8" y2="121.92" width="0.1524" layer="91"/>
<wire x1="304.8" y1="121.92" x2="312.42" y2="121.92" width="0.1524" layer="91"/>
<wire x1="312.42" y1="121.92" x2="312.42" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="304.8" y1="124.46" x2="304.8" y2="121.92" width="0.1524" layer="91"/>
<junction x="304.8" y="121.92"/>
<pinref part="SUPPLY10" gate="G$1" pin="GND"/>
<wire x1="304.8" y1="121.92" x2="304.8" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="G$1" pin="GND"/>
<wire x1="238.76" y1="119.38" x2="238.76" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="60.96" y1="114.3" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="G$1" pin="GND"/>
<wire x1="66.04" y1="114.3" x2="66.04" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="58.42" y1="121.92" x2="60.96" y2="121.92" width="0.1524" layer="91"/>
<wire x1="60.96" y1="121.92" x2="66.04" y2="121.92" width="0.1524" layer="91"/>
<wire x1="66.04" y1="121.92" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<junction x="66.04" y="114.3"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="60.96" y1="127" x2="60.96" y2="121.92" width="0.1524" layer="91"/>
<junction x="60.96" y="121.92"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="33.02" y1="114.3" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
<wire x1="38.1" y1="114.3" x2="38.1" y2="111.76" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="SHLD"/>
<wire x1="38.1" y1="111.76" x2="38.1" y2="109.22" width="0.1524" layer="91"/>
<wire x1="33.02" y1="111.76" x2="38.1" y2="111.76" width="0.1524" layer="91"/>
<junction x="38.1" y="111.76"/>
<pinref part="SUPPLY12" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="POS"/>
<wire x1="147.32" y1="144.78" x2="157.48" y2="144.78" width="0.1524" layer="91"/>
<pinref part="SUPPLY14" gate="G$1" pin="GND"/>
<wire x1="157.48" y1="144.78" x2="157.48" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="157.48" y1="142.24" x2="157.48" y2="139.7" width="0.1524" layer="91"/>
<wire x1="162.56" y1="142.24" x2="157.48" y2="142.24" width="0.1524" layer="91"/>
<junction x="157.48" y="142.24"/>
</segment>
<segment>
<wire x1="154.94" y1="119.38" x2="167.64" y2="119.38" width="0.1524" layer="91"/>
<wire x1="167.64" y1="119.38" x2="167.64" y2="116.84" width="0.1524" layer="91"/>
<pinref part="SUPPLY16" gate="G$1" pin="GND"/>
<pinref part="J2" gate="J1" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY27" gate="G$1" pin="GND"/>
<wire x1="-7.62" y1="58.42" x2="-7.62" y2="55.88" width="0.1524" layer="91"/>
<wire x1="7.62" y1="58.42" x2="0" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SW4" gate="G$1" pin="SH"/>
<wire x1="0" y1="58.42" x2="-7.62" y2="58.42" width="0.1524" layer="91"/>
<wire x1="10.16" y1="63.5" x2="7.62" y2="63.5" width="0.1524" layer="91"/>
<wire x1="7.62" y1="63.5" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SW4" gate="G$1" pin="2"/>
<wire x1="7.62" y1="60.96" x2="7.62" y2="58.42" width="0.1524" layer="91"/>
<wire x1="10.16" y1="60.96" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<junction x="7.62" y="60.96"/>
<pinref part="SW1" gate="G$1" pin="2"/>
<wire x1="0" y1="58.42" x2="0" y2="45.72" width="0.1524" layer="91"/>
<wire x1="0" y1="45.72" x2="0" y2="43.18" width="0.1524" layer="91"/>
<wire x1="0" y1="43.18" x2="5.08" y2="43.18" width="0.1524" layer="91"/>
<junction x="0" y="58.42"/>
<pinref part="SW1" gate="G$1" pin="SH"/>
<wire x1="5.08" y1="45.72" x2="0" y2="45.72" width="0.1524" layer="91"/>
<junction x="0" y="45.72"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="10.16" y1="71.12" x2="10.16" y2="68.58" width="0.1524" layer="91"/>
<pinref part="SUPPLY29" gate="G$1" pin="GND"/>
<wire x1="-10.16" y1="81.28" x2="-10.16" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="71.12" x2="10.16" y2="71.12" width="0.1524" layer="91"/>
<junction x="10.16" y="71.12"/>
<pinref part="SW2" gate="G$1" pin="2"/>
<pinref part="SW2" gate="G$1" pin="SH"/>
<wire x1="-10.16" y1="83.82" x2="-10.16" y2="81.28" width="0.1524" layer="91"/>
<junction x="-10.16" y="81.28"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GNDIO"/>
<wire x1="147.32" y1="27.94" x2="142.24" y2="27.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY17" gate="G$1" pin="GND"/>
<wire x1="142.24" y1="27.94" x2="142.24" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="147.32" y1="30.48" x2="142.24" y2="30.48" width="0.1524" layer="91"/>
<wire x1="142.24" y1="30.48" x2="142.24" y2="27.94" width="0.1524" layer="91"/>
<junction x="142.24" y="27.94"/>
<pinref part="U1" gate="G$1" pin="PS1"/>
<wire x1="147.32" y1="43.18" x2="132.08" y2="43.18" width="0.1524" layer="91"/>
<wire x1="132.08" y1="43.18" x2="132.08" y2="27.94" width="0.1524" layer="91"/>
<wire x1="132.08" y1="27.94" x2="134.62" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PS0/WAKE"/>
<wire x1="134.62" y1="27.94" x2="142.24" y2="27.94" width="0.1524" layer="91"/>
<wire x1="147.32" y1="40.64" x2="134.62" y2="40.64" width="0.1524" layer="91"/>
<wire x1="134.62" y1="40.64" x2="134.62" y2="27.94" width="0.1524" layer="91"/>
<junction x="134.62" y="27.94"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="218.44" y1="35.56" x2="210.82" y2="35.56" width="0.1524" layer="91"/>
<pinref part="SUPPLY30" gate="G$1" pin="GND"/>
<wire x1="210.82" y1="35.56" x2="210.82" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="210.82" y1="35.56" x2="203.2" y2="35.56" width="0.1524" layer="91"/>
<junction x="210.82" y="35.56"/>
</segment>
<segment>
<pinref part="SUPPLY31" gate="G$1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="129.54" y1="50.8" x2="129.54" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="200.66" y1="68.58" x2="208.28" y2="68.58" width="0.1524" layer="91"/>
<pinref part="SUPPLY32" gate="G$1" pin="GND"/>
<wire x1="208.28" y1="68.58" x2="208.28" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CLKSEL0"/>
<pinref part="SUPPLY33" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY34" gate="G$1" pin="GND"/>
<wire x1="91.44" y1="15.24" x2="96.52" y2="15.24" width="0.1524" layer="91"/>
<wire x1="96.52" y1="15.24" x2="96.52" y2="10.16" width="0.1524" layer="91"/>
<wire x1="91.44" y1="17.78" x2="96.52" y2="17.78" width="0.1524" layer="91"/>
<wire x1="96.52" y1="17.78" x2="96.52" y2="15.24" width="0.1524" layer="91"/>
<junction x="96.52" y="15.24"/>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="127" y1="22.86" x2="132.08" y2="22.86" width="0.1524" layer="91"/>
<wire x1="132.08" y1="22.86" x2="132.08" y2="15.24" width="0.1524" layer="91"/>
<wire x1="132.08" y1="15.24" x2="96.52" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="EXP"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="GND@3"/>
<wire x1="91.44" y1="127" x2="86.36" y2="127" width="0.1524" layer="91"/>
<wire x1="86.36" y1="127" x2="86.36" y2="124.46" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="GND@5"/>
<wire x1="86.36" y1="124.46" x2="86.36" y2="121.92" width="0.1524" layer="91"/>
<wire x1="86.36" y1="121.92" x2="86.36" y2="119.38" width="0.1524" layer="91"/>
<wire x1="86.36" y1="119.38" x2="86.36" y2="114.3" width="0.1524" layer="91"/>
<wire x1="91.44" y1="124.46" x2="86.36" y2="124.46" width="0.1524" layer="91"/>
<junction x="86.36" y="124.46"/>
<pinref part="J3" gate="G$1" pin="KEY"/>
<wire x1="91.44" y1="121.92" x2="86.36" y2="121.92" width="0.1524" layer="91"/>
<junction x="86.36" y="121.92"/>
<pinref part="J3" gate="G$1" pin="GNDDTCT"/>
<wire x1="91.44" y1="119.38" x2="86.36" y2="119.38" width="0.1524" layer="91"/>
<junction x="86.36" y="119.38"/>
<pinref part="SUPPLY36" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="116.84" y1="162.56" x2="116.84" y2="160.02" width="0.1524" layer="91"/>
<wire x1="116.84" y1="160.02" x2="119.38" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="119.38" y1="160.02" x2="124.46" y2="160.02" width="0.1524" layer="91"/>
<wire x1="124.46" y1="160.02" x2="124.46" y2="165.1" width="0.1524" layer="91"/>
<wire x1="119.38" y1="160.02" x2="119.38" y2="157.48" width="0.1524" layer="91"/>
<junction x="119.38" y="160.02"/>
<pinref part="SUPPLY37" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY26" gate="G$1" pin="GND"/>
<wire x1="86.36" y1="162.56" x2="86.36" y2="154.94" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GND"/>
<wire x1="271.78" y1="170.18" x2="261.62" y2="170.18" width="0.1524" layer="91"/>
<wire x1="261.62" y1="170.18" x2="261.62" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SUPPLY40" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="294.64" y1="172.72" x2="294.64" y2="170.18" width="0.1524" layer="91"/>
<wire x1="294.64" y1="170.18" x2="302.26" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="302.26" y1="170.18" x2="309.88" y2="170.18" width="0.1524" layer="91"/>
<wire x1="309.88" y1="170.18" x2="309.88" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="302.26" y1="172.72" x2="302.26" y2="170.18" width="0.1524" layer="91"/>
<junction x="302.26" y="170.18"/>
<pinref part="SUPPLY42" gate="G$1" pin="GND"/>
<wire x1="302.26" y1="170.18" x2="302.26" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="271.78" y1="55.88" x2="266.7" y2="55.88" width="0.1524" layer="91"/>
<wire x1="266.7" y1="55.88" x2="266.7" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SUPPLY44" gate="G$1" pin="GND"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="246.38" y1="63.5" x2="246.38" y2="55.88" width="0.1524" layer="91"/>
<wire x1="246.38" y1="55.88" x2="266.7" y2="55.88" width="0.1524" layer="91"/>
<junction x="266.7" y="55.88"/>
<pinref part="U$2" gate="G$1" pin="PAD"/>
<wire x1="271.78" y1="58.42" x2="266.7" y2="58.42" width="0.1524" layer="91"/>
<label x="266.7" y="58.42" size="2.1844" layer="95"/>
<wire x1="266.7" y1="58.42" x2="266.7" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="SUPPLY4" gate="G$1" pin="GND"/>
<wire x1="325.12" y1="68.58" x2="325.12" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="325.12" y1="63.5" x2="325.12" y2="60.96" width="0.1524" layer="91"/>
<wire x1="325.12" y1="60.96" x2="325.12" y2="58.42" width="0.1524" layer="91"/>
<wire x1="325.12" y1="58.42" x2="325.12" y2="55.88" width="0.1524" layer="91"/>
<wire x1="325.12" y1="55.88" x2="325.12" y2="53.34" width="0.1524" layer="91"/>
<wire x1="314.96" y1="63.5" x2="325.12" y2="63.5" width="0.1524" layer="91"/>
<junction x="325.12" y="63.5"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="314.96" y1="60.96" x2="325.12" y2="60.96" width="0.1524" layer="91"/>
<junction x="325.12" y="60.96"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="312.42" y1="58.42" x2="325.12" y2="58.42" width="0.1524" layer="91"/>
<junction x="325.12" y="58.42"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="312.42" y1="55.88" x2="325.12" y2="55.88" width="0.1524" layer="91"/>
<junction x="325.12" y="55.88"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="314.96" y1="68.58" x2="325.12" y2="68.58" width="0.1524" layer="91"/>
<junction x="325.12" y="68.58"/>
</segment>
<segment>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="162.56" y1="177.8" x2="162.56" y2="172.72" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="149.86" y1="177.8" x2="149.86" y2="172.72" width="0.1524" layer="91"/>
<wire x1="149.86" y1="172.72" x2="157.48" y2="172.72" width="0.1524" layer="91"/>
<wire x1="157.48" y1="172.72" x2="162.56" y2="172.72" width="0.1524" layer="91"/>
<wire x1="157.48" y1="172.72" x2="157.48" y2="170.18" width="0.1524" layer="91"/>
<junction x="157.48" y="172.72"/>
<pinref part="SUPPLY45" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="210.82" y1="83.82" x2="210.82" y2="78.74" width="0.1524" layer="91"/>
<pinref part="SUPPLY18" gate="G$1" pin="GND"/>
</segment>
</net>
<net name="3.3V" class="3">
<segment>
<pinref part="U3" gate="G$1" pin="OUT"/>
<pinref part="SUPPLY9" gate="G$1" pin="3.3V"/>
<wire x1="289.56" y1="134.62" x2="297.18" y2="134.62" width="0.1524" layer="91"/>
<wire x1="297.18" y1="134.62" x2="304.8" y2="134.62" width="0.1524" layer="91"/>
<wire x1="304.8" y1="134.62" x2="312.42" y2="134.62" width="0.1524" layer="91"/>
<wire x1="312.42" y1="134.62" x2="317.5" y2="134.62" width="0.1524" layer="91"/>
<wire x1="317.5" y1="134.62" x2="317.5" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="312.42" y1="132.08" x2="312.42" y2="134.62" width="0.1524" layer="91"/>
<junction x="312.42" y="134.62"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="304.8" y1="132.08" x2="304.8" y2="134.62" width="0.1524" layer="91"/>
<junction x="304.8" y="134.62"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="297.18" y1="132.08" x2="297.18" y2="134.62" width="0.1524" layer="91"/>
<junction x="297.18" y="134.62"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="10.16" y1="93.98" x2="10.16" y2="96.52" width="0.1524" layer="91"/>
<pinref part="SUPPLY28" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="SUPPLY20" gate="G$1" pin="3.3V"/>
<wire x1="91.44" y1="78.74" x2="96.52" y2="78.74" width="0.1524" layer="91"/>
<wire x1="96.52" y1="78.74" x2="96.52" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="3V3"/>
</segment>
<segment>
<pinref part="SUPPLY3" gate="G$1" pin="3.3V"/>
<pinref part="J3" gate="G$1" pin="VCC"/>
<wire x1="86.36" y1="132.08" x2="86.36" y2="129.54" width="0.1524" layer="91"/>
<wire x1="86.36" y1="129.54" x2="91.44" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY23" gate="G$1" pin="3.3V"/>
<wire x1="91.44" y1="195.58" x2="91.44" y2="187.96" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="149.86" y1="198.12" x2="149.86" y2="200.66" width="0.1524" layer="91"/>
<pinref part="SUPPLY39" gate="G$1" pin="3.3V"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<wire x1="35.56" y1="73.66" x2="22.86" y2="73.66" width="0.1524" layer="91"/>
<label x="22.86" y="73.66" size="2.1844" layer="95"/>
<wire x1="10.16" y1="81.28" x2="22.86" y2="81.28" width="0.1524" layer="91"/>
<wire x1="10.16" y1="81.28" x2="10.16" y2="83.82" width="0.1524" layer="91"/>
<junction x="10.16" y="81.28"/>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="10.16" y1="78.74" x2="10.16" y2="81.28" width="0.1524" layer="91"/>
<wire x1="22.86" y1="73.66" x2="22.86" y2="81.28" width="0.1524" layer="91"/>
<wire x1="10.16" y1="81.28" x2="5.08" y2="81.28" width="0.1524" layer="91"/>
<pinref part="SW2" gate="G$1" pin="1"/>
<pinref part="TP15" gate="G$1" pin="TP"/>
<junction x="22.86" y="81.28"/>
<pinref part="U2" gate="G$1" pin="EN"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="!RESET"/>
<wire x1="124.46" y1="119.38" x2="129.54" y2="119.38" width="0.1524" layer="91"/>
<label x="127" y="119.38" size="2.1844" layer="95"/>
</segment>
</net>
<net name="VCC" class="3">
<segment>
<pinref part="SUPPLY1" gate="G$1" pin="VCC"/>
<wire x1="246.38" y1="78.74" x2="246.38" y2="76.2" width="0.1524" layer="91"/>
<wire x1="246.38" y1="76.2" x2="251.46" y2="76.2" width="0.1524" layer="91"/>
<wire x1="251.46" y1="76.2" x2="259.08" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<junction x="259.08" y="76.2"/>
<pinref part="U$2" gate="G$1" pin="VIN"/>
<pinref part="R17" gate="G$1" pin="2"/>
<junction x="251.46" y="76.2"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="259.08" y1="76.2" x2="271.78" y2="76.2" width="0.1524" layer="91"/>
<wire x1="246.38" y1="76.2" x2="246.38" y2="71.12" width="0.1524" layer="91"/>
<junction x="246.38" y="76.2"/>
</segment>
<segment>
<pinref part="SUPPLY6" gate="G$1" pin="VCC"/>
<wire x1="238.76" y1="149.86" x2="238.76" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="238.76" y1="129.54" x2="241.3" y2="129.54" width="0.1524" layer="91"/>
<junction x="238.76" y="129.54"/>
</segment>
<segment>
<pinref part="F1" gate="G$1" pin="2"/>
<wire x1="55.88" y1="137.16" x2="60.96" y2="137.16" width="0.1524" layer="91"/>
<wire x1="60.96" y1="137.16" x2="68.58" y2="137.16" width="0.1524" layer="91"/>
<wire x1="68.58" y1="137.16" x2="68.58" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SUPPLY13" gate="G$1" pin="VCC"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="60.96" y1="134.62" x2="60.96" y2="137.16" width="0.1524" layer="91"/>
<junction x="60.96" y="137.16"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<pinref part="SUPPLY35" gate="G$1" pin="VCC"/>
<wire x1="254" y1="134.62" x2="254" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_BATT" class="1">
<segment>
<pinref part="Q1" gate="G$1" pin="D"/>
<pinref part="SUPPLY5" gate="G$1" pin="V_BATT"/>
<wire x1="248.92" y1="142.24" x2="248.92" y2="147.32" width="0.1524" layer="91"/>
<pinref part="TP8" gate="G$1" pin="TP"/>
<wire x1="248.92" y1="147.32" x2="248.92" y2="149.86" width="0.1524" layer="91"/>
<junction x="248.92" y="147.32"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="NEG"/>
<wire x1="147.32" y1="147.32" x2="157.48" y2="147.32" width="0.1524" layer="91"/>
<wire x1="157.48" y1="147.32" x2="157.48" y2="149.86" width="0.1524" layer="91"/>
<pinref part="SUPPLY15" gate="G$1" pin="V_BATT"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="157.48" y1="149.86" x2="157.48" y2="152.4" width="0.1524" layer="91"/>
<wire x1="157.48" y1="149.86" x2="162.56" y2="149.86" width="0.1524" layer="91"/>
<junction x="157.48" y="149.86"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VBAT"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="302.26" y1="76.2" x2="325.12" y2="76.2" width="0.1524" layer="91"/>
<wire x1="325.12" y1="76.2" x2="325.12" y2="83.82" width="0.1524" layer="91"/>
<junction x="325.12" y="76.2"/>
<pinref part="SUPPLY2" gate="G$1" pin="V_BATT"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="210.82" y1="109.22" x2="210.82" y2="114.3" width="0.1524" layer="91"/>
<pinref part="SUPPLY22" gate="G$1" pin="V_BATT"/>
</segment>
</net>
<net name="VHIGH" class="1">
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="246.38" y1="129.54" x2="248.92" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="248.92" y1="129.54" x2="259.08" y2="129.54" width="0.1524" layer="91"/>
<wire x1="259.08" y1="129.54" x2="264.16" y2="129.54" width="0.1524" layer="91"/>
<wire x1="259.08" y1="127" x2="259.08" y2="129.54" width="0.1524" layer="91"/>
<junction x="259.08" y="129.54"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="248.92" y1="127" x2="248.92" y2="129.54" width="0.1524" layer="91"/>
<junction x="248.92" y="129.54"/>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="248.92" y1="129.54" x2="248.92" y2="132.08" width="0.1524" layer="91"/>
<junction x="248.92" y="129.54"/>
<wire x1="264.16" y1="129.54" x2="264.16" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VIN"/>
<wire x1="264.16" y1="134.62" x2="271.78" y2="134.62" width="0.1524" layer="91"/>
<label x="264.16" y="134.62" size="2.1844" layer="95"/>
<pinref part="TP7" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VIN"/>
<wire x1="271.78" y1="180.34" x2="261.62" y2="180.34" width="0.1524" layer="91"/>
<label x="261.62" y="180.34" size="2.1844" layer="95"/>
</segment>
</net>
<net name="PWR_EN" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="259.08" y1="114.3" x2="259.08" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="EN"/>
<wire x1="271.78" y1="129.54" x2="266.7" y2="129.54" width="0.1524" layer="91"/>
<wire x1="266.7" y1="129.54" x2="266.7" y2="114.3" width="0.1524" layer="91"/>
<wire x1="259.08" y1="114.3" x2="266.7" y2="114.3" width="0.1524" layer="91"/>
<pinref part="S1" gate="1" pin="S"/>
<wire x1="261.62" y1="106.68" x2="266.7" y2="106.68" width="0.1524" layer="91"/>
<wire x1="266.7" y1="106.68" x2="266.7" y2="114.3" width="0.1524" layer="91"/>
<junction x="266.7" y="114.3"/>
</segment>
</net>
<net name="VBATT_PREFUSE" class="1">
<segment>
<pinref part="J1" gate="G$1" pin="VBUS"/>
<wire x1="33.02" y1="137.16" x2="45.72" y2="137.16" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="USBRES1" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="CC1"/>
<wire x1="33.02" y1="127" x2="45.72" y2="127" width="0.1524" layer="91"/>
<wire x1="45.72" y1="127" x2="45.72" y2="121.92" width="0.1524" layer="91"/>
<wire x1="45.72" y1="121.92" x2="48.26" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="USBRES2" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="CC2"/>
<wire x1="33.02" y1="124.46" x2="43.18" y2="124.46" width="0.1524" layer="91"/>
<wire x1="43.18" y1="124.46" x2="43.18" y2="114.3" width="0.1524" layer="91"/>
<wire x1="43.18" y1="114.3" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="D-" class="2">
<segment>
<pinref part="J1" gate="G$1" pin="D-"/>
<wire x1="33.02" y1="121.92" x2="38.1" y2="121.92" width="0.1524" layer="91"/>
<label x="35.56" y="121.92" size="2.1844" layer="95"/>
<pinref part="TP4" gate="G$1" pin="TP"/>
</segment>
<segment>
<wire x1="91.44" y1="55.88" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
<label x="93.98" y="55.88" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO19/D-"/>
</segment>
</net>
<net name="D+" class="2">
<segment>
<pinref part="J1" gate="G$1" pin="D+"/>
<wire x1="33.02" y1="119.38" x2="38.1" y2="119.38" width="0.1524" layer="91"/>
<label x="35.56" y="119.38" size="2.1844" layer="95"/>
<pinref part="TP5" gate="G$1" pin="TP"/>
</segment>
<segment>
<wire x1="91.44" y1="53.34" x2="99.06" y2="53.34" width="0.1524" layer="91"/>
<label x="93.98" y="53.34" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO20/D+"/>
</segment>
</net>
<net name="SDA" class="2">
<segment>
<wire x1="154.94" y1="124.46" x2="160.02" y2="124.46" width="0.1524" layer="91"/>
<label x="154.94" y="124.46" size="2.1844" layer="95"/>
<pinref part="TP3" gate="G$1" pin="TP"/>
<pinref part="J2" gate="J1" pin="SDA"/>
</segment>
<segment>
<wire x1="91.44" y1="45.72" x2="99.06" y2="45.72" width="0.1524" layer="91"/>
<label x="93.98" y="45.72" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO33"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="182.88" y1="144.78" x2="182.88" y2="142.24" width="0.1524" layer="91"/>
<wire x1="182.88" y1="142.24" x2="175.26" y2="142.24" width="0.1524" layer="91"/>
<label x="175.26" y="142.24" size="2.1844" layer="95"/>
</segment>
</net>
<net name="SCL" class="2">
<segment>
<wire x1="154.94" y1="127" x2="160.02" y2="127" width="0.1524" layer="91"/>
<label x="154.94" y="127" size="2.1844" layer="95"/>
<pinref part="TP2" gate="G$1" pin="TP"/>
<pinref part="J2" gate="J1" pin="SCL"/>
</segment>
<segment>
<wire x1="91.44" y1="43.18" x2="99.06" y2="43.18" width="0.1524" layer="91"/>
<label x="93.98" y="43.18" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO34"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="193.04" y1="144.78" x2="193.04" y2="139.7" width="0.1524" layer="91"/>
<wire x1="193.04" y1="139.7" x2="182.88" y2="139.7" width="0.1524" layer="91"/>
<label x="185.42" y="139.7" size="2.1844" layer="95"/>
</segment>
</net>
<net name="BOOT0" class="0">
<segment>
<label x="25.4" y="60.96" size="2.1844" layer="95"/>
<wire x1="25.4" y1="60.96" x2="27.94" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SW4" gate="G$1" pin="1"/>
<pinref part="TP13" gate="G$1" pin="TP"/>
<wire x1="27.94" y1="60.96" x2="35.56" y2="60.96" width="0.1524" layer="91"/>
<junction x="27.94" y="60.96"/>
<pinref part="U2" gate="G$1" pin="IO0_BOOT0"/>
</segment>
</net>
<net name="BNO_RST" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="NRST"/>
<wire x1="187.96" y1="45.72" x2="198.12" y2="45.72" width="0.1524" layer="91"/>
<label x="187.96" y="45.72" size="2.1844" layer="95"/>
</segment>
<segment>
<wire x1="91.44" y1="63.5" x2="99.06" y2="63.5" width="0.1524" layer="91"/>
<label x="93.98" y="63.5" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO16"/>
</segment>
</net>
<net name="XTAL2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CLKSEL1/XOUT32"/>
<wire x1="187.96" y1="58.42" x2="218.44" y2="58.42" width="0.1524" layer="91"/>
<wire x1="218.44" y1="58.42" x2="218.44" y2="48.26" width="0.1524" layer="91"/>
<wire x1="218.44" y1="48.26" x2="218.44" y2="43.18" width="0.1524" layer="91"/>
<junction x="218.44" y="48.26"/>
<pinref part="C10" gate="G$1" pin="1"/>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="213.36" y1="48.26" x2="218.44" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="XTAL1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="XIN32"/>
<wire x1="203.2" y1="48.26" x2="203.2" y2="60.96" width="0.1524" layer="91"/>
<wire x1="203.2" y1="60.96" x2="187.96" y2="60.96" width="0.1524" layer="91"/>
<junction x="203.2" y="48.26"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="203.2" y1="43.18" x2="203.2" y2="48.26" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="203.2" y1="48.26" x2="208.28" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BNOCAP" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CAP"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="187.96" y1="68.58" x2="193.04" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STATUS" class="0">
<segment>
<wire x1="35.56" y1="63.5" x2="25.4" y2="63.5" width="0.1524" layer="91"/>
<label x="25.4" y="63.5" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO45_SPIV"/>
</segment>
<segment>
<wire x1="73.66" y1="170.18" x2="66.04" y2="170.18" width="0.1524" layer="91"/>
<label x="66.04" y="170.18" size="2.1844" layer="95"/>
<pinref part="TP11" gate="G$1" pin="TP"/>
<wire x1="66.04" y1="170.18" x2="63.5" y2="170.18" width="0.1524" layer="91"/>
<junction x="66.04" y="170.18"/>
<pinref part="LED2" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="ACTLED" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="D5" gate="G$1" pin="A"/>
</segment>
</net>
<net name="TMS_D42" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="SWDIO/TMS"/>
<wire x1="124.46" y1="129.54" x2="129.54" y2="129.54" width="0.1524" layer="91"/>
<label x="127" y="129.54" size="2.1844" layer="95"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="91.44" y1="22.86" x2="109.22" y2="22.86" width="0.1524" layer="91"/>
<label x="91.44" y="22.86" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO42/TMS"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="SWDCLK/TCK"/>
<wire x1="124.46" y1="127" x2="129.54" y2="127" width="0.1524" layer="91"/>
<label x="127" y="127" size="2.1844" layer="95"/>
</segment>
<segment>
<wire x1="91.44" y1="30.48" x2="96.52" y2="30.48" width="0.1524" layer="91"/>
<label x="93.98" y="30.48" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO39/TCK"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="SWO/TDO"/>
<wire x1="124.46" y1="124.46" x2="129.54" y2="124.46" width="0.1524" layer="91"/>
<label x="127" y="124.46" size="2.1844" layer="95"/>
</segment>
<segment>
<wire x1="91.44" y1="27.94" x2="96.52" y2="27.94" width="0.1524" layer="91"/>
<label x="93.98" y="27.94" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO40/TDO"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="NC/TDI"/>
<wire x1="124.46" y1="121.92" x2="129.54" y2="121.92" width="0.1524" layer="91"/>
<label x="127" y="121.92" size="2.1844" layer="95"/>
</segment>
<segment>
<wire x1="91.44" y1="25.4" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<label x="93.98" y="25.4" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO41/TDI"/>
</segment>
</net>
<net name="BUTTON" class="0">
<segment>
<label x="25.4" y="43.18" size="2.1844" layer="95"/>
<wire x1="35.56" y1="43.18" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="1"/>
<pinref part="TP14" gate="G$1" pin="TP"/>
<junction x="22.86" y="43.18"/>
<pinref part="U2" gate="G$1" pin="IO6"/>
<wire x1="20.32" y1="43.18" x2="22.86" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<wire x1="91.44" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<label x="93.98" y="73.66" size="2.1844" layer="95"/>
<pinref part="JP1" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="TXD0"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<wire x1="91.44" y1="71.12" x2="104.14" y2="71.12" width="0.1524" layer="91"/>
<label x="93.98" y="71.12" size="2.1844" layer="95"/>
<pinref part="JP1" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="RXD0"/>
</segment>
</net>
<net name="TCA_RST" class="0">
<segment>
<wire x1="35.56" y1="38.1" x2="0" y2="38.1" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="1"/>
<label x="25.4" y="38.1" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO8"/>
</segment>
</net>
<net name="VDIV" class="0">
<segment>
<wire x1="35.56" y1="45.72" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<label x="25.4" y="45.72" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO5"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="210.82" y1="93.98" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="210.82" y1="96.52" x2="210.82" y2="99.06" width="0.1524" layer="91"/>
<junction x="210.82" y="96.52"/>
<wire x1="210.82" y1="96.52" x2="203.2" y2="96.52" width="0.1524" layer="91"/>
<label x="203.2" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="EN2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="EN"/>
<wire x1="271.78" y1="175.26" x2="261.62" y2="175.26" width="0.1524" layer="91"/>
<label x="261.62" y="175.26" size="2.1844" layer="95"/>
<pinref part="TP10" gate="G$1" pin="TP"/>
</segment>
<segment>
<wire x1="91.44" y1="35.56" x2="101.6" y2="35.56" width="0.1524" layer="91"/>
<label x="93.98" y="35.56" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO37"/>
</segment>
</net>
<net name="VCCIO" class="3">
<segment>
<pinref part="U5" gate="G$1" pin="OUT"/>
<wire x1="289.56" y1="180.34" x2="294.64" y2="180.34" width="0.1524" layer="91"/>
<pinref part="SUPPLY41" gate="G$1" pin="VCCIO"/>
<wire x1="294.64" y1="180.34" x2="302.26" y2="180.34" width="0.1524" layer="91"/>
<wire x1="302.26" y1="180.34" x2="309.88" y2="180.34" width="0.1524" layer="91"/>
<wire x1="309.88" y1="180.34" x2="309.88" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="294.64" y="180.34"/>
<pinref part="C15" gate="G$1" pin="1"/>
<junction x="302.26" y="180.34"/>
<pinref part="C16" gate="G$1" pin="1"/>
<junction x="309.88" y="180.34"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDDIO"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="147.32" y1="66.04" x2="137.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="137.16" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="129.54" y1="66.04" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="147.32" y1="68.58" x2="129.54" y2="68.58" width="0.1524" layer="91"/>
<wire x1="129.54" y1="68.58" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<junction x="129.54" y="66.04"/>
<wire x1="129.54" y1="68.58" x2="129.54" y2="71.12" width="0.1524" layer="91"/>
<junction x="129.54" y="68.58"/>
<pinref part="SUPPLY19" gate="G$1" pin="VCCIO"/>
<pinref part="TP1" gate="G$1" pin="TP"/>
<wire x1="129.54" y1="71.12" x2="129.54" y2="73.66" width="0.1524" layer="91"/>
<wire x1="147.32" y1="45.72" x2="121.92" y2="45.72" width="0.1524" layer="91"/>
<wire x1="121.92" y1="45.72" x2="121.92" y2="71.12" width="0.1524" layer="91"/>
<wire x1="121.92" y1="71.12" x2="129.54" y2="71.12" width="0.1524" layer="91"/>
<junction x="129.54" y="71.12"/>
<pinref part="U1" gate="G$1" pin="H_SA0/MOSI"/>
<wire x1="147.32" y1="55.88" x2="137.16" y2="55.88" width="0.1524" layer="91"/>
<wire x1="137.16" y1="55.88" x2="137.16" y2="66.04" width="0.1524" layer="91"/>
<junction x="137.16" y="66.04"/>
<pinref part="U1" gate="G$1" pin="BOOTN"/>
</segment>
<segment>
<wire x1="154.94" y1="121.92" x2="167.64" y2="121.92" width="0.1524" layer="91"/>
<wire x1="167.64" y1="121.92" x2="167.64" y2="129.54" width="0.1524" layer="91"/>
<pinref part="SUPPLY21" gate="G$1" pin="VCCIO"/>
<pinref part="J2" gate="J1" pin="VCC"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="2"/>
<pinref part="SUPPLY38" gate="G$1" pin="VCCIO"/>
<wire x1="187.96" y1="167.64" x2="187.96" y2="170.18" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="203.2" y1="154.94" x2="203.2" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="213.36" y1="154.94" x2="213.36" y2="157.48" width="0.1524" layer="91"/>
<wire x1="213.36" y1="157.48" x2="203.2" y2="157.48" width="0.1524" layer="91"/>
<wire x1="203.2" y1="157.48" x2="203.2" y2="167.64" width="0.1524" layer="91"/>
<wire x1="203.2" y1="167.64" x2="187.96" y2="167.64" width="0.1524" layer="91"/>
<junction x="203.2" y="157.48"/>
<junction x="187.96" y="167.64"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="H_CSN"/>
<wire x1="147.32" y1="35.56" x2="139.7" y2="35.56" width="0.1524" layer="91"/>
<wire x1="139.7" y1="35.56" x2="139.7" y2="38.1" width="0.1524" layer="91"/>
<pinref part="SUPPLY43" gate="G$1" pin="VCCIO"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="162.56" y1="198.12" x2="162.56" y2="200.66" width="0.1524" layer="91"/>
<pinref part="SUPPLY25" gate="G$1" pin="VCCIO"/>
</segment>
</net>
<net name="BNO_INT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="H_INTN"/>
<wire x1="147.32" y1="48.26" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<label x="134.62" y="48.26" size="2.1844" layer="95"/>
</segment>
<segment>
<wire x1="91.44" y1="50.8" x2="99.06" y2="50.8" width="0.1524" layer="91"/>
<label x="93.98" y="50.8" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO21"/>
</segment>
</net>
<net name="SDA_INT" class="2">
<segment>
<pinref part="U1" gate="G$1" pin="H_SDA/MISO/TX"/>
<wire x1="147.32" y1="53.34" x2="139.7" y2="53.34" width="0.1524" layer="91"/>
<label x="134.62" y="53.34" size="2.1844" layer="95"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="203.2" y1="144.78" x2="203.2" y2="139.7" width="0.1524" layer="91"/>
<wire x1="203.2" y1="139.7" x2="205.74" y2="139.7" width="0.1524" layer="91"/>
<label x="202.946" y="138.938" size="2.1844" layer="95" rot="R270"/>
<pinref part="TP17" gate="G$1" pin="TP"/>
<junction x="203.2" y="144.78"/>
</segment>
<segment>
<wire x1="91.44" y1="40.64" x2="99.06" y2="40.64" width="0.1524" layer="91"/>
<label x="93.98" y="40.64" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO35"/>
</segment>
</net>
<net name="SCL_INT" class="2">
<segment>
<pinref part="U1" gate="G$1" pin="H_SCL/SCLK/RX"/>
<wire x1="147.32" y1="50.8" x2="139.7" y2="50.8" width="0.1524" layer="91"/>
<label x="134.62" y="50.8" size="2.1844" layer="95"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="213.36" y1="144.78" x2="213.36" y2="142.24" width="0.1524" layer="91"/>
<wire x1="213.36" y1="142.24" x2="213.36" y2="139.7" width="0.1524" layer="91"/>
<wire x1="213.36" y1="139.7" x2="215.9" y2="139.7" width="0.1524" layer="91"/>
<label x="213.36" y="139.192" size="2.1844" layer="95" rot="R270"/>
<pinref part="TP16" gate="G$1" pin="TP"/>
<junction x="213.36" y="142.24"/>
</segment>
<segment>
<wire x1="91.44" y1="38.1" x2="99.06" y2="38.1" width="0.1524" layer="91"/>
<label x="93.98" y="38.1" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO36"/>
</segment>
</net>
<net name="PULLUPS" class="3">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="193.04" y1="154.94" x2="193.04" y2="157.48" width="0.1524" layer="91"/>
<wire x1="193.04" y1="157.48" x2="187.96" y2="157.48" width="0.1524" layer="91"/>
<pinref part="JP4" gate="G$1" pin="1"/>
<junction x="187.96" y="157.48"/>
<wire x1="187.96" y1="157.48" x2="182.88" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="182.88" y1="154.94" x2="182.88" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="1BITMEM" class="0">
<segment>
<wire x1="91.44" y1="33.02" x2="99.06" y2="33.02" width="0.1524" layer="91"/>
<label x="93.98" y="33.02" size="2.1844" layer="95"/>
<pinref part="U2" gate="G$1" pin="IO38"/>
</segment>
<segment>
<wire x1="124.46" y1="175.26" x2="116.84" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="116.84" y1="175.26" x2="109.22" y2="175.26" width="0.1524" layer="91"/>
<wire x1="116.84" y1="172.72" x2="116.84" y2="175.26" width="0.1524" layer="91"/>
<junction x="116.84" y="175.26"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="124.46" y1="175.26" x2="124.46" y2="172.72" width="0.1524" layer="91"/>
<label x="111.76" y="175.26" size="2.1844" layer="95"/>
</segment>
</net>
<net name="FAULTLED" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="259.08" y1="66.04" x2="259.08" y2="63.5" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="259.08" y1="63.5" x2="261.62" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FAULTLED2" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="U$2" gate="G$1" pin="FAULT"/>
<wire x1="269.24" y1="63.5" x2="271.78" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STATLED2" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="U$2" gate="G$1" pin="STATUS"/>
<wire x1="261.62" y1="60.96" x2="271.78" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STATLED" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="254" y1="60.96" x2="251.46" y2="60.96" width="0.1524" layer="91"/>
<wire x1="251.46" y1="60.96" x2="251.46" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IREF" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="U$2" gate="G$1" pin="IREF"/>
<wire x1="304.8" y1="60.96" x2="302.26" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IMIN" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<pinref part="U$2" gate="G$1" pin="IMIN"/>
<wire x1="304.8" y1="63.5" x2="302.26" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TIME" class="0">
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="U$2" gate="G$1" pin="TIME"/>
<wire x1="304.8" y1="58.42" x2="302.26" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V2P8" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="V2P8"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="302.26" y1="55.88" x2="304.8" y2="55.88" width="0.1524" layer="91"/>
<wire x1="304.8" y1="55.88" x2="304.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="304.8" y1="50.8" x2="299.72" y2="50.8" width="0.1524" layer="91"/>
<junction x="304.8" y="55.88"/>
<label x="299.72" y="50.8" size="2.1844" layer="95"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="314.96" y1="71.12" x2="320.04" y2="71.12" width="0.1524" layer="91"/>
<label x="312.42" y="71.12" size="2.1844" layer="95"/>
</segment>
</net>
<net name="CHRG_EN" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="ENABLE"/>
<wire x1="271.78" y1="73.66" x2="261.62" y2="73.66" width="0.1524" layer="91"/>
<label x="261.62" y="73.66" size="2.1844" layer="95"/>
</segment>
</net>
<net name="TEMP" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="TEMP"/>
<wire x1="302.26" y1="73.66" x2="304.8" y2="73.66" width="0.1524" layer="91"/>
<junction x="304.8" y="73.66"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="304.8" y1="73.66" x2="304.8" y2="71.12" width="0.1524" layer="91"/>
<wire x1="304.8" y1="73.66" x2="309.88" y2="73.66" width="0.1524" layer="91"/>
<label x="304.8" y="73.66" size="2.1844" layer="95"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="304.8" y1="71.12" x2="304.8" y2="68.58" width="0.1524" layer="91"/>
<junction x="304.8" y="71.12"/>
</segment>
</net>
<net name="3VLED" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="149.86" y1="187.96" x2="149.86" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IOLED" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="162.56" y1="187.96" x2="162.56" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="202,1,35.56,66.04,U2,IO46_DEBUGOUT,,,,"/>
<approved hash="208,1,309.88,185.42,VCCIO,sup,,,,"/>
<approved hash="208,1,129.54,73.66,VCCIO,sup,,,,"/>
<approved hash="208,1,147.32,55.88,VCCIO,out,,,,"/>
<approved hash="208,1,167.64,129.54,VCCIO,sup,,,,"/>
<approved hash="208,1,187.96,170.18,VCCIO,sup,,,,"/>
<approved hash="208,1,139.7,38.1,VCCIO,sup,,,,"/>
<approved hash="208,1,162.56,200.66,VCCIO,sup,,,,"/>
<approved hash="113,1,167.64,50.4043,U1,,,,,"/>
<approved hash="113,1,146.687,144.261,J4,,,,,"/>
<approved hash="113,1,150.385,124.788,J2,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
</compatibility>
</eagle>
