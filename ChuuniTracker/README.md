# ChuuniTracker

For all of your 3D tracking needs.

This is an in-development project. Check the `.txt` for materials.

The firmware can be found [here](https://gitlab.com/StarWitch/chuuni-tracker-firmware).

Hardware is released under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/). This means:

1. It is free to use and remix, so long as you attribute me for the original design somewhere (like a README)
2. You do not use it in a commercial product without my permission
3. You do not attempt to prevent others from freely using the design by claiming you “own” it based on a derivative work

## ChuuniTracker IMU

The main IMU board. Uses an ESP32-S2-WROVER. Battery life is about 6-7 hours with a 3000mAh battery, and charges in about an hour.

Additional thermal considerations are built into the board, but it does get quite hot when charging from completely drained.

1. ESP32-S2-WROVER
2. BNO085/BNO086 IMU
3. ISL6292 Li-Ion Charger
4. AP2112 3.3v voltage regulators
5. USB-C 2.0 connector
6. 0603 SMD capacitors/resistors/LEDs
7. JST-SH connectors (QWIIC)
8. Through-hole right-handle buttons/toggles

<img src="./assets/ChuuniTracker.png" alt="drawing" width="300"/>

## FingerSensor (Large/Small)

For tracking individual fingers, but you can also use them for body parts.

Designed for being used two per finger, hooked up to a `GloveConnector`.

1. BNO085/BNO086 IMU
2. JST-SH connectors (QWIIC)
3. 0603 capacitors/resistors

<img src="./assets/FingerSensorSmall.png" alt="drawing" width="300"/>
<img src="./assets/FingerSensorLarge.png" alt="drawing" width="300"/>

## GloveConnector

For connecting the FingerSensors to the main board.

1. TCA9458 I2C Multiplexer
2. 2504 resistor arrays
3. 0603 capacitors/resistors
4. JST-SH connectors (QWIIC)

<img src="./assets/GloveConnector.png" alt="drawing" width="300"/>
